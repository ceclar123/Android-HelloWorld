package com.google.zxing.client.android.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.google.zxing.client.android.Intents;
import com.google.zxing.client.android.history.HistoryItem;
import com.google.zxing.client.android.history.HistoryItemAdapter;
import com.google.zxing.client.android.history.HistoryManager;
import com.mls.wms.android.R;

public final class HistoryActivity extends ListActivity {

    private static final String TAG = HistoryActivity.class.getSimpleName();

    private HistoryManager historyManager;
    private ArrayAdapter<HistoryItem> adapter;
    private CharSequence originalTitle;

    @Override
    protected void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        this.historyManager = new HistoryManager(this);
        adapter = new HistoryItemAdapter(this);
        setListAdapter(adapter);
        View listView = getListView();
        registerForContextMenu(listView);
        originalTitle = getTitle();
    }

    @Override
    protected void onResume() {
        super.onResume();
        reloadHistoryItems();
    }

    private void reloadHistoryItems() {
        Iterable<HistoryItem> items = historyManager.buildHistoryItems();
        adapter.clear();
        for (HistoryItem item : items) {
            adapter.add(item);
        }
        setTitle(originalTitle + " (" + adapter.getCount() + ')');
        if (adapter.isEmpty()) {
            adapter.add(new HistoryItem(null, null, null));
        }
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        if (adapter.getItem(position).getResult() != null) {
            Intent intent = new Intent(this, WmsCaptureActivity.class);
            intent.putExtra(Intents.History.ITEM_NUMBER, position);
            setResult(Activity.RESULT_OK, intent);
            finish();
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        int position = ((AdapterView.AdapterContextMenuInfo) menuInfo).position;
        if (position >= adapter.getCount() || adapter.getItem(position).getResult() != null) {
            menu.add(Menu.NONE, position, position, R.string.scan_history_clear_one_history_text);
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        int position = item.getItemId();
        historyManager.deleteHistoryItem(position);
        reloadHistoryItems();
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (historyManager.hasHistoryItems()) {
            MenuInflater menuInflater = getMenuInflater();
            menuInflater.inflate(R.menu.scan_menu_history, menu);
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_history_clear_text:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage(R.string.dialog_msg_delete_confirm);
                builder.setCancelable(true);
                builder.setPositiveButton(R.string.btn_text_confirm, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i2) {
                        historyManager.clearHistory();
                        dialog.dismiss();
                        finish();
                    }
                });
                builder.setNegativeButton(R.string.btn_text_cancel, null);
                builder.show();
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

}

package com.google.zxing.client.android.activity;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Handler;

import com.google.zxing.Result;
import com.google.zxing.client.android.ViewfinderView;
import com.google.zxing.client.android.camera.CameraManager;

/**
 * Created by 2014-400 on 2015-09-23.
 */
public class BaseCaptureActivity extends Activity implements IActivity {
    @Override
    public void restartPreviewAfterDelay(long delayMS) {
        //
    }

    @Override
    public ViewfinderView getViewfinderView() {
        return null;
    }

    @Override
    public Handler getHandler() {
        return null;
    }

    @Override
    public CameraManager getCameraManager() {
        return null;
    }

    @Override
    public void handleDecode(Result rawResult, Bitmap barcode, float scaleFactor) {
        //
    }

    @Override
    public void drawViewfinder() {
        //
    }
}

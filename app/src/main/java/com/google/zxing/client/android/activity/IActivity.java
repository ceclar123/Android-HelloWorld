package com.google.zxing.client.android.activity;

import android.graphics.Bitmap;
import android.os.Handler;

import com.google.zxing.Result;
import com.google.zxing.client.android.ViewfinderView;
import com.google.zxing.client.android.camera.CameraManager;

/**
 * Created by 2014-400 on 2015-09-23.
 */
public interface IActivity {
    void restartPreviewAfterDelay(long delayMS);

    ViewfinderView getViewfinderView();

    Handler getHandler();

    CameraManager getCameraManager();

    void handleDecode(Result rawResult, Bitmap barcode, float scaleFactor);

    void drawViewfinder();
}

package com.mls.wms.android.model;

import com.mls.wms.android.util.JSONUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Zhuzhenpeng on 2015/9/17.
 */
public class InventoryCountListModel implements IModel, Serializable {
    private long id;
    private String typeCode;
    private String typeName;
    private String modeCode;
    private String modeName;
    private int countPercent;
    private String countBeginTime;//盘点时间
    private String createDate;//制单时间

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTypeCode() {
        return typeCode;
    }

    public void setTypeCode(String typeCode) {
        this.typeCode = typeCode;
    }

    public String getModeCode() {
        return modeCode;
    }

    public void setModeCode(String modeCode) {
        this.modeCode = modeCode;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public String getModeName() {
        return modeName;
    }

    public void setModeName(String modeName) {
        this.modeName = modeName;
    }

    public int getCountPercent() {
        return countPercent;
    }

    public void setCountPercent(int countPercent) {
        this.countPercent = countPercent;
    }

    public String getCountBeginTime() {
        return countBeginTime;
    }

    public void setCountBeginTime(String countBeginTime) {
        this.countBeginTime = countBeginTime;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }


    @Override
    public InventoryCountListModel parseJson(String json) throws Exception {
        InventoryCountListModel model = new InventoryCountListModel();
        JSONObject object = new JSONObject(json);

        model.setId(JSONUtils.getLong(object, "id", 0L));
        model.setTypeCode(JSONUtils.getString(object, "typeCode", ""));
        model.setTypeName(JSONUtils.getString(object, "typeName", ""));
        model.setModeCode(JSONUtils.getString(object, "modeCode", ""));
        model.setModeName(JSONUtils.getString(object, "modeName", ""));
        model.setCountPercent(JSONUtils.getInt(object, "countPercent", 0));
        model.setCountBeginTime(JSONUtils.getString(object, "countBeginTime", ""));
        model.setCreateDate(JSONUtils.getString(object, "createTime", ""));
        return model;
    }

    @Override
    public List<InventoryCountListModel> parseJsons(String json) throws Exception {
        List<InventoryCountListModel> list = new ArrayList<InventoryCountListModel>();
        JSONArray array = new JSONArray(json);
        for (int i = 0; i < array.length(); i++) {
            list.add(parseJson(array.getString(i)));
        }
        return list;
    }
}

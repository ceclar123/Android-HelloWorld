package com.mls.wms.android.extend;

import com.android.http.LoadControler;
import com.android.http.RequestManager;

import java.util.Map;

/**
 * Created by 2014-400 on 2015-09-15.
 */
public interface IHttpVolley {
    /**
     * get
     *
     * @param url
     * @param requestListener
     * @param actionId
     * @return
     */
    public LoadControler get(String url, RequestManager.RequestListener requestListener, int actionId);

    /**
     * get
     *
     * @param url
     * @param data
     * @param requestListener
     * @param actionId
     * @return
     */
    public LoadControler get(String url, Map<String, String> data, RequestManager.RequestListener requestListener, int actionId);

    /**
     * get
     *
     * @param url
     * @param data
     * @param headers
     * @param requestListener
     * @param actionId
     * @return
     */
    public LoadControler get(String url, Map<String, String> data, Map<String, String> headers, RequestManager.RequestListener requestListener, int actionId);

    /**
     * get
     *
     * @param url
     * @param requestListener
     * @param shouldCache
     * @param actionId
     * @return
     */
    public LoadControler get(String url, RequestManager.RequestListener requestListener, boolean shouldCache, int actionId);

    /**
     * get
     *
     * @param url
     * @param data
     * @param headers
     * @param requestListener
     * @param shouldCache
     * @param actionId
     * @return
     */
    public LoadControler get(String url, Map<String, String> data, Map<String, String> headers, RequestManager.RequestListener requestListener, boolean shouldCache, int actionId);

    /**
     * default post method
     *
     * @param url
     * @param data            String, Map<String, String> or RequestMap(with file)
     * @param requestListener
     * @param actionId
     * @return
     */
    public LoadControler post(String url, Object data, final RequestManager.RequestListener requestListener, int actionId);

    /**
     * @param url
     * @param data
     * @param headers
     * @param requestListener
     * @param actionId
     * @return
     */
    public LoadControler post(String url, Object data, Map<String, String> headers, final RequestManager.RequestListener requestListener, int actionId);

    /**
     * put
     *
     * @param url
     * @param data
     * @param requestListener
     * @param actionId
     * @return
     */
    public LoadControler put(String url, Object data, final RequestManager.RequestListener requestListener, int actionId);


}

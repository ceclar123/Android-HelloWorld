package com.mls.wms.android.extend;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;

import com.android.http.LoadControler;
import com.android.http.RequestManager;
import com.google.zxing.client.android.activity.WmsCaptureActivity;
import com.mls.wms.android.R;
import com.mls.wms.android.constant.ScanTypeConstants;
import com.mls.wms.android.constant.ServicePort;
import com.mls.wms.android.fragment.IBottomBar;
import com.mls.wms.android.fragment.ITopBar;
import com.mls.wms.android.util.DialogUtils;
import com.mls.wms.android.util.ViewUtils;
import com.mls.wms.android.util.VolleyUtils;
import com.mls.wms.android.widget.LoginActivity;
import com.mls.wms.android.widget.SettingActivity;

import java.util.Map;

/**
 * Created by 2014-400 on 2015-08-14.
 */
public class BaseActivity extends Activity implements IBottomBar, ITopBar, IHttpVolley {
    protected BaseApplication _BaseApplication;
    private int errorCount = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        _BaseApplication = (BaseApplication) getApplication();
        _BaseApplication.addActivity(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        _BaseApplication.removeActivity(this);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        Log.i(this.getClass().toString(), "keycode=" + keyCode);
        switch (keyCode) {
            //屏蔽Back键
            case KeyEvent.KEYCODE_BACK:
                Log.i(this.getClass().toString(), "KEYCODE_BACK");
                return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    /*-------TopBar--------*/

    @Override
    public int getTopNameResId() {
        return R.string.title_text_about;
    }

    @Override
    public String getTopNameString() {
        return "";
    }

    @Override
    public boolean getBackVisible() {
        return true;
    }

    @Override
    public boolean getScannerVisible() {
        return true;
    }

    @Override
    public boolean getSetVisible() {
        return true;
    }

    public void tv_back_click() {
        finish();
    }

    @Override
    public void tv_scan_click() {
        Intent intent = new Intent("com.google.zxing.client.android.SCAN");
        intent.addCategory(Intent.ACTION_MEDIA_SCANNER_STARTED);
        intent.setClass(this, WmsCaptureActivity.class);
        startActivityForResult(intent, ScanTypeConstants.SCAN_BAR_CODE);
    }

    @Override
    public void tv_setting_click() {
        Intent intent = new Intent();
        intent.setClass(this, SettingActivity.class);
        startActivity(intent);
    }

    /*-------TopBar--------*/

    /*-------BottomBar--------*/
    @Override
    public String getBtnLeftName() {
        return getResources().getString(R.string.btn_text_logout);
    }

    @Override
    public String getBtnMiddleName() {
        return getResources().getString(R.string.btn_text_about);
    }

    @Override
    public String getBtnRightName() {
        return getResources().getString(R.string.btn_text_back);
    }

    @Override
    public void btn_left_click(View v) {
        AlertDialog dialog = new AlertDialog.Builder(v.getContext()).create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.setIcon(android.R.drawable.ic_dialog_alert);
        dialog.setTitle(getResources().getString(R.string.title_text_logout));
        dialog.setMessage(getResources().getString(R.string.dialog_msg_logout_confirm));
        //确定按钮
        dialog.setButton(DialogInterface.BUTTON_POSITIVE, getResources().getText(R.string.enum_yes),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        BaseActivity.this.get(ServicePort.getUrl(ServicePort.LoginOut), null,
                                new RequestManager.RequestListener() {
                                    @Override
                                    public void onRequest() {

                                    }

                                    @Override
                                    public void onSuccess(String s, Map<String, String> stringStringMap, String s2, int i) {
                                        Intent intent = new Intent(Intent.ACTION_VIEW);
                                        intent.setClass(BaseActivity.this, LoginActivity.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                        BaseActivity.this.startActivity(intent);
                                    }

                                    @Override
                                    public void onError(String s, String s2, int i) {
//                                        AlertDialog dialog = new AlertDialog.Builder(BaseActivity.this).create();
//                                        dialog.setCanceledOnTouchOutside(false);
//                                        dialog.setCancelable(false);
//                                        dialog.setIcon(android.R.drawable.ic_dialog_info);
//                                        dialog.setTitle(getResources().getString(R.string.title_text_tip));
//                                        dialog.setMessage(getResources().getString(R.string.dialog_msg_logout_fail));
//                                        //确定按钮
//                                        dialog.setButton(DialogInterface.BUTTON_POSITIVE, getResources().getText(R.string.enum_ok), new DialogInterface.OnClickListener() {
//                                            @Override
//                                            public void onClick(DialogInterface dialog, int which) {
//                                                //
//                                            }
//                                        });
//                                        dialog.show();

                                        Intent intent = new Intent(Intent.ACTION_VIEW);
                                        intent.setClass(BaseActivity.this, LoginActivity.class);
                                        BaseActivity.this.startActivity(intent);
                                    }
                                }, 1);
                    }
                });
        dialog.setButton(DialogInterface.BUTTON_NEGATIVE, getResources().getText(R.string.enum_no),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //
                    }
                });
        dialog.show();
    }

    @Override
    public void btn_middle_click(View v) {
        AlertDialog dialog = new AlertDialog.Builder(v.getContext()).create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.setIcon(android.R.drawable.ic_dialog_info);
        dialog.setTitle(getResources().getString(R.string.title_text_about));
        dialog.setMessage(getResources().getString(R.string.dialog_msg_copyright));
        //确定按钮
        dialog.setButton(DialogInterface.BUTTON_POSITIVE, getResources().getText(R.string.enum_ok),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //
                    }
                });

        dialog.show();
    }

    @Override
    public void btn_right_click(View v) {
        if (this.getBtnRightName().equals(getResources().getString(R.string.btn_text_quit))) {
            AlertDialog dialog = new AlertDialog.Builder(v.getContext()).create();
            dialog.setCanceledOnTouchOutside(false);
            dialog.setCancelable(false);
            dialog.setIcon(android.R.drawable.ic_dialog_alert);
            dialog.setTitle(getResources().getString(R.string.title_text_quit));
            dialog.setMessage(getResources().getString(R.string.dialog_msg_quit_confirm));
            //确定按钮
            dialog.setButton(DialogInterface.BUTTON_POSITIVE, getResources().getText(R.string.enum_yes),
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            BaseActivity.this.get(ServicePort.getUrl(ServicePort.LoginOut), null,
                                    new RequestManager.RequestListener() {
                                        @Override
                                        public void onRequest() {

                                        }

                                        @Override
                                        public void onSuccess(String s, Map<String, String> stringStringMap, String s2, int i) {
                                            _BaseApplication.exit();
                                        }

                                        @Override
                                        public void onError(String s, String s2, int i) {
//                                            AlertDialog dialog = new AlertDialog.Builder(BaseActivity.this).create();
//                                            dialog.setCanceledOnTouchOutside(false);
//                                            dialog.setCancelable(false);
//                                            dialog.setIcon(android.R.drawable.ic_dialog_info);
//                                            dialog.setTitle(getResources().getString(R.string.title_text_about));
//                                            dialog.setMessage(getResources().getString(R.string.dialog_msg_quit_fail));
//                                            //确定按钮
//                                            dialog.setButton(DialogInterface.BUTTON_POSITIVE, getResources().getText(R.string.enum_ok),
//                                                    new DialogInterface.OnClickListener() {
//                                                        @Override
//                                                        public void onClick(DialogInterface dialog, int which) {
//                                                            if (errorCount >= 3) {
//                                                                _BaseApplication.exit();
//                                                            }
//                                                            errorCount++;
//                                                        }
//                                                    });
//                                            dialog.show();

                                            _BaseApplication.exit();
                                        }
                                    }, 1);

                        }
                    });
            dialog.setButton(DialogInterface.BUTTON_NEGATIVE, getResources().getText(R.string.enum_no), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    //
                }
            });

            dialog.show();
        } else {
            this.finish();
        }
    }
  /*-------BottomBar--------*/


    /**
     * ********http请求方法*********************************************
     */

    private BaseApplication getBaseApplication() {
        return (BaseApplication) getApplication();
    }

    /**
     * get
     *
     * @param url
     * @param requestListener
     * @param actionId
     * @return
     */
    @Override
    public LoadControler get(String url, RequestManager.RequestListener requestListener, int actionId) {
        return VolleyUtils.get(getBaseApplication(), url, null, null, requestListener, true, actionId);
    }

    /**
     * get
     *
     * @param url
     * @param data
     * @param requestListener
     * @param actionId
     * @return
     */
    @Override
    public LoadControler get(String url, Map<String, String> data, RequestManager.RequestListener requestListener, int actionId) {
        return VolleyUtils.get(getBaseApplication(), url, data, null, requestListener, true, actionId);
    }

    /**
     * get
     *
     * @param url
     * @param data
     * @param headers
     * @param requestListener
     * @param actionId
     * @return
     */
    @Override
    public LoadControler get(String url, Map<String, String> data, Map<String, String> headers, RequestManager.RequestListener requestListener, int actionId) {
        return VolleyUtils.get(getBaseApplication(), url, data, headers, requestListener, true, actionId);
    }

    /**
     * get
     *
     * @param url
     * @param requestListener
     * @param shouldCache
     * @param actionId
     * @return
     */
    @Override
    public LoadControler get(String url, RequestManager.RequestListener requestListener, boolean shouldCache, int actionId) {
        return VolleyUtils.get(getBaseApplication(), url, null, null, requestListener, shouldCache, actionId);
    }

    /**
     * get
     *
     * @param url
     * @param data
     * @param headers
     * @param requestListener
     * @param shouldCache
     * @param actionId
     * @return
     */
    @Override
    public LoadControler get(String url, Map<String, String> data, Map<String, String> headers, RequestManager.RequestListener requestListener, boolean shouldCache, int actionId) {
        return VolleyUtils.get(getBaseApplication(), url, data, headers, requestListener, shouldCache, actionId);
    }

    /**
     * default post method
     *
     * @param url
     * @param data            String, Map<String, String> or RequestMap(with file)
     * @param requestListener
     * @param actionId
     * @return
     */
    @Override
    public LoadControler post(String url, Object data, final RequestManager.RequestListener requestListener, int actionId) {
        return VolleyUtils.post(getBaseApplication(), url, data, null, requestListener, actionId);
    }

    /**
     * @param url
     * @param data
     * @param headers
     * @param requestListener
     * @param actionId
     * @return
     */
    @Override
    public LoadControler post(String url, Object data, Map<String, String> headers, final RequestManager.RequestListener requestListener, int actionId) {
        return VolleyUtils.post(getBaseApplication(), url, data, headers, requestListener, actionId);
    }

    /**
     * put
     *
     * @param url
     * @param data
     * @param requestListener
     * @param actionId
     * @return
     */
    @Override
    public LoadControler put(String url, Object data, final RequestManager.RequestListener requestListener, int actionId) {
        return VolleyUtils.put(getBaseApplication(), url, data, requestListener, actionId);
    }


    /**
     * 弹出提示框
     *
     * @param errorMsg 错误内容
     * @param focusCtl 确定后焦点定位
     */
    protected void showDialog(String errorMsg, final EditText focusCtl) {
        DialogUtils.showOK(this, getResources().getString(R.string.title_text_exception), errorMsg, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                focusCtl.clearFocus();
                focusCtl.setFocusable(true);
                focusCtl.setFocusableInTouchMode(true);
                focusCtl.requestFocus();
                focusCtl.setCursorVisible(true);
            }
        });
    }

    /**
     * 弹出提示框
     *
     * @param resId    文本资源ID
     * @param focusCtl 确定后焦点定位
     */
    protected void showDialog(int resId, final EditText focusCtl) {
        String message = getResources().getString(resId);
        this.showDialog(message, focusCtl);
    }

    /**
     * 弹出提示框
     *
     * @param errorMsg 错误内容
     * @param focusCtl 确定后焦点定位
     */
    protected void showDialog(String errorMsg, final RadioGroup focusCtl) {
        DialogUtils.showOK(this, getResources().getString(R.string.title_text_exception), errorMsg, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                focusCtl.clearFocus();
                focusCtl.setFocusable(true);
                focusCtl.setFocusableInTouchMode(true);
                focusCtl.requestFocus();
            }
        });
    }

    /**
     * 弹出提示框
     *
     * @param resId    文本资源ID
     * @param focusCtl 确定后焦点定位
     */
    protected void showDialog(int resId, final RadioGroup focusCtl) {
        String message = getResources().getString(resId);
        this.showDialog(message, focusCtl);
    }


    /**
     * 控件操作
     */

    public void focusNextControl(View current) {
        if (false == ViewUtils.isLastControl(BaseActivity.this, current, EditText.class)) {
            ViewUtils.focusNextControl(BaseActivity.this, current, EditText.class);
        } else {
            Button btnLeft = (Button) this.findViewById(R.id.btn_left);
            if (btnLeft != null) {
                btnLeft.clearFocus();
                btnLeft.setFocusable(true);
                btnLeft.setFocusableInTouchMode(true);
                btnLeft.requestFocus();
                btnLeft.setCursorVisible(true);
                this.btn_left_click(btnLeft);
            }
        }
    }

}

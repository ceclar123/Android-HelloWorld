package com.mls.wms.android.widget.outwh;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.widget.Toast;

import com.android.http.RequestManager;
import com.mls.wms.android.R;
import com.mls.wms.android.adapter.PickShipDetailAdapter;
import com.mls.wms.android.constant.ServicePort;
import com.mls.wms.android.extend.AnimDialogFragment;
import com.mls.wms.android.extend.BaseActivity;
import com.mls.wms.android.extend.MultiListView;
import com.mls.wms.android.model.PickShipDetailModel;
import com.mls.wms.android.model.ResponseResult;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by 2014-400 on 2015-09-02.
 */
public class PickShipDetailActivity extends BaseActivity {
    private final int LIST_ITEM_COUNT = 3;
    private MultiListView listHeader = null;
    private PickShipDetailAdapter adapter = null;
    private PickShipDetailModel selectModel = null;
    private int pickType = 0;//0=边拣边分 1=先拣后分 2=按单拣货
    private long pickId = 0L;//波次ID 或则 出库单ID

    private List<PickShipDetailModel> _listAll = null;
    private List<PickShipDetailModel> _listCurrent = null;

    private AnimDialogFragment progressDialog = null;
    private Handler handler = new Handler();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_base_detail);

        //弹出框
        progressDialog = new AnimDialogFragment();
        progressDialog.setResId(R.drawable.anim_progress_dialog);


        Intent intent = this.getIntent();
        if (intent != null) {
            pickType = intent.getIntExtra("type", 0);
            pickId = intent.getLongExtra("id", 0L);
        } else {
            Toast.makeText(this, R.string.dialog_msg_para_error, Toast.LENGTH_SHORT).show();
            this.finish();
        }

        this.listHeader = (MultiListView) this.findViewById(R.id.act_base_detail_header);

        this.listHeader.setOnRefreshListener(new MultiListView.OnRefreshListener() {
            @Override
            public void onRefresh() {
                load();
            }
        });
        this.listHeader.setOnLoadListener(new MultiListView.OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        boolean flag = copyData();
                        adapter.notifyDataSetChanged();

                        listHeader.setCanLoadMore(flag);
                        listHeader.onLoadMoreComplete();
                    }
                }, 1 * 1000);
            }
        });

        //加载数据
        this.load();
    }

    private void load() {
        Map<String, String> paras = new HashMap<String, String>();
        paras.put("id", this.pickId + "");
        paras.put("type", this.pickType + "");

        progressDialog.show(getFragmentManager(), this.toString());
        get(ServicePort.getUrl(ServicePort.PickDetailWave), paras, new RequestManager.RequestListener() {
            @Override
            public void onRequest() {

            }

            @Override
            public void onSuccess(String s, Map<String, String> stringStringMap, String s2, int i) {
                try {
                    ResponseResult result = new ResponseResult();
                    result = result.parseJson(s);
                    if (result != null && result.getSuc() == true) {
                        if (result.getResult() == null || TextUtils.isEmpty(result.getResult().toString())) {
                            throw new Exception(getResources().getString(R.string.exception_no_data));
                        }

                        PickShipDetailModel model = new PickShipDetailModel();
                        _listCurrent = null;
                        _listAll = model.parseJsons(result.getResult().toString());

                        boolean flag = copyData();
                        listHeader.setCanLoadMore(flag);

                        adapter = new PickShipDetailAdapter(PickShipDetailActivity.this, pickType, pickId, _listCurrent);
                        listHeader.setAdapter(adapter);

                        progressDialog.dismiss();
                    } else {
                        String message = result == null ? getResources().getString(R.string.title_text_operate_fail) : result.getMessage();
                        progressDialog.dismiss();
                        Toast.makeText(PickShipDetailActivity.this, message, Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    progressDialog.dismiss();
                    Toast.makeText(PickShipDetailActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                } finally {
                    listHeader.onRefreshComplete();
                }

            }

            @Override
            public void onError(String s, String s2, int i) {
                progressDialog.dismiss();
                Toast.makeText(PickShipDetailActivity.this, s, Toast.LENGTH_SHORT).show();
            }
        }, 1);
    }


    /**
     * 复制数据
     *
     * @return false:数据加载完了
     */
    private boolean copyData() {
        if (_listAll != null) {
            if (_listCurrent == null) {
                _listCurrent = new ArrayList<PickShipDetailModel>();
            }

            if (_listAll.size() > LIST_ITEM_COUNT) {
                for (int i = 0; i < LIST_ITEM_COUNT; i++) {
                    _listCurrent.add(_listAll.get(0));
                    _listAll.remove(0);
                }
            } else {
                _listCurrent.addAll(_listAll);
                _listAll.clear();
                return false;
            }
        }

        return true;
    }


    @Override
    public String getTopNameString() {
        if (this.pickType == 0 || this.pickType == 1) {
            return "波次单号:" + this.pickId;
        } else {
            return "出库单号:" + this.pickId;
        }
    }

    @Override
    public boolean getScannerVisible() {
        return false;
    }

    @Override
    public boolean getSetVisible() {
        return false;
    }
}
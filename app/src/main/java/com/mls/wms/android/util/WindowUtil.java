package com.mls.wms.android.util;

import android.app.Activity;
import android.graphics.Point;
import android.util.DisplayMetrics;

/**
 * Created by 2014-400 on 2015-08-13.
 */
public class WindowUtil {
    private WindowUtil() {
    }


    /**
     * 获取实际屏幕大小(Android API 17)
     *
     * @param activity
     * @return
     */
    public static Point getRealSize(Activity activity) {
        Point outPoint = new Point();
        activity.getWindowManager().getDefaultDisplay().getRealSize(outPoint);


        return outPoint;
    }

    public static Point getMetrics(Activity activity) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);

        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;

        return new Point(width, height);
    }

}

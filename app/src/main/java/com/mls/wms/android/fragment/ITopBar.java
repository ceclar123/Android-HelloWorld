package com.mls.wms.android.fragment;

/**
 * Created by 2014-400 on 2015-09-09.
 */
public interface ITopBar {
    public int getTopNameResId();

    public String getTopNameString();

    public boolean getBackVisible();

    public boolean getScannerVisible();

    public boolean getSetVisible();

    public void tv_back_click();

    public void tv_scan_click();

    public void tv_setting_click();
}

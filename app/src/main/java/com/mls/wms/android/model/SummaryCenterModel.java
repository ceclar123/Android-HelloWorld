package com.mls.wms.android.model;

import android.text.TextUtils;

import com.mls.wms.android.util.JSONUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by 2014-400 on 2015-09-21.
 */
public class SummaryCenterModel implements IModel, Serializable {
    private String[] series;
    private String[] labels;
    private int[][] inbData;
    private int[][] onHandData;
    private int[][] oubData;
    private int[] inbTotal;
    private int[] oubTotal;
    private int[] onHandTotal;

    public String[] getSeries() {
        return series;
    }

    public void setSeries(String[] series) {
        this.series = series;
    }

    public String[] getLabels() {
        return labels;
    }

    public void setLabels(String[] labels) {
        this.labels = labels;
    }

    public int[][] getInbData() {
        return inbData;
    }

    public void setInbData(int[][] inbData) {
        this.inbData = inbData;
    }

    public int[][] getOnHandData() {
        return onHandData;
    }

    public void setOnHandData(int[][] onHandData) {
        this.onHandData = onHandData;
    }

    public int[][] getOubData() {
        return oubData;
    }

    public void setOubData(int[][] oubData) {
        this.oubData = oubData;
    }

    public int[] getInbTotal() {
        return inbTotal;
    }

    public void setInbTotal(int[] inbTotal) {
        this.inbTotal = inbTotal;
    }

    public int[] getOubTotal() {
        return oubTotal;
    }

    public void setOubTotal(int[] oubTotal) {
        this.oubTotal = oubTotal;
    }

    public int[] getOnHandTotal() {
        return onHandTotal;
    }

    public void setOnHandTotal(int[] onHandTotal) {
        this.onHandTotal = onHandTotal;
    }

    @Override
    public SummaryCenterModel parseJson(String json) throws Exception {
        SummaryCenterModel model = new SummaryCenterModel();

        JSONObject object = new JSONObject(json);
        String data = JSONUtils.getString(object, "series", "");
        if (false == TextUtils.isEmpty(data)) {
            model.setSeries(copyString(data));
        }
        data = JSONUtils.getString(object, "labels", "");
        if (false == TextUtils.isEmpty(data)) {
            model.setLabels(copyString(data));
        }
        data = JSONUtils.getString(object, "inbData", "");
        if (false == TextUtils.isEmpty(data)) {
            model.setInbData(copyData(data));
        }
        data = JSONUtils.getString(object, "onHandData", "");
        if (false == TextUtils.isEmpty(data)) {
            model.setOnHandData(copyData(data));
        }
        data = JSONUtils.getString(object, "oubData", "");
        if (false == TextUtils.isEmpty(data)) {
            model.setOubData(copyData(data));
        }
        data = JSONUtils.getString(object, "inbTotal", "");
        if (false == TextUtils.isEmpty(data)) {
            model.setInbTotal(copyInt(data));
        }
        data = JSONUtils.getString(object, "onHandTotal", "");
        if (false == TextUtils.isEmpty(data)) {
            model.setOnHandTotal(copyInt(data));
        }
        data = JSONUtils.getString(object, "oubTotal", "");
        if (false == TextUtils.isEmpty(data)) {
            model.setOubTotal(copyInt(data));
        }

        return model;
    }

    @Override
    public List<SummaryCenterModel> parseJsons(String json) throws Exception {
        List<SummaryCenterModel> list = new ArrayList<SummaryCenterModel>();
        JSONArray array = new JSONArray(json);
        for (int i = 0; i < array.length(); i++) {
            list.add(parseJson(array.getString(i)));
        }
        return list;
    }

    private int[] copyInt(String input) {
        int[] temp = null;
        try {
            JSONArray jsonArray = new JSONArray(input);
            temp = new int[jsonArray.length()];
            for (int i = 0; i < jsonArray.length(); i++) {
                temp[i] = jsonArray.getInt(i);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return temp;
    }

    private String[] copyString(String input) {
        String[] temp = null;
        try {
            JSONArray jsonArray = new JSONArray(input);
            temp = new String[jsonArray.length()];
            for (int i = 0; i < jsonArray.length(); i++) {
                temp[i] = jsonArray.getString(i);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return temp;
    }

    private int[][] copyData(String input) {
        int[][] dataObj = null;
        try {
            JSONArray jsonArrayOuter = new JSONArray(input);
            dataObj = new int[jsonArrayOuter.length()][];
            for (int i = 0; i < jsonArrayOuter.length(); i++) {
                JSONArray jsonObjectInner = jsonArrayOuter.getJSONArray(i);
                int[] temp = new int[jsonObjectInner.length()];
                for (int j = 0; j < jsonObjectInner.length(); j++) {
                    temp[j] = jsonObjectInner.getInt(j);
                }

                dataObj[i] = temp;
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return dataObj;
    }
}

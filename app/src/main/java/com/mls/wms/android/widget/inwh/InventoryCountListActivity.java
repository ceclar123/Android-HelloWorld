package com.mls.wms.android.widget.inwh;

import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.http.RequestManager;
import com.mls.wms.android.R;
import com.mls.wms.android.adapter.InventoryCountListAdapter;
import com.mls.wms.android.constant.ServicePort;
import com.mls.wms.android.extend.AnimDialogFragment;
import com.mls.wms.android.extend.BaseActivity;
import com.mls.wms.android.extend.MultiListView;
import com.mls.wms.android.model.InventoryCountListModel;
import com.mls.wms.android.model.ResponseResult;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Zhuzhenpeng on 2015/9/17.
 */
public class InventoryCountListActivity extends BaseActivity {
    private final int LIST_ITEM_COUNT = 4;
    private EditText searchText = null;
    private TextView tvQuery = null;
    private MultiListView listHeader = null;
    private InventoryCountListAdapter adapter = null;
    private InventoryCountListModel selectModel = null;

    private List<InventoryCountListModel> _listAll = null;
    private List<InventoryCountListModel> _listCurrent = null;

    private AnimDialogFragment progressDialog = null;
    private Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_base_list);

        //弹出框
        progressDialog = new AnimDialogFragment();
        progressDialog.setResId(R.drawable.anim_progress_dialog);

        this.searchText = (EditText) this.findViewById(R.id.act_base_list_search_text);
        this.tvQuery = (TextView) this.findViewById(R.id.act_base_list_tv_query);
        this.listHeader = (MultiListView) this.findViewById(R.id.act_base_list_header);

        this.searchText.setHint(R.string.hint_inventory_count_id);
        this.tvQuery.setTypeface(_BaseApplication.getIconFont());

        this.listHeader.setOnRefreshListener(new MultiListView.OnRefreshListener() {
            @Override
            public void onRefresh() {
                load();
            }
        });
        this.listHeader.setOnLoadListener(new MultiListView.OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        boolean flag = copyData();
                        adapter.notifyDataSetChanged();

                        listHeader.setCanLoadMore(flag);
                        listHeader.onLoadMoreComplete();
                    }
                }, 1 * 1000);
            }
        });

        //点击查询
        this.tvQuery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                load();
            }
        });

    }

    private void load() {
        Map<String, String> paras = new HashMap<String, String>();
        try {
            paras.put("id", Integer.parseInt(searchText.getText().toString()) + "");
        } catch (Exception e) {
            paras.put("id", "");
            e.printStackTrace();
        }

        progressDialog.show(getFragmentManager(), this.toString());
        get(ServicePort.getUrl(ServicePort.CountList), paras, new RequestManager.RequestListener() {
            @Override
            public void onRequest() {

            }

            @Override
            public void onSuccess(String s, Map<String, String> stringStringMap, String s2, int i) {
                try {
                    ResponseResult result = new ResponseResult();
                    result = result.parseJson(s);
                    if (result != null && result.getSuc() == true) {
                        if (result.getResult() == null || TextUtils.isEmpty(result.getResult().toString())) {
                            throw new Exception(getResources().getString(R.string.exception_no_data));
                        }

                        InventoryCountListModel model = new InventoryCountListModel();
                        _listCurrent = null;
                        _listAll = model.parseJsons(result.getResult().toString());

                        boolean flag = copyData();
                        listHeader.setCanLoadMore(flag);

                        adapter = new InventoryCountListAdapter(InventoryCountListActivity.this, _listCurrent);
                        listHeader.setAdapter(adapter);
                        progressDialog.dismiss();
                    } else {
                        String message = result == null ? getResources().getString(R.string.title_text_operate_fail) : result.getMessage();
                        progressDialog.dismiss();
                        Toast.makeText(InventoryCountListActivity.this, message, Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    progressDialog.dismiss();
                    Toast.makeText(InventoryCountListActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                } finally {
                    listHeader.onRefreshComplete();
                }
            }

            @Override
            public void onError(String s, String s2, int i) {
                progressDialog.dismiss();
                Toast.makeText(InventoryCountListActivity.this, s, Toast.LENGTH_SHORT).show();
            }
        }, 1);
    }

    /**
     * 复制数据
     *
     * @return false:数据加载完了
     */
    private boolean copyData() {
        if (_listAll != null) {
            if (_listCurrent == null) {
                _listCurrent = new ArrayList<InventoryCountListModel>();
            }

            if (_listAll.size() > LIST_ITEM_COUNT) {
                for (int i = 0; i < LIST_ITEM_COUNT; i++) {
                    _listCurrent.add(_listAll.get(0));
                    _listAll.remove(0);
                }
            } else {
                _listCurrent.addAll(_listAll);
                _listAll.clear();
                return false;
            }
        }

        return true;
    }

    @Override
    public boolean getScannerVisible() {
        return false;
    }

    @Override
    public boolean getSetVisible() {
        return false;
    }

    @Override
    public int getTopNameResId() {
        return R.string.title_text_inventory_count;
    }
}

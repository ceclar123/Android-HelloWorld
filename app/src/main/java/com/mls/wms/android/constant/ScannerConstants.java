package com.mls.wms.android.constant;

/**
 * Created by 2014-400 on 2015-09-07.
 */
public class ScannerConstants {
    public static final int DECODE_SUCCEEDED = 1;
    public static final int DECODE_FAILED = 2;
    public static final int DECODE = 3;
    public static final int QUIT = 4;
    public static final int AUTO_FOCUS = 5;
    public static final int RESTART_PREVIEW = 6;
    public static final int RETURN_SCAN_RESULT = 7;
    public static final int LAUNCH_PRODUCT_QUERY = 8;

}

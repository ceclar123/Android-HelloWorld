package com.mls.wms.android.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.HorizontalScrollView;
import android.widget.TextView;

import com.mls.wms.android.R;
import com.mls.wms.android.constant.InventoryCountModeCode;
import com.mls.wms.android.extend.SwipeLayoutAdapter;
import com.mls.wms.android.model.InventoryCountListModel;
import com.mls.wms.android.widget.inwh.InventoryCountAmountActivity;
import com.mls.wms.android.widget.inwh.InventoryCountCommodityActivity;
import com.mls.wms.android.widget.inwh.InventoryCountDetailActivity;

import java.util.List;

/**
 * Created by Zhuzhenpeng on 2015/9/17.
 */
public class InventoryCountListAdapter extends SwipeLayoutAdapter<InventoryCountListModel> {

    private LayoutInflater inflater;
    private Context context;
    private List<InventoryCountListModel> items;

    public InventoryCountListAdapter(Context context, List<InventoryCountListModel> items) {
        super(context, R.layout.act_inventory_count_item, R.layout.act_inventory_count_item_swipe, items);
        this.inflater = LayoutInflater.from(context);
        this.context = context;
        this.items = items;
    }

    @Override
    public long getItemId(int position) {
        return position % this.items.size();
    }

    @Override
    public int getCount() {
        return this.items.size();
    }

    @Override
    public InventoryCountListModel getItem(int position) {
        return position <= this.items.size() - 1 ? this.items.get(position) : null;
    }

    @Override
    public void setContentView(View contentView, int position, HorizontalScrollView parent) {
        InventoryCountListModel item = getItem(position);

        //盘点单号
        TextView tvCycleCountId = (TextView) contentView.findViewById(R.id.act_inventory_count_item_cycle_count_id);
        tvCycleCountId.setText(Long.toString(item.getId()));
        //盘点方式
        TextView tvCycleCountType = (TextView) contentView.findViewById(R.id.act_inventory_count_item_type_name);
        tvCycleCountType.setText(item.getTypeName());
        //盘点类型
        TextView tvCountMode = (TextView) contentView.findViewById(R.id.act_inventory_count_item_mode_name);
        tvCountMode.setText(item.getModeName());
        //盘点比例
        TextView tvCountPercent = (TextView) contentView.findViewById(R.id.act_inventory_count_item_count_percent);
        tvCountPercent.setText(Integer.toString(item.getCountPercent()));
        //盘点日期
        TextView tvCountBeginTime = (TextView) contentView.findViewById(R.id.act_inventory_count_item_count_begin_time);
        tvCountBeginTime.setText(item.getCountBeginTime());
        //制单日期
        TextView tvCountCreateDate = (TextView) contentView.findViewById(R.id.act_inventory_count_create_date);
        tvCountCreateDate.setText(item.getCreateDate());
    }

    //实现setActionView方法
    @Override
    public void setActionView(View actionView, final int position, final HorizontalScrollView parent) {
        //盘点
        actionView.findViewById(R.id.act_inventory_count_item_swipe_btn_count).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InventoryCountListModel item = items.get(position);
                if (InventoryCountModeCode.COUNTING.toString().equals(item.getModeCode())) {
                    Intent intent = new Intent(v.getContext(), InventoryCountAmountActivity.class);
                    intent.putExtra("countId", item.getId());
                    intent.putExtra("countType", item.getModeCode());
                    v.getContext().startActivity(intent);
                } else {
                    Intent intent = new Intent(v.getContext(), InventoryCountCommodityActivity.class);
                    intent.putExtra("countId", item.getId());
                    intent.putExtra("countType", item.getModeCode());
                    v.getContext().startActivity(intent);
                }
            }
        });

        //明细
        actionView.findViewById(R.id.act_inventory_count_item_swipe_btn_detail).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InventoryCountListModel item = items.get(position);

                Intent intent = new Intent(v.getContext(), InventoryCountDetailActivity.class);
                intent.putExtra("countType", item.getModeCode());
                intent.putExtra("countId", item.getId());
                v.getContext().startActivity(intent);
            }
        });
    }
}

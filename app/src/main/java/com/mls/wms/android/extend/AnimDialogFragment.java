package com.mls.wms.android.extend;

import android.app.DialogFragment;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.mls.wms.android.R;

/**
 * Created by 2014-400 on 2015-09-18.
 */
public class AnimDialogFragment extends DialogFragment {
    private View createView;
    private ImageView imageView;
    private TextView tvLoading;
    private int resId;
    private String message;

    public int getResId() {
        return resId;
    }

    public void setResId(int resId) {
        this.resId = resId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        setCancelable(false);

        createView = inflater.inflate(R.layout.dg_progress_dialog, container);
        return createView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        tvLoading = (TextView) view.findViewById(R.id.dg_progress_dialog_loadingTv);
        imageView = (ImageView) view.findViewById(R.id.dg_progress_dialog_loadingIv);

        tvLoading.setText(this.message);
        imageView.setBackgroundResource(this.resId);
        if (TextUtils.isEmpty(this.message)) {
            tvLoading.setVisibility(View.GONE);
        }

        AnimationDrawable animation = (AnimationDrawable) imageView.getBackground();
        animation.start();
    }
}
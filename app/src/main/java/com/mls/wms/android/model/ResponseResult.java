package com.mls.wms.android.model;

import com.mls.wms.android.util.JSONUtils;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.List;

/**
 * Created by 2014-400 on 2015-08-13.
 */
public class ResponseResult implements IModel, Serializable {

    private static final long serialVersionUID = 1L;
    private String code;
    private Boolean suc = true;
    private String message;
    private String result;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Boolean getSuc() {
        return suc;
    }

    public void setSuc(Boolean suc) {
        this.suc = suc;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    @Override
    public ResponseResult parseJson(String json) throws Exception {
        ResponseResult responseResult = new ResponseResult();

        JSONObject object = new JSONObject(json);

        responseResult.setCode(JSONUtils.getString(object, "code", ""));
        responseResult.setMessage(JSONUtils.getString(object, "message", ""));
        responseResult.setResult(JSONUtils.getString(object, "result", ""));
        responseResult.setSuc(JSONUtils.getBoolean(object, "suc", false));


        return responseResult;
    }

    @Override
    public List<ResponseResult> parseJsons(String json) throws Exception {
        return null;
    }
}


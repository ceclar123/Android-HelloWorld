package com.mls.wms.android.constant;

import android.graphics.Color;

import com.mls.wms.android.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 2014-400 on 2015-09-22.
 */
public class DrawColorUtil {
    private DrawColorUtil() {
    }

    public static final int[] COLOR_1 = new int[]{R.color.menu_01};
    public static final int[] COLOR_2 = new int[]{R.color.menu_01, R.color.menu_04};
    public static final int[] COLOR_3 = new int[]{R.color.menu_01, R.color.menu_06, R.color.menu_04};
    public static final int[] COLOR_4 = new int[]{R.color.menu_01, R.color.menu_06, R.color.menu_04, R.color.menu_05};
    public static final int[] COLOR_5 = new int[]{R.color.menu_01, R.color.menu_06, R.color.menu_04, R.color.menu_05, R.color.menu_01};
    public static final int[] COLOR_6 = new int[]{R.color.menu_01, R.color.menu_06, R.color.menu_02, R.color.menu_04, R.color.menu_05, R.color.menu_01};
    public static final int[] COLOR_7 = new int[]{R.color.menu_01, R.color.menu_06, R.color.menu_02, R.color.menu_04, R.color.menu_05, R.color.menu_01, R.color.menu_11};

    public static final int[] VORDIPLOM_COLORS = {
            Color.rgb(192, 255, 140), Color.rgb(255, 247, 140), Color.rgb(255, 208, 140),
            Color.rgb(140, 234, 255), Color.rgb(255, 140, 157), Color.rgb(73, 199, 224),
            Color.rgb(51, 182, 154), Color.rgb(142, 206, 98), Color.rgb(251, 180, 59)
    };

    public static int[] getColor(int size) {
        switch (size) {
            case 1:
                return COLOR_1;
            case 2:
                return COLOR_2;
            case 3:
                return COLOR_3;
            case 4:
                return COLOR_4;
            case 5:
                return COLOR_5;
            case 6:
                return COLOR_6;
            case 7:
                return COLOR_7;

            default:
                return null;
        }
    }

    public static List<Integer> getColorList(int size) {
        int[] colors = getColor(size);
        if (colors != null) {
            List<Integer> list = new ArrayList<Integer>();
            for (int item : colors) {
                list.add(item);
            }
            return list;
        }

        return null;
    }
}

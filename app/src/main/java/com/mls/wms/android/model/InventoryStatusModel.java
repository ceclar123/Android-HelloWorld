package com.mls.wms.android.model;

import com.mls.wms.android.util.JSONUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by 2014-400 on 2015-09-14.
 */
public class InventoryStatusModel implements IModel, Serializable {
    public long locationId;
    public String inventoryStatusCode;

    public long getLocationId() {
        return locationId;
    }

    public void setLocationId(long locationId) {
        this.locationId = locationId;
    }

    public String getInventoryStatusCode() {
        return inventoryStatusCode;
    }

    public void setInventoryStatusCode(String inventoryStatusCode) {
        this.inventoryStatusCode = inventoryStatusCode;
    }

    @Override
    public InventoryStatusModel parseJson(String json) throws Exception {
        InventoryStatusModel model = new InventoryStatusModel();

        JSONObject object = new JSONObject(json);

        model.setLocationId(JSONUtils.getLong(object, "locationId", 0L));
        model.setInventoryStatusCode(JSONUtils.getString(object, "inventoryStatusCode", ""));

        return model;
    }

    @Override
    public List<InventoryStatusModel> parseJsons(String json) throws Exception {
        List<InventoryStatusModel> list = new ArrayList<InventoryStatusModel>();
        JSONArray array = new JSONArray(json);
        for (int i = 0; i < array.length(); i++) {
            list.add(parseJson(array.getString(i)));
        }
        return list;
    }
}

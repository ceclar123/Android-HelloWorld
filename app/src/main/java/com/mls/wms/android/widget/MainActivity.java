package com.mls.wms.android.widget;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.mls.wms.android.R;
import com.mls.wms.android.extend.BaseActivity;
import com.mls.wms.android.fragment.DrawMenuInvFragment;
import com.mls.wms.android.fragment.DrawMenuInwhFragment;
import com.mls.wms.android.fragment.DrawMenuOtherFragment;
import com.mls.wms.android.fragment.DrawMenuOutwhFragment;
import com.mls.wms.android.fragment.DrawMenuReturnFragment;
import com.mls.wms.android.fragment.DrawMenuTransferFragment;
import com.mls.wms.android.widget.other.SummaryCenterActivity;

/**
 * Created by 2014-400 on 2015-09-09.
 */
public class MainActivity extends BaseActivity {
    private DrawerLayout drawerLayout;
    private FrameLayout drawView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_main);

        this.init();
    }


    private void init() {
        drawerLayout = (DrawerLayout) this.findViewById(R.id.act_main_drawer_layout);
        drawerLayout.setDrawerShadow(R.drawable.bg_layout_line_blue, GravityCompat.END);
        ActionBarDrawerToggle drawerToggle = new CustomActionBarDrawerToggle(this, drawerLayout, R.string.title_drawer_open, R.string.title_drawer_close);
        drawerLayout.setDrawerListener(drawerToggle);

        drawView = (FrameLayout) this.findViewById(R.id.act_main_draw_view);

        TextView tvLeft00 = (TextView) this.findViewById(R.id.act_main_tv_00_left);
        TextView tvLeft01 = (TextView) this.findViewById(R.id.act_main_tv_01_left);
        TextView tvLeft02 = (TextView) this.findViewById(R.id.act_main_tv_02_left);
        TextView tvLeft03 = (TextView) this.findViewById(R.id.act_main_tv_03_left);
        TextView tvLeft04 = (TextView) this.findViewById(R.id.act_main_tv_04_left);
        TextView tvLeft05 = (TextView) this.findViewById(R.id.act_main_tv_05_left);
        TextView tvLeft06 = (TextView) this.findViewById(R.id.act_main_tv_06_left);

        tvLeft00.setTypeface(_BaseApplication.getIconFont());
        tvLeft01.setTypeface(_BaseApplication.getIconFont());
        tvLeft02.setTypeface(_BaseApplication.getIconFont());
        tvLeft03.setTypeface(_BaseApplication.getIconFont());
        tvLeft04.setTypeface(_BaseApplication.getIconFont());
        tvLeft05.setTypeface(_BaseApplication.getIconFont());
        tvLeft06.setTypeface(_BaseApplication.getIconFont());

        TextView tvRight00 = (TextView) this.findViewById(R.id.act_main_tv_00_right);
        TextView tvRight01 = (TextView) this.findViewById(R.id.act_main_tv_01_right);
        TextView tvRight02 = (TextView) this.findViewById(R.id.act_main_tv_02_right);
        TextView tvRight03 = (TextView) this.findViewById(R.id.act_main_tv_03_right);
        TextView tvRight04 = (TextView) this.findViewById(R.id.act_main_tv_04_right);
        TextView tvRight05 = (TextView) this.findViewById(R.id.act_main_tv_05_right);
        TextView tvRight06 = (TextView) this.findViewById(R.id.act_main_tv_06_right);

        tvRight00.setTypeface(_BaseApplication.getIconFont());
        tvRight01.setTypeface(_BaseApplication.getIconFont());
        tvRight02.setTypeface(_BaseApplication.getIconFont());
        tvRight03.setTypeface(_BaseApplication.getIconFont());
        tvRight04.setTypeface(_BaseApplication.getIconFont());
        tvRight05.setTypeface(_BaseApplication.getIconFont());
        tvRight06.setTypeface(_BaseApplication.getIconFont());

        TextView tvMiddle00 = (TextView) this.findViewById(R.id.act_main_tv_00_middle);
        TextView tvMiddle01 = (TextView) this.findViewById(R.id.act_main_tv_01_middle);
        TextView tvMiddle02 = (TextView) this.findViewById(R.id.act_main_tv_02_middle);
        TextView tvMiddle03 = (TextView) this.findViewById(R.id.act_main_tv_03_middle);
        TextView tvMiddle04 = (TextView) this.findViewById(R.id.act_main_tv_04_middle);
        TextView tvMiddle05 = (TextView) this.findViewById(R.id.act_main_tv_05_middle);
        TextView tvMiddle06 = (TextView) this.findViewById(R.id.act_main_tv_06_middle);

        tvMiddle00.setOnClickListener(myClickListener);
        tvMiddle01.setOnClickListener(myClickListener);
        tvMiddle02.setOnClickListener(myClickListener);
        tvMiddle03.setOnClickListener(myClickListener);
        tvMiddle04.setOnClickListener(myClickListener);
        tvMiddle05.setOnClickListener(myClickListener);
        tvMiddle06.setOnClickListener(myClickListener);
    }

    private View.OnClickListener myClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (true == drawerLayout.isDrawerOpen(GravityCompat.END)) {
                drawerLayout.closeDrawer(GravityCompat.END);
                //drawView.setVisibility(View.GONE);
            } else {
                //drawView.setVisibility(View.VISIBLE);
                if (v.getId() == R.id.act_main_tv_00_middle) {
                    Intent intent = new Intent();
                    intent.setClass(v.getContext(), SummaryCenterActivity.class);
                    startActivity(intent);
                    return;
                } else if (v.getId() == R.id.act_main_tv_01_middle) {
                    showDrawMenu(1);
                } else if (v.getId() == R.id.act_main_tv_02_middle) {
                    showDrawMenu(2);
                } else if (v.getId() == R.id.act_main_tv_03_middle) {
                    showDrawMenu(3);
                } else if (v.getId() == R.id.act_main_tv_04_middle) {
                    showDrawMenu(4);
                } else if (v.getId() == R.id.act_main_tv_05_middle) {
                    showDrawMenu(5);
                } else if (v.getId() == R.id.act_main_tv_06_middle) {
                    showDrawMenu(6);
                }
                drawerLayout.openDrawer(GravityCompat.END);
            }
        }
    };

    private void showDrawMenu(int type) {
        FragmentManager fm = null;
        FragmentTransaction ft = null;
        Fragment fragment = null;
        switch (type) {
            case 1:
                fm = getFragmentManager();
                ft = fm.beginTransaction();
                fragment = new DrawMenuInwhFragment();
                ft.replace(R.id.act_main_draw_view, fragment, this.getClass().toString());
                ft.commit();
                break;

            case 2:
                fm = getFragmentManager();
                ft = fm.beginTransaction();
                fragment = new DrawMenuOutwhFragment();
                ft.replace(R.id.act_main_draw_view, fragment, this.getClass().toString());
                ft.commit();
                break;

            case 3:
                fm = getFragmentManager();
                ft = fm.beginTransaction();
                fragment = new DrawMenuInvFragment();
                ft.replace(R.id.act_main_draw_view, fragment, this.getClass().toString());
                ft.commit();
                break;

            case 4:
                fm = getFragmentManager();
                ft = fm.beginTransaction();
                fragment = new DrawMenuReturnFragment();
                ft.replace(R.id.act_main_draw_view, fragment, this.getClass().toString());
                ft.commit();
                break;

            case 5:
                fm = getFragmentManager();
                ft = fm.beginTransaction();
                fragment = new DrawMenuTransferFragment();
                ft.replace(R.id.act_main_draw_view, fragment, this.getClass().toString());
                ft.commit();
                break;

            case 6:
                fm = getFragmentManager();
                ft = fm.beginTransaction();
                fragment = new DrawMenuOtherFragment();
                ft.replace(R.id.act_main_draw_view, fragment, this.getClass().toString());
                ft.commit();
                break;


            default:
                break;
        }
    }

    @Override
    public void tv_back_click() {
        //
    }

    @Override
    public int getTopNameResId() {
        return R.string.title_text_navigation;
    }

    @Override
    public boolean getBackVisible() {
        return false;
    }

    @Override
    public boolean getScannerVisible() {
        return false;
    }


    private class CustomActionBarDrawerToggle extends ActionBarDrawerToggle {
        public CustomActionBarDrawerToggle(Activity activity, DrawerLayout drawerLayout, int openDrawerContentDescRes, int closeDrawerContentDescRes) {
            super(activity, drawerLayout, openDrawerContentDescRes, closeDrawerContentDescRes);
        }

        @Override
        public void onDrawerOpened(View drawerView) {
            super.onDrawerOpened(drawerView);
            invalidateOptionsMenu();
        }

        @Override
        public void onDrawerClosed(View drawerView) {
            super.onDrawerClosed(drawerView);
            invalidateOptionsMenu();
        }
    }
}

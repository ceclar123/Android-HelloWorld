package com.mls.wms.android.widget;

import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.Toast;

import com.mls.wms.android.R;
import com.mls.wms.android.extend.BaseActivity;
import com.mls.wms.android.model.Menu;
import com.mls.wms.android.util.MenuUtil;
import com.mls.wms.android.util.WindowUtil;

import java.util.List;


public class MainMenuActivity extends BaseActivity {
    public final int COLUMN_COUNT = 3;
    private int errorCount = 0;
    private GridLayout mainMenuGrid;
    private int pid = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_menu);

        mainMenuGrid = (GridLayout) this.findViewById(R.id.mainMenuGrid);


        this.init();
    }


    private void init() {
        try {
            Intent intent = getIntent();
            if (intent != null) {
                pid = intent.getIntExtra("pid", 0);
            }
            List<Menu> list = MenuUtil.getMenuByPid(this, pid);
            Point point = WindowUtil.getMetrics(this);

            int width = point.x / COLUMN_COUNT;
            int height = width;

            int rowCount = list.size() % COLUMN_COUNT == 0 ? list.size() / COLUMN_COUNT : list.size() / COLUMN_COUNT + 1;
            mainMenuGrid.setRowCount(rowCount);
            mainMenuGrid.setColumnCount(COLUMN_COUNT);

            for (int i = 0; i < rowCount; i++) {
                for (int j = 0; j < COLUMN_COUNT; j++) {
                    if (i * COLUMN_COUNT + j >= list.size()) {
                        break;
                    }

                    Menu menu = list.get(i * COLUMN_COUNT + j);

                    Button btn = new Button(this);
                    btn.setWidth(width);
                    btn.setHeight(height);
                    btn.setText(menu.getTitle());
                    btn.setTextAppearance(this, R.style.wms_button_middle);
                    btn.setTag(menu);
                    btn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            //Toast.makeText(MainMenu.this, "Hello", Toast.LENGTH_SHORT).show();
                            Menu menu = (Menu) v.getTag();
                            if (false == TextUtils.isEmpty(menu.getViewClass())) {
                                Intent intent = new Intent(Intent.ACTION_VIEW);
                                intent.setClassName(MainMenuActivity.this, menu.getViewClass());
                                intent.putExtra("pid", menu.getId());

                                MainMenuActivity.this.startActivity(intent);
                            }
                        }
                    });

                    //行列坐标
                    GridLayout.Spec rowSpec = GridLayout.spec(i);
                    GridLayout.Spec columnSpec = GridLayout.spec(j);
                    GridLayout.LayoutParams params = new GridLayout.LayoutParams(rowSpec, columnSpec);
                    params.setGravity(Gravity.CENTER_HORIZONTAL);

                    mainMenuGrid.addView(btn, params);
                }
            }

        } catch (Exception e) {
            Log.e(MainMenuActivity.class.toString(), e.getMessage());
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public String getBtnRightName() {
        return pid == 0 ? getResources().getString(R.string.btn_text_quit) : getResources().getString(R.string.btn_text_back);
    }

}
package com.mls.wms.android.constant;

/**
 * Created by 2014-400 on 2015-09-24.
 */
public enum InventoryCountModeCode {
    COUNTING("counting", "库位清点"), SCAN("scan", "扫描");

    private String value;
    private String cnValue;

    private InventoryCountModeCode(String value, String cnValue) {
        this.value = value;
        this.cnValue = cnValue;
    }

    public String toString() {
        return this.value;
    }

    public String toCn() {
        return this.cnValue;
    }
}

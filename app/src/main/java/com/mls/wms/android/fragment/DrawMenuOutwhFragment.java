package com.mls.wms.android.fragment;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mls.wms.android.R;
import com.mls.wms.android.extend.BaseApplication;
import com.mls.wms.android.widget.outwh.PickAndSortListActivity;
import com.mls.wms.android.widget.outwh.PickToSortListActivity;
import com.mls.wms.android.widget.outwh.TransferActivity;

/**
 * Created by 2014-400 on 2015-09-11.
 */
public class DrawMenuOutwhFragment extends Fragment {
    private View createVview = null;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        createVview = inflater.inflate(R.layout.fg_draw_menu_outwh, container, false);
        return createVview;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        final BaseApplication baseApplication = (BaseApplication) getActivity().getApplication();

        LinearLayout layoutPick = (LinearLayout) view.findViewById(R.id.fg_draw_menu_outwh_layout_pick);
        RelativeLayout layoutPickAndSort = (RelativeLayout) view.findViewById(R.id.fg_draw_menu_outwh_layout_pick_and_sort);
        RelativeLayout layoutPickToSort = (RelativeLayout) view.findViewById(R.id.fg_draw_menu_outwh_layout_pick_to_sort);
        RelativeLayout layoutPickOrder = (RelativeLayout) view.findViewById(R.id.fg_draw_menu_outwh_layout_pick_order);

        LinearLayout layoutRepick = (LinearLayout) view.findViewById(R.id.fg_draw_menu_outwh_layout_repick);
        LinearLayout layoutPacking = (LinearLayout) view.findViewById(R.id.fg_draw_menu_outwh_layout_packing);
        LinearLayout layoutTransfer = (LinearLayout) view.findViewById(R.id.fg_draw_menu_outwh_layout_transfer);

        TextView tvLogo = (TextView) view.findViewById(R.id.fg_draw_menu_outwh_tv_logo);
        tvLogo.setTypeface(baseApplication.getIconFont());

        //拣货
        if (baseApplication.getGlobalSetting().getModuleService().indexOf(getResources().getString(R.string.MODULE_PICK_AND_SORT)) > -1 ||
                baseApplication.getGlobalSetting().getModuleService().indexOf(getResources().getString(R.string.MODULE_PICK_TO_SORT)) > -1 ||
                baseApplication.getGlobalSetting().getModuleService().indexOf(getResources().getString(R.string.MODULE_PICK_ORDER)) > -1) {
            TextView tvImg1 = (TextView) view.findViewById(R.id.fg_draw_menu_outwh_tv_pick_img);
            tvImg1.setTypeface(baseApplication.getIconFont());

            //边拣边分
            if (baseApplication.getGlobalSetting().getModuleService().indexOf(getResources().getString(R.string.MODULE_PICK_AND_SORT)) > -1) {
                TextView tvPickAndSort = (TextView) view.findViewById(R.id.fg_draw_menu_outwh_tv_pick_and_sort);
                tvPickAndSort.setOnClickListener(myClickListener);
            } else {
                layoutPickAndSort.setVisibility(View.GONE);
            }
            //先拣后分
            if (baseApplication.getGlobalSetting().getModuleService().indexOf(getResources().getString(R.string.MODULE_PICK_TO_SORT)) > -1) {
                TextView tvPickToSort = (TextView) view.findViewById(R.id.fg_draw_menu_outwh_tv_pick_to_sort);
                tvPickToSort.setOnClickListener(myClickListener);
            } else {
                layoutPickToSort.setVisibility(View.GONE);
            }
            //按单拣货
            if (baseApplication.getGlobalSetting().getModuleService().indexOf(getResources().getString(R.string.MODULE_PICK_ORDER)) > -1) {
                TextView tvPickOrder = (TextView) view.findViewById(R.id.fg_draw_menu_outwh_tv_pick_order);
                tvPickOrder.setOnClickListener(myClickListener);
            } else {
                layoutPickOrder.setVisibility(View.GONE);
            }

        } else {
            layoutPick.setVisibility(View.GONE);
        }

        //二次分拣
        if (baseApplication.getGlobalSetting().getModuleService().indexOf(getResources().getString(R.string.MODULE_REPICK)) > -1) {
            TextView tvImg2 = (TextView) view.findViewById(R.id.fg_draw_menu_outwh_tv_repick_img);
            tvImg2.setTypeface(baseApplication.getIconFont());

            TextView tvRepick = (TextView) view.findViewById(R.id.fg_draw_menu_outwh_tv_repick);
            tvRepick.setOnClickListener(myClickListener);
        } else {
            layoutRepick.setVisibility(View.GONE);
        }
        //出库装箱
        if (baseApplication.getGlobalSetting().getModuleService().indexOf(getResources().getString(R.string.MODULE_REPICK)) > -1) {
            TextView tvImg3 = (TextView) view.findViewById(R.id.fg_draw_menu_outwh_tv_package_img);
            tvImg3.setTypeface(baseApplication.getIconFont());

            TextView tvPackage = (TextView) view.findViewById(R.id.fg_draw_menu_outwh_tv_package);
            tvPackage.setOnClickListener(myClickListener);
        } else {
            layoutPacking.setVisibility(View.GONE);
        }
        //出库交接
        if (baseApplication.getGlobalSetting().getModuleService().indexOf(getResources().getString(R.string.MODULE_OUT_DELIVERY)) > -1) {
            TextView tvImg4 = (TextView) view.findViewById(R.id.fg_draw_menu_outwh_tv_transfer_img);
            tvImg4.setTypeface(baseApplication.getIconFont());

            TextView tvTransfer = (TextView) view.findViewById(R.id.fg_draw_menu_outwh_tv_transfer);
            tvTransfer.setOnClickListener(myClickListener);
        } else {
            layoutTransfer.setVisibility(View.GONE);
        }
    }


    private View.OnClickListener myClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v.getId() == R.id.fg_draw_menu_outwh_tv_pick_and_sort) {
                Intent intent = new Intent();
                intent.setClass(v.getContext(), PickAndSortListActivity.class);
                startActivity(intent);
            } else if (v.getId() == R.id.fg_draw_menu_outwh_tv_pick_to_sort) {
                Intent intent = new Intent();
                intent.setClass(v.getContext(), PickToSortListActivity.class);
                startActivity(intent);
            } else if (v.getId() == R.id.fg_draw_menu_outwh_tv_pick_order) {
                //
            } else if (v.getId() == R.id.fg_draw_menu_outwh_tv_repick) {
                //
            } else if (v.getId() == R.id.fg_draw_menu_outwh_tv_package) {
                //
            } else if (v.getId() == R.id.fg_draw_menu_outwh_tv_transfer) {
                Intent intent = new Intent();
                intent.setClass(v.getContext(), TransferActivity.class);
                startActivity(intent);
            }
        }
    };
}
package com.mls.wms.android.extend;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.mls.wms.android.R;

/**
 * Created by 2014-400 on 2015-09-17.
 */
public class AnimProgressDialog extends ProgressDialog {

    private AnimationDrawable animation;
    private Context context;
    private ImageView imageView;
    private TextView tvLoading;
    private String loadingTip;
    private int count = 0;
    private String oldLoadingTip;
    private int resId;

    public AnimProgressDialog(Context context, String content) {
        super(context);
        this.context = context;
        this.loadingTip = content;
        this.resId = R.drawable.anim_progress_dialog;

        setCancelable(false);
        setCanceledOnTouchOutside(true);
    }

    /**
     * AnimProgressDialog
     *
     * @param context
     * @param content 加载文字
     * @param id      动画资源ID
     */
    public AnimProgressDialog(Context context, String content, int id) {
        super(context);
        this.context = context;
        this.loadingTip = content;
        this.resId = id;

        setCancelable(false);
        setCanceledOnTouchOutside(true);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dg_progress_dialog);
        tvLoading = (TextView) findViewById(R.id.dg_progress_dialog_loadingTv);
        imageView = (ImageView) findViewById(R.id.dg_progress_dialog_loadingIv);

        initData();
    }

    private void initData() {
        tvLoading.setText(loadingTip);

        imageView.setBackgroundResource(resId);
        animation = (AnimationDrawable) imageView.getBackground();
    }

    public void setContent(String str) {
        tvLoading.setText(str);
    }

    @Override
    public void show() {
        super.show();
        animation.start();
    }

    @Override
    public void cancel() {
        super.cancel();
        if (animation != null && animation.isRunning()) {
            animation.stop();
        }
    }

    @Override
    public void dismiss() {
        super.dismiss();
        if (animation != null && animation.isRunning()) {
            animation.stop();
        }
    }
}
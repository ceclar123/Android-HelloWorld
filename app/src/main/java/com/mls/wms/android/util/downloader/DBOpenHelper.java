package com.mls.wms.android.util.downloader;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * SQLite管理器，实现创建数据库和表，但版本变化时实现对表的数据库表的操作
 */
public class DBOpenHelper extends SQLiteOpenHelper {
    //设置数据库的名称
    private static final String DB_NAME = "downloader.db";
    //设置数据库的版本
    private static final int VERSION = 1;

    /**
     * 通过构造方法
     *
     * @param context
     */
    public DBOpenHelper(Context context) {
        super(context, DB_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS downloadlog (id integer primary key autoincrement, downpath varchar(100), threadid INTEGER, downlength INTEGER)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS downloadlog");
        onCreate(db);
    }

}

package com.mls.wms.android.fragment;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mls.wms.android.R;
import com.mls.wms.android.extend.BaseApplication;
import com.mls.wms.android.widget.other.DeliveryProgressActivity;
import com.mls.wms.android.widget.other.SummaryCenterActivity;

/**
 * Created by 2014-400 on 2015-09-11.
 */
public class DrawMenuOtherFragment extends Fragment {
    private View createView = null;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        createView = inflater.inflate(R.layout.fg_draw_menu_other, container, false);
        return createView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        final BaseApplication baseApplication = (BaseApplication) getActivity().getApplication();

        LinearLayout layoutBindBox = (LinearLayout) view.findViewById(R.id.fg_draw_menu_other_layout_bind_box);
        LinearLayout layoutSummaryCenter = (LinearLayout) view.findViewById(R.id.fg_draw_menu_other_layout_summary_center);
        LinearLayout layoutDeliveryProgress = (LinearLayout) view.findViewById(R.id.fg_draw_menu_other_layout_delivery_progress);

        TextView tvLogo = (TextView) view.findViewById(R.id.fg_draw_menu_other_tv_logo);
        tvLogo.setTypeface(baseApplication.getIconFont());

        //绑定周转箱
        if (baseApplication.getGlobalSetting().getModuleService().indexOf(getResources().getString(R.string.MODULE_BIND_BOX)) > -1) {
            TextView tvImg1 = (TextView) view.findViewById(R.id.fg_draw_menu_other_tv_bind_car_img);
            tvImg1.setTypeface(baseApplication.getIconFont());

            TextView tvBindCar = (TextView) view.findViewById(R.id.fg_draw_menu_other_tv_bind_car);
            tvBindCar.setOnClickListener(myClickListener);
        } else {
            layoutBindBox.setVisibility(View.GONE);
        }
        //汇总中心
        if (1 == 1) {
            TextView tvImg2 = (TextView) view.findViewById(R.id.fg_draw_menu_other_tv_summary_center_img);
            tvImg2.setTypeface(baseApplication.getIconFont());

            TextView tvSummaryCenter = (TextView) view.findViewById(R.id.fg_draw_menu_other_tv_summary_center);
            tvSummaryCenter.setOnClickListener(myClickListener);
        } else {
            layoutSummaryCenter.setVisibility(View.GONE);
        }
        //发货进度
        if (1 == 1) {
            TextView tvImg3 = (TextView) view.findViewById(R.id.fg_draw_menu_other_tv_delivery_progress_img);
            tvImg3.setTypeface(baseApplication.getIconFont());

            TextView tvDeliveryProgress = (TextView) view.findViewById(R.id.fg_draw_menu_other_tv_delivery_progress);
            tvDeliveryProgress.setOnClickListener(myClickListener);
        } else {
            layoutDeliveryProgress.setVisibility(View.GONE);
        }
    }


    private View.OnClickListener myClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v.getId() == R.id.fg_draw_menu_other_tv_bind_car) {
                //
            } else if (v.getId() == R.id.fg_draw_menu_other_tv_summary_center) {
                Intent intent = new Intent();
                intent.setClass(v.getContext(), SummaryCenterActivity.class);
                startActivity(intent);
            } else if (v.getId() == R.id.fg_draw_menu_other_tv_delivery_progress) {
                Intent intent = new Intent();
                intent.setClass(v.getContext(), DeliveryProgressActivity.class);
                startActivity(intent);
            }
        }
    };
}

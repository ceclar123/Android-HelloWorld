package com.mls.wms.android.widget.inwh;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.http.RequestManager;
import com.google.zxing.client.android.Intents;
import com.mls.wms.android.R;
import com.mls.wms.android.constant.ScanTypeConstants;
import com.mls.wms.android.constant.ServicePort;
import com.mls.wms.android.extend.AnimDialogFragment;
import com.mls.wms.android.extend.BaseActivity;
import com.mls.wms.android.model.PutAwayAdviceModel;
import com.mls.wms.android.model.ResponseResult;
import com.mls.wms.android.util.ViewUtils;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by 2014-400 on 2015-09-16.
 */
public class RevokePutActivity extends BaseActivity {
    private long toLocationId = 0L;
    private long skuId = 0L;
    private int availableQty = 0;

    private EditText etScanBarcode = null;
    private EditText etInventoryStatus = null;
    private EditText etItemName = null;
    private EditText etRecommendLocationNo = null;
    private EditText etToLocationNo = null;
    private EditText etPutQty = null;

    private LinearLayout layoutQty = null;

    private Button btnContinue = null;
    private EditText selectEt = null;
    private AnimDialogFragment progressDialog = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_revoke_put);

        //弹出框
        progressDialog = new AnimDialogFragment();
        progressDialog.setResId(R.drawable.anim_progress_dialog);

        this.etScanBarcode = (EditText) this.findViewById(R.id.act_revoke_put_et_scan_barcode);
        this.etInventoryStatus = (EditText) this.findViewById(R.id.act_revoke_put_et_inventory_status);
        this.etItemName = (EditText) this.findViewById(R.id.act_revoke_put_et_spec_item_name);
        this.etRecommendLocationNo = (EditText) this.findViewById(R.id.act_revoke_put_et_recommend_location_no);
        this.etToLocationNo = (EditText) this.findViewById(R.id.act_revoke_put_et_real_location_no);
        this.etPutQty = (EditText) this.findViewById(R.id.act_revoke_put_et_put_qty);
        this.etPutQty.setText("1");

        this.layoutQty = (LinearLayout) this.findViewById(R.id.act_revoke_put_layout_qty);
        this.btnContinue = (Button) this.findViewById(R.id.act_revoke_put_btn_continue);


        //输入数量
        if (0 == _BaseApplication.getGlobalSetting().getIsMultiScan()) {
            layoutQty.setVisibility(View.GONE);
            etPutQty.setVisibility(View.GONE);
        }

        //文本框事件
        List<EditText> ets = ViewUtils.getDescendants(this, EditText.class, false);
        for (final EditText item : ets) {
            if (item.isEnabled() == true && item.getVisibility() == View.VISIBLE) {
                item.setSelectAllOnFocus(true);//获取焦点全选
                item.setOnKeyListener(new View.OnKeyListener() {
                    @Override
                    public boolean onKey(View v, int keyCode, KeyEvent event) {
                        if (keyCode == KeyEvent.KEYCODE_ENTER && event.getAction() == KeyEvent.ACTION_DOWN) {
                            return onEnterDownEditText(v);
                        }

                        return false;
                    }
                });
                item.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                    @Override
                    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                        if (actionId == EditorInfo.IME_ACTION_DONE) {
                            return onEnterDownEditText(v);
                        }
                        return false;
                    }
                });

                item.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View v, boolean hasFocus) {
                        if (true == hasFocus) {
                            selectEt = item;
                        }
                    }
                });
            }
        }

        //按钮事件
        this.btnContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doSubmit();
            }
        });

    }

    /**
     * 按下回车
     *
     * @param view
     * @return
     */
    private boolean onEnterDownEditText(View view) {
        if (view.getId() == this.etScanBarcode.getId()) {
            GetAvailableQty();
            return true;
        } else if (view.getId() == this.etToLocationNo.getId()) {
            isValidToLocation();
            return true;
        } else if (view.getId() == this.etPutQty.getId()) {
            isValidPutQty();
            return true;
        }

        return false;
    }

    private void GetAvailableQty() {
        if (TextUtils.isEmpty(this.etScanBarcode.getText().toString())) {
            showDialog(getResources().getString(R.string.dialog_msg_item_barcode_not_null), this.etScanBarcode);
        } else {
            Map<String, String> data = new HashMap<String, String>();
            data.put("barcode", this.etScanBarcode.getText().toString().trim());

            get(ServicePort.getUrl(ServicePort.PiecePutAdvice), data, new RequestManager.RequestListener() {
                @Override
                public void onRequest() {

                }

                @Override
                public void onSuccess(String s, Map<String, String> stringStringMap, String s2, int i) {
                    String errorMsg = null;
                    try {
                        ResponseResult result = new ResponseResult();
                        result = result.parseJson(s);
                        if (result != null && result.getSuc() == true) {
                            PutAwayAdviceModel model = new PutAwayAdviceModel();
                            model = model.parseJson(result.getResult());

                            skuId = model.getSkuId();
                            availableQty = model.getQty();

                            etItemName.setText(model.getItemName());
                            etInventoryStatus.setText(model.getInventoryStatusName());
                            etRecommendLocationNo.setText(model.getLocationNo());

                            etToLocationNo.requestFocus();
                        } else {
                            errorMsg = result == null ? getResources().getString(R.string.title_text_operate_fail) : result.getMessage();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        errorMsg = e.getMessage();
                    }

                    if (false == TextUtils.isEmpty(errorMsg)) {
                        showDialog(errorMsg, selectEt);
                    }
                }

                @Override
                public void onError(String s, String s2, int i) {
                    showDialog(s, selectEt);
                }
            }, 1);
        }
    }

    private void isValidToLocation() {
        if (true == TextUtils.isEmpty(this.etToLocationNo.getText().toString())) {
            showDialog(getResources().getString(R.string.dialog_msg_real_location_no_not_null), etToLocationNo);
        } else {
            String url = ServicePort.getUrl(ServicePort.PiecePutLocation).replace("{restfulid0}", this.etToLocationNo.getText().toString().trim());
            get(url, null, new RequestManager.RequestListener() {
                @Override
                public void onRequest() {

                }

                @Override
                public void onSuccess(String s, Map<String, String> stringStringMap, String s2, int i) {
                    String errorMsg = null;
                    try {
                        ResponseResult result = new ResponseResult();
                        result = result.parseJson(s);
                        if (result != null && result.getSuc() == true) {
                            toLocationId = Long.parseLong(result.getResult());

                            if (layoutQty.getVisibility() == View.VISIBLE) {
                                etPutQty.requestFocus();
                            } else {
                                doSubmit();
                            }
                        } else {
                            errorMsg = result == null ? getResources().getString(R.string.title_text_operate_fail) : result.getMessage();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        errorMsg = e.getMessage();
                    }

                    if (false == TextUtils.isEmpty(errorMsg)) {
                        showDialog(errorMsg, etToLocationNo);
                    }
                }

                @Override
                public void onError(String s, String s2, int i) {
                    showDialog(s, etToLocationNo);
                }
            }, 1);
        }
    }


    private void isValidPutQty() {
        if (TextUtils.isEmpty(this.etPutQty.getText().toString())) {
            showDialog(getResources().getString(R.string.dialog_msg_put_qty_not_null), etPutQty);
        } else {
            int moveQty = Integer.parseInt(this.etPutQty.getText().toString().trim());
            if (moveQty <= 0) {
                showDialog(getResources().getString(R.string.dialog_msg_put_qty_smaller), etPutQty);
            } else if (moveQty > this.availableQty) {
                showDialog(getResources().getString(R.string.dialog_msg_put_qty_bigger), etPutQty);
            } else {
                doSubmit();
            }
        }
    }

    private boolean checkSubmit() {
        if (this.skuId == 0L) {
            showDialog(getResources().getString(R.string.dialog_msg_item_barcode_not_null), this.etScanBarcode);
            return false;
        } else if (this.toLocationId == 0L) {
            showDialog(getResources().getString(R.string.dialog_msg_real_location_no_not_null), this.etToLocationNo);
            return false;
        } else if (TextUtils.isEmpty(this.etPutQty.getText().toString())) {
            showDialog(getResources().getString(R.string.dialog_msg_put_qty_not_null), etPutQty);
        } else {
            int moveQty = Integer.parseInt(this.etPutQty.getText().toString().trim());
            if (moveQty <= 0) {
                showDialog(getResources().getString(R.string.dialog_msg_put_qty_smaller), this.etPutQty);
                return false;
            } else if (moveQty > this.availableQty) {
                showDialog(getResources().getString(R.string.dialog_msg_put_qty_bigger), this.etPutQty);
                return false;
            } else {
                //
            }
        }


        return true;
    }


    private void doSubmit() {
        if (true == this.checkSubmit()) {
            progressDialog.show(getFragmentManager(), this.toString());
            JSONObject data = null;
            try {
                data = new JSONObject();
                data.put("warehouseId", _BaseApplication.getGlobalSetting().getWarehouseId());
                data.put("warehouseNo", _BaseApplication.getGlobalSetting().getWarehouseNo());
                data.put("warehouseName", _BaseApplication.getGlobalSetting().getWarehouseName());
                data.put("toLocationId", this.toLocationId);
                data.put("skuId", this.skuId);
                data.put("qty", this.etPutQty.getText().toString().trim());
            } catch (Exception e) {
                e.printStackTrace();
            }
            post(ServicePort.getUrl(ServicePort.PiecePutInsert), data, new RequestManager.RequestListener() {
                @Override
                public void onRequest() {

                }

                @Override
                public void onSuccess(String s, Map<String, String> stringStringMap, String s2, int i) {
                    String errorMsg = null;
                    try {
                        ResponseResult result = new ResponseResult();
                        result = result.parseJson(s);
                        if (result != null && result.getSuc() == true) {
                            toLocationId = 0L;
                            skuId = 0L;
                            availableQty = 0;

                            etScanBarcode.setText("");
                            etInventoryStatus.setText("");
                            etItemName.setText("");
                            etRecommendLocationNo.setText("");
                            etToLocationNo.setText("");
                            etPutQty.setText("1");

                            etScanBarcode.requestFocus();
                            progressDialog.dismiss();
                            return;
                        } else {
                            errorMsg = result == null ? getResources().getString(R.string.title_text_operate_fail) : result.getMessage();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        errorMsg = e.getMessage();
                    }

                    if (false == TextUtils.isEmpty(errorMsg)) {
                        progressDialog.dismiss();
                        showDialog(errorMsg, etPutQty);
                    }
                }

                @Override
                public void onError(String s, String s2, int i) {
                    progressDialog.dismiss();
                    showDialog(s, etPutQty);
                }
            }, 1);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (this.selectEt != null) {
            switch (requestCode) {
                case ScanTypeConstants.SCAN_BAR_CODE:
                    if (resultCode == RESULT_OK) {
                        Bundle bundle = data.getExtras();
                        this.selectEt.setText(bundle.getString(Intents.Scan.RESULT, ""));
                        this.selectEt.requestFocus();
                        onEnterDownEditText(this.selectEt);
                    }
                default:
                    break;
            }
        }
    }

    @Override
    public int getTopNameResId() {
        return R.string.title_text_revoke_put;
    }
}

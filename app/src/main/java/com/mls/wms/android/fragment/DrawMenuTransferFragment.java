package com.mls.wms.android.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mls.wms.android.R;
import com.mls.wms.android.extend.BaseApplication;

/**
 * Created by 2014-400 on 2015-09-11.
 */
public class DrawMenuTransferFragment extends Fragment {
    private View createView = null;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        createView = inflater.inflate(R.layout.fg_draw_menu_transfer, container, false);
        return createView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        final BaseApplication baseApplication = (BaseApplication) getActivity().getApplication();

        LinearLayout layoutTransferIn = (LinearLayout) view.findViewById(R.id.fg_draw_menu_transfer_layout_transfer_in);
        LinearLayout layoutTransferOut = (LinearLayout) view.findViewById(R.id.fg_draw_menu_transfer_layout_transfer_out);

        TextView tvLogo = (TextView) view.findViewById(R.id.fg_draw_menu_transfer_tv_logo);
        tvLogo.setTypeface(baseApplication.getIconFont());

        //调拨入库
        if (baseApplication.getGlobalSetting().getModuleService().indexOf(getResources().getString(R.string.MODULE_TRANSFER_IN)) > -1) {
            TextView tvImg1 = (TextView) view.findViewById(R.id.fg_draw_menu_transfer_tv_transfer_in_img);
            tvImg1.setTypeface(baseApplication.getIconFont());

            TextView tvTransferIn = (TextView) view.findViewById(R.id.fg_draw_menu_transfer_tv_transfer_in);
            tvTransferIn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //
                }
            });
        } else {
            layoutTransferIn.setVisibility(View.GONE);
        }
        //调拨出库
        if (baseApplication.getGlobalSetting().getModuleService().indexOf(getResources().getString(R.string.MODULE_TRANSFER_OUT)) > -1) {
            TextView tvImg2 = (TextView) view.findViewById(R.id.fg_draw_menu_transfer_tv_transfer_out_img);
            tvImg2.setTypeface(baseApplication.getIconFont());

            TextView tvTransferOut = (TextView) view.findViewById(R.id.fg_draw_menu_transfer_tv_transfer_out);
            tvTransferOut.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //
                }
            });
        } else {
            layoutTransferOut.setVisibility(View.GONE);
        }
    }

}


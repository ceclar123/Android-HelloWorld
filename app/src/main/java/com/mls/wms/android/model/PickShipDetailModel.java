package com.mls.wms.android.model;

import com.mls.wms.android.util.JSONUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by 2014-400 on 2015-09-02.
 */
public class PickShipDetailModel implements IModel, Serializable {
    private int pickUpSeq;
    private String locationNo;
    private String sku;
    private String barcode;
    private int availableQty;
    private int allocatedQty;
    private int pickedQty;
    private String unitName;
    private String itemName;
    private String colorName;
    private String sizeName;
    private String orderIndex;

    public int getPickUpSeq() {
        return pickUpSeq;
    }

    public void setPickUpSeq(int pickUpSeq) {
        this.pickUpSeq = pickUpSeq;
    }

    public String getLocationNo() {
        return locationNo;
    }

    public void setLocationNo(String locationNo) {
        this.locationNo = locationNo;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public int getAvailableQty() {
        return availableQty;
    }

    public void setAvailableQty(int availableQty) {
        this.availableQty = availableQty;
    }

    public int getAllocatedQty() {
        return allocatedQty;
    }

    public void setAllocatedQty(int allocatedQty) {
        this.allocatedQty = allocatedQty;
    }

    public int getPickedQty() {
        return pickedQty;
    }

    public void setPickedQty(int pickedQty) {
        this.pickedQty = pickedQty;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    public String getSizeName() {
        return sizeName;
    }

    public void setSizeName(String sizeName) {
        this.sizeName = sizeName;
    }

    public String getOrderIndex() {
        return orderIndex;
    }

    public void setOrderIndex(String orderIndex) {
        this.orderIndex = orderIndex;
    }


    @Override
    public PickShipDetailModel parseJson(String json) throws Exception {
        PickShipDetailModel model = new PickShipDetailModel();

        JSONObject object = new JSONObject(json);

        model.setPickUpSeq(JSONUtils.getInt(object, "pickUpSeq", 0));
        model.setLocationNo(JSONUtils.getString(object, "locationNo", ""));
        model.setSku(JSONUtils.getString(object, "sku", ""));
        model.setBarcode(JSONUtils.getString(object, "barcode", ""));

        model.setAvailableQty(JSONUtils.getInt(object, "availableQty", 0));
        model.setAllocatedQty(JSONUtils.getInt(object, "allocatedQty", 0));
        model.setPickedQty(JSONUtils.getInt(object, "pickedQty", 0));

        model.setUnitName(JSONUtils.getString(object, "unitName", ""));
        model.setItemName(JSONUtils.getString(object, "itemName", ""));
        model.setColorName(JSONUtils.getString(object, "colorName", ""));
        model.setSizeName(JSONUtils.getString(object, "sizeName", ""));
        model.setOrderIndex(JSONUtils.getString(object, "orderIndex", "0"));

        return model;
    }

    @Override
    public List<PickShipDetailModel> parseJsons(String json) throws Exception {
        List<PickShipDetailModel> list = new ArrayList<PickShipDetailModel>();
        JSONArray array = new JSONArray(json);
        for (int i = 0; i < array.length(); i++) {
            list.add(parseJson(array.getString(i)));
        }
        return list;
    }
}

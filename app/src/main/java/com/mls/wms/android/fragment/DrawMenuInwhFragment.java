package com.mls.wms.android.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mls.wms.android.R;
import com.mls.wms.android.extend.BaseApplication;

/**
 * Created by 2014-400 on 2015-09-11.
 */
public class DrawMenuInwhFragment extends Fragment {
    private View createView = null;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        createView = inflater.inflate(R.layout.fg_draw_menu_inwh, container, false);
        return createView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        final BaseApplication baseApplication = (BaseApplication) getActivity().getApplication();

        LinearLayout layoutAsn = (LinearLayout) view.findViewById(R.id.fg_draw_menu_inwh_layout_asn);
        LinearLayout layoutReceipt = (LinearLayout) view.findViewById(R.id.fg_draw_menu_inwh_layout_receipt);

        TextView tvLogo = (TextView) view.findViewById(R.id.fg_draw_menu_inwh_tv_logo);
        tvLogo.setTypeface(baseApplication.getIconFont());

        //按单收货
        if (baseApplication.getGlobalSetting().getModuleService().indexOf(getResources().getString(R.string.MODULE_ASN)) > -1) {
            TextView tvImg1 = (TextView) view.findViewById(R.id.fg_draw_menu_inwh_tv_asn_img);
            tvImg1.setTypeface(baseApplication.getIconFont());

            TextView tvAsn = (TextView) view.findViewById(R.id.fg_draw_menu_inwh_tv_asn);
            tvAsn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //
                }
            });
        } else {
            layoutAsn.setVisibility(View.GONE);
        }
        //直接收货
        if (baseApplication.getGlobalSetting().getModuleService().indexOf(getResources().getString(R.string.MODULE_RECEIPT)) > -1) {
            TextView tvImg2 = (TextView) view.findViewById(R.id.fg_draw_menu_inwh_tv_receipt_img);
            tvImg2.setTypeface(baseApplication.getIconFont());

            TextView tvReceipt = (TextView) view.findViewById(R.id.fg_draw_menu_inwh_tv_receipt);
            tvReceipt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //
                }
            });
        } else {
            layoutReceipt.setVisibility(View.GONE);
        }


    }

}

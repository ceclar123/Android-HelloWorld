package com.mls.wms.android.model;

import com.mls.wms.android.util.JSONUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Zhuzhenpeng on 2015/9/17.
 */
public class InventoryCountDetailModel implements IModel, Serializable {
    private long id;
    private String locationNo;
    private String statusName;
    private int countQty;
    private String assignUser;
    private String realUser;
    private String updateTime;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLocationNo() {
        return locationNo;
    }

    public void setLocationNo(String locationNo) {
        this.locationNo = locationNo;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public int getCountQty() {
        return countQty;
    }

    public void setCountQty(int countQty) {
        this.countQty = countQty;
    }

    public String getAssignUser() {
        return assignUser;
    }

    public void setAssignUser(String assignUser) {
        this.assignUser = assignUser;
    }

    public String getRealUser() {
        return realUser;
    }

    public void setRealUser(String realUser) {
        this.realUser = realUser;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public InventoryCountDetailModel parseJson(String json) throws Exception {
        InventoryCountDetailModel model = new InventoryCountDetailModel();
        JSONObject object = new JSONObject(json);

        model.setId(JSONUtils.getLong(object, "id", 0L));
        model.setLocationNo(JSONUtils.getString(object, "locationNo", ""));
        model.setStatusName(JSONUtils.getString(object, "statusName", ""));
        model.setCountQty(JSONUtils.getInt(object, "countQty", 0));
        model.setAssignUser(JSONUtils.getString(object, "assignUser", ""));
        model.setRealUser(JSONUtils.getString(object, "realUser", ""));
        model.setUpdateTime(JSONUtils.getString(object, "updateTime", ""));

        return model;
    }

    @Override
    public List<InventoryCountDetailModel> parseJsons(String json) throws Exception {
        List<InventoryCountDetailModel> list = new ArrayList<InventoryCountDetailModel>();
        JSONArray array = new JSONArray(json);
        for (int i = 0; i < array.length(); i++) {
            list.add(parseJson(array.getString(i)));
        }
        return list;
    }
}

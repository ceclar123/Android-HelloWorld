package com.mls.wms.android.model;

import com.mls.wms.android.util.JSONUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by 2014-400 on 2015-11-02.
 */
public class WarehouseModel implements IModel, Serializable {
    private long warehouseId;
    private String warehouseNo;
    private String warehouseName;

    @Override
    public WarehouseModel parseJson(String json) throws Exception {
        WarehouseModel model = new WarehouseModel();
        JSONObject object = new JSONObject(json);

        model.setWarehouseId(JSONUtils.getLong(object, "warehouseId", 0L));
        model.setWarehouseNo(JSONUtils.getString(object, "warehouseNo", ""));
        model.setWarehouseName(JSONUtils.getString(object, "warehouseName", ""));

        return model;
    }

    @Override
    public List<WarehouseModel> parseJsons(String json) throws Exception {
        List<WarehouseModel> list = new ArrayList<WarehouseModel>();
        JSONArray array = new JSONArray(json);
        for (int i = 0; i < array.length(); i++) {
            list.add(parseJson(array.getString(i)));
        }
        return list;
    }

    public long getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(long warehouseId) {
        this.warehouseId = warehouseId;
    }

    public String getWarehouseNo() {
        return warehouseNo;
    }

    public void setWarehouseNo(String warehouseNo) {
        this.warehouseNo = warehouseNo;
    }

    public String getWarehouseName() {
        return warehouseName;
    }

    public void setWarehouseName(String warehouseName) {
        this.warehouseName = warehouseName;
    }
}

package com.mls.wms.android.constant;


/**
 * Created by 2014-400 on 2015-08-13.
 */
public class ServicePort {
    private ServicePort() {
    }

    /// <summary>
    /// 登录
    /// </summary>
    public static final String Login = "Get_RF_Login";
    /**
     * 注销
     */
    public static final String LoginOut = "Get_RF_LoginOut";

    /// <summary>
    /// 修改密码
    /// </summary>
    public static final String Password = "Put_RF_Password";


    /// <summary>
    /// 收货任务列表
    /// </summary>
    public static final String ReceiveList = "Get_RF_ReceiveList";

    /// <summary>
    /// 收货明细列表
    /// </summary>
    public static final String ReceiveDetail = "Get_RF_ReceiveDetail";
    /// <summary>
    /// 收货明细列表(收货完成)
    /// </summary>
    public static final String ReceiveDetailInsert = "Put_RF_ReceiveDetailInsert";

    /// <summary>
    /// 盲收列表
    /// </summary>
    public static final String ReceiptList = "Get_RF_ReceiptList";

    /// <summary>
    /// 盲收明细列表
    /// </summary>
    public static final String ReceiptDetail = "Get_RF_ReceiptDetail";

    /// <summary>
    /// 收货入库查询（歌瑞尔）
    /// </summary>
    public static final String Receive = "Get_RF_Receive";
    /// <summary>
    /// 收货入库插入（歌瑞尔）
    /// </summary>
    public static final String ReceiveInsert = "Post_RF_ReceiveInsert";

    /// <summary>
    /// 收货入库查询（标准版）
    /// </summary>
    public static final String Receive_Standard = "Get_RF_Receive_Standard";
    /// <summary>
    /// 收货入库插入（标准版）
    /// </summary>
    public static final String Receive_StandardInsert = "Post_RF_Receive_StandardInsert";

    /// <summary>
    /// 整库上架查询建议货位
    /// </summary>
    public static final String CartonPutAdvice = "Get_RF_CartonPutAdvice";
    /// <summary>
    /// 整库上架查询指定区域货位
    /// </summary>
    public static final String CartonPutLocation = "Get_RF_CartonPutLocation";
    /// <summary>
    /// 整库上架插入
    /// </summary>
    public static final String CartonPutInsert = "Post_RF_CartonPutInsert";

    /// <summary>
    /// 零仓上架查询建议货位
    /// </summary>
    public static final String PiecePutAdvice = "Get_RF_PiecePutAdvice";
    /// <summary>
    /// 零仓上架查询区域货位
    /// </summary>
    public static final String PiecePutLocation = "Get_RF_PiecePutLocation";
    /// <summary>
    /// 零仓上架插入
    /// </summary>
    public static final String PiecePutInsert = "Post_RF_PiecePutInsert";

    /// <summary>
    /// 库存查询
    /// </summary>
    public static final String Inventory = "Get_RF_Inventory";
    /// <summary>
    /// 查询指定货位库存数量
    /// </summary>
    public static final String SearchInventoryByLocation = "Get_SearchInventoryByLocation";

    /// <summary>
    /// 补货任务列表
    /// </summary>
    public static final String ReplenList = "Get_RF_ReplenList";

    /// <summary>
    /// 补货明细列表
    /// </summary>
    public static final String ReplenDetail = "Get_RF_ReplenDetail";

    /// <summary>
    /// 补货确认完成
    /// </summary>
    public static final String ReplenDetailInsert = "Put_RF_ReplenDetailInsert";

    /// <summary>
    /// 补货下架(此箱号是否有库存记录)
    /// </summary>
    public static final String ReplenPickAdvice = "Get_RF_ReplenPickAdvice";

    /// <summary>
    /// 补货下架(指定区域货位)
    /// </summary>
    public static final String ReplenPickLocation = "Get_RF_ReplenPickLocation";

    /// <summary>
    /// 补货下架(指定货位是否有商品)
    /// </summary>
    public static final String ReplenPickAvailableQty = "Get_RF_ReplenPickAvailableQty";

    /// <summary>
    /// 补货下架(下一笔待下架的记录)
    /// </summary>
    public static final String ReplenPickNextRecord = "Get_RF_ReplenPickNextRecord";
    /// <summary>
    /// 补货下架(插入数据)
    /// </summary>
    public static final String ReplenPickInsert = "Post_RF_ReplenPickInsert";

    /// <summary>
    /// 溢出补货(查询货位)
    /// </summary>
    public static final String ReplenOverLocation = "Get_RF_ReplenOverLocation";
    /// <summary>
    /// 溢出补货(建议货位)
    /// </summary>
    public static final String ReplenOverAdvice = "Get_RF_ReplenOverAdvice";
    /// <summary>
    /// 溢出补货(下一笔记录)
    /// </summary>
    public static final String ReplenOverNextRecord = "Get_RF_ReplenOverNextRecord";
    /// <summary>
    /// 溢出补货(插入数据)
    /// </summary>
    public static final String ReplenOverInsert = "Post_RF_ReplenOverInsert";

    /// <summary>
    /// 拣货任务列表(按出库单)
    /// </summary>
    public static final String PickListOrder = "Get_RF_PickListOrder";
    /// <summary>
    /// 拣货任务列表(按波次单)
    /// </summary>
    public static final String PickListWave = "Get_RF_PickListWave";

    /// <summary>
    /// 拣货明细列表(指定出库单)
    /// </summary>
    public static final String PickDetailOrder = "Get_RF_PickDetailOrder";
    /// <summary>
    /// 拣货明细列表(指定波次单)
    /// </summary>
    public static final String PickDetailWave = "Get_RF_PickDetailWave";
    /// <summary>
    /// 拣货明细列表(拣货完成)
    /// </summary>
    public static final String PickDetailInsert = "Put_RF_PickDetailInsert";
    /// <summary>
    /// 拣货下架(查询小车)
    /// </summary>
    public static final String ShipPickContainer = "Get_ShipPickContainer";
    /// <summary>
    /// 拣货下架(查询货位)
    /// </summary>
    public static final String ShipPickLocation = "Get_RF_ShipPickLocation";
    /// <summary>
    /// 拣货下架(按照出库单)
    /// </summary>
    public static final String ShipPickOrder = "Get_RF_ShipPickOrder";
    /// <summary>
    /// 拣货下架(按照波次单)
    /// </summary>
    public static final String ShipPickWave = "Get_RF_ShipPickWave";
    /// <summary>
    /// 拣货下架(插入数据)
    /// </summary>
    public static final String ShipPickInsert = "Put_RF_ShipPickInsert";

    /// <summary>
    /// 二次分拣(校验波次单号)
    /// </summary>
    public static final String RepickWave = "Get_RF_RepickWave";
    /// <summary>
    /// 二次分拣(根据波次单货区商品信息)
    /// </summary>
    public static final String RepickOrder = "Get_RF_RepickOrder";
    /// <summary>
    /// 二次分拣(插入数据)
    /// </summary>
    public static final String RepickInsert = "Put_RF_RepickInsert";

    /// <summary>
    /// 波次单号绑定周转箱号(根据物流单号查询波次单号)
    /// </summary>
    public static final String BindTurnoverBoxWave = "Get_BindTurnoverBoxWave";

    /// <summary>
    /// 波次单号绑定周转箱号(插入数据)
    /// </summary>
    public static final String BindTurnoverBoxInsert = "Put_BindTurnoverBoxInsert";

    /// <summary>
    /// 直接补货(查询货位)
    /// </summary>
    public static final String DirectReplenLocation = "Get_RF_DirectReplenLocation";
    /// <summary>
    /// 直接补货(查询指定箱号的数据)
    /// </summary>
    public static final String DirectReplenCarton = "Get_RF_DirectReplenCarton";
    /// <summary>
    /// 直接补货(插入数据)
    /// </summary>
    public static final String DirectReplenInsert = "Post_RF_DirectReplenInsert";

    /// <summary>
    /// 直接移货(查询来源货位)
    /// </summary>
    public static final String DirectMoveFromLocation = "Get_RF_DirectMoveFromLocation";
    /// <summary>
    /// 直接移货(查询目的货位)
    /// </summary>
    public static final String DirectMoveToLocation = "Get_RF_DirectMoveToLocation";
    /// <summary>
    /// 直接移货(查询指定货位商品信息)
    /// </summary>
    public static final String DirectMoveQty = "Get_RF_DirectMoveQty";
    /// <summary>
    /// 直接移货(插入数据)
    /// </summary>
    public static final String DirectMoveInsert = "Post_RF_DirectMoveInsert";

    /// <summary>
    /// 整箱拆零
    /// </summary>
    public static final String Unpacking = "RF_Unpacking";

    /// <summary>
    /// 散货装箱(查询货位)
    /// </summary>
    public static final String PackageLocation = "RF_PackageLocation";
    /// <summary>
    /// 散货装箱(指定货位商品)
    /// </summary>
    public static final String PackageSKU = "RF_PackageSKU";
    /// <summary>
    /// 散货装箱(插入数据)
    /// </summary>
    public static final String PackageInsert = "RF_PackageInsert";

    /// <summary>
    /// 批发装箱（查询订单信息）
    /// </summary>
    public static final String WholesalePackingOrder = "Get_RF_WholesalePackingOrder";
    /// <summary>
    /// 批发装箱（查询商品信息）
    /// </summary>
    public static final String WholesalePackingSKU = "Get_RF_WholesalePackingSKU";
    /// <summary>
    /// 批发装箱（查询指定箱号商品信息）
    /// </summary>
    public static final String WholesalePackingCarton = "Get_RF_WholesalePackingCarton";
    /// <summary>
    /// 批发装箱（插入数据）
    /// </summary>
    public static final String WholesalePackingInsert = "Post_RF_WholesalePackingInsert";

    /// <summary>
    /// 退货扫描
    /// </summary>
    public static final String ReturnScan = "RF_ReturnScan";

    /// <summary>
    /// 出库交接
    /// </summary>
    public static final String Delivery = "Put_RF_Delivery";

    /// <summary>
    /// 盘点任务列表
    /// </summary>
    public static final String CycleCountList = "Get_RF_CycleCountList";

    /// <summary>
    /// 盘点明细列表
    /// </summary>
    public static final String CycleCountDetail = "Get_RF_CycleCountDetail";

    /// <summary>
    /// 库存盘点(查询货位ID)
    /// </summary>
    public static final String CycleCountLocation = "Get_RF_CycleCountLocation";
    /// <summary>
    /// 库存盘点(查询商品信息)
    /// </summary>
    public static final String CycleCountSKU = "Get_RF_CycleCountSKU";
    /// <summary>
    /// 库存盘点(下一步记录)
    /// </summary>
    public static final String CycleCountNextRecord = "Get_RF_CycleCountNextRecord";
    /// <summary>
    /// 库存盘点(插入数据)
    /// </summary>
    public static final String CycleCountInsert = "Post_RF_CycleCountInsert";

    /// <summary>
    /// 通用SKU获取
    /// </summary>
    public static final String CommonGetSKU = "Get_RF_GetSKU";
    /// <summary>
    /// 盘点任务列表
    /// </summary>
    public static final String CountList = "Get_RF_CountList";
    /// <summary>
    /// 盘点明细列表
    /// </summary>
    public static final String CountDetail = "Get_RF_CountDetail";
    /// <summary>
    /// 库存盘点(下一步记录)
    /// </summary>
    public static final String CountNextRecord = "Get_RF_CountNextRecord";
    /// <summary>
    /// 库存盘点(查询货位ID)
    /// </summary>
    public static final String CountLocation = "Get_RF_CountLocation";
    /// <summary>
    /// 库存盘点(插入数据)
    /// </summary>
    public static final String CountInsert = "Post_RF_CountInsert";
    /// <summary>
    /// 盘点问题反馈
    /// </summary>
    public static final String CountExceptionLog = "Post_RF_CountExceptionLog";


    /// <summary>
    /// 异常信息记录
    /// </summary>
    public static final String ExceptionLog_PutAway = "Post_RF_ExceptionLog_PutAway";
    /// <summary>
    /// 库存状态货位查询
    /// </summary>
    public static final String InventoryStatusLocation = "Get_InventoryStatusLocation";
    /// <summary>
    /// 库存状态货位提交(生成调整单)
    /// </summary>
    public static final String InventoryStatusLocationInsert = "Post_InventoryStatusLocationInsert";
    /// <summary>
    /// 获取承运商信息
    /// </summary>
    public static final String GetCarrierInfo = "Get_RF_CarrierInfo";
    /// <summary>
    /// 通过Auto获取交接编号
    /// </summary>
    public static final String GetVehicleNo = "Get_RF_VehicleNo";

    /// <summary>
    /// 获取版本号
    /// </summary>
    public static final String AutoUpdateGetVersion = "Get_RF_AutoUpdateGetVersion";
    /// <summary>
    /// 获取更新数据
    /// </summary>
    public static final String AutoUpdateGetPatch = "Get_RF_AutoUpdateGetPatch";


    public static final String SummaryCenter_Day = "Get_RF_SummaryCenter_Day";
    public static final String SummaryCenter_Week = "Get_RF_SummaryCenter_Week";
    public static final String SummaryCenter_Month = "Get_RF_SummaryCenter_Month";
    public static final String SummaryCenter_Year = "Get_RF_SummaryCenter_Year";
    public static final String GetWarehouse = "Get_RF_Warehouse";
    public static final String GetDeliveryProgress = "Get_RF_Delivery_Progress";


    public static String getUrl(String servicePort) {
        String url = "";

        switch (servicePort) {
            //登录相关
            case ServicePort.Login:
                url = "mobileapi/login";
                break;
            case ServicePort.LoginOut:
                url = "logout";
                break;
            case ServicePort.Password:
                url = "author/user/passwd";
                break;

            case ServicePort.ReceiveList:
                url = "mobileapi/asnHeaders";
                break;
            case ServicePort.ReceiveDetail:
                url = "mobileapi/asnHeaders/{restfulid0}/details";
                break;
            case ServicePort.ReceiveDetailInsert:
                url = "mobileapi/asnHeader/{restfulid0}/confirm";
                break;
            case ServicePort.ReceiptList:
                url = "mobileapi/receiptHeaders";
                break;
            case ServicePort.ReceiptDetail:
                url = "mobileapi/receiptHeaders/{restfulid0}/details";
                break;
            case ServicePort.Receive_Standard:
                url = "mobileapi/asnHeader/{restfulid0}/barcode";
                break;
            case ServicePort.Receive_StandardInsert:
                url = "mobileapi/asnHeader/{restfulid0}/receipt";
                break;
            case ServicePort.Receive:
                url = "mobileapi/receive/receive";
                break;
            case ServicePort.ReceiveInsert:
                url = "mobileapi/receive/receiveInsert";
                break;

            case ServicePort.CartonPutAdvice:
                url = "mobileapi/putaway/carton";
                break;
            case ServicePort.CartonPutLocation:
                //url = "mobileapi/putaway/cartonLocation";
                url = "mobileapi/location/{restfulid0}";
                break;
            case ServicePort.CartonPutInsert:
                url = "mobileapi/putaway/carton";
                break;
            case ServicePort.PiecePutAdvice:
                url = "mobileapi/putaway/piece";
                break;
            case ServicePort.PiecePutLocation:
                //url = "mobileapi/putaway/pieceSkuLocation";
                url = "mobileapi/location/{restfulid0}";
                break;
            case ServicePort.PiecePutInsert:
                url = "mobileapi/putaway/piece";
                break;
            case ServicePort.ExceptionLog_PutAway:
                url = "mobileapi/putaway/exception";
                break;

            //库存相关
            case ServicePort.Inventory:
                url = "mobileapi/inventory";
                break;
            case ServicePort.SearchInventoryByLocation:
                url = "mobileapi/inventory/location/{restfulid0}";
                break;


            case ServicePort.ReplenList:
                url = "mobileapi/replenish";
                break;
            case ServicePort.ReplenDetail:
                url = "mobileapi/replenish/{restfulid0}/allocates";
                break;
            case ServicePort.ReplenDetailInsert:
                url = "mobileapi/replenish/{restfulid0}";
                break;
            case ServicePort.ReplenPickAdvice:
                url = "mobileapi/inventory/carton/{restfulid0}";
                break;
            case ServicePort.ReplenPickLocation:
                url = "mobileapi/location/{restfulid0}";
                break;
            case ServicePort.ReplenPickAvailableQty:
                url = "mobileapi/inventory/location/{restfulid0}/sku/{restfulid1}";
                break;
            case ServicePort.ReplenPickNextRecord:
                url = "mobileapi/replenish/{restfulid0}/allocate";
                break;
            case ServicePort.ReplenPickInsert:
                url = "mobileapi/replenish/{restfulid0}/allocate/{restfulid1}";
                break;

            //溢出区补货
            case ServicePort.ReplenOverLocation:
                url = "mobileapi/replenish/replenishOverLocation";
                break;
            case ServicePort.ReplenOverAdvice:
                url = "mobileapi/replenish/replenishOverAdvice";
                break;
            case ServicePort.ReplenOverNextRecord:
                url = "mobileapi/replenish/replenishOverNextRecord";
                break;
            case ServicePort.ReplenOverInsert:
                url = "mobileapi/replenish/replenishOverInsert";
                break;

            case ServicePort.PickListOrder:
                url = "mobileapi/shipment/pickorder";
                break;
            case ServicePort.PickListWave:
                url = "mobileapi/shipment/pickorder";
                break;
            case ServicePort.PickDetailOrder:
                url = "mobileapi/shipment/pickorder/detail";
                break;
            case ServicePort.PickDetailWave:
                url = "mobileapi/shipment/pickorder/detail";
                break;
            case ServicePort.PickDetailInsert:
                url = "mobileapi/shipment/pickstatus";
                break;
            case ServicePort.ShipPickContainer:
                url = "mobileapi/shipment/container";
                break;
            case ServicePort.ShipPickLocation:
                url = "mobileapi/location/{restfulid0}";
                break;
            case ServicePort.ShipPickOrder:
                url = "mobileapi/shipment/pickship";
                break;
            case ServicePort.ShipPickWave:
                url = "mobileapi/shipment/pickship";
                break;
            case ServicePort.ShipPickInsert:
                url = "mobileapi/shipment/pickship";
                break;

            //二次分拣
            case ServicePort.RepickOrder:
                url = "mobileapi/shipment/repickorder";
                break;
            case ServicePort.RepickWave:
                url = "mobileapi/wave/{restfulid0}";
                break;
            case ServicePort.RepickInsert:
                url = "mobileapi/shipment/repickorder";
                break;


            case ServicePort.BindTurnoverBoxWave:
                url = "mobileapi/wave/id";
                break;
            case ServicePort.BindTurnoverBoxInsert:
                url = "mobileapi/wave/bindboxno";
                break;

            //直接补货程序暂时没用上
            case ServicePort.DirectReplenLocation:
                url = "mobileapi/location/{restfulid0}";
                break;
            case ServicePort.DirectReplenCarton:
                url = "mobileapi/inventory/carton/{restfulid0}";
                break;
            case ServicePort.DirectReplenInsert:
                url = "mobileapi/directMove/directReplenInsert";
                break;

            //直接移货
            case ServicePort.DirectMoveFromLocation:
                url = "mobileapi/inventory/unlocked/location";
                break;
            case ServicePort.DirectMoveToLocation:
                url = "mobileapi/location/{restfulid0}";
                break;
            case ServicePort.DirectMoveQty:
                url = "mobileapi/inventory/location/{restfulid0}/barcode/{restfulid1}";
                break;
            case ServicePort.DirectMoveInsert:
                url = "mobileapi/directMove";
                break;

            //打包
            case ServicePort.Unpacking:
                url = "mobileapi/move/unpacking";
                break;
            case ServicePort.PackageLocation:
                url = "mobileapi/move/packageLocation";
                break;
            case ServicePort.PackageSKU:
                url = "mobileapi/move/packageSKU";
                break;
            case ServicePort.PackageInsert:
                url = "mobileapi/move/packageInsert";
                break;

            case ServicePort.WholesalePackingOrder:
                url = "mobileapi/shipment/package";
                break;
            case ServicePort.WholesalePackingSKU:
                url = "mobileapi/sku";
                break;
            case ServicePort.WholesalePackingCarton:
                url = "mobileapi/package/wholesalePackingCarton";
                break;
            case ServicePort.WholesalePackingInsert:
                url = "mobileapi/shipment/package";
                break;


            //退货相关
            case ServicePort.ReturnScan:
                url = "mobileapi/return/returnScan";
                break;

            //出库相关
            case ServicePort.Delivery:
                url = "mobileapi/shipment/statuscode";
                break;


            case ServicePort.CycleCountList:
                url = "mobileapi/cycle/header";
                break;
            case ServicePort.CycleCountDetail:
                url = "mobileapi/cycle/detail";
                break;
            case ServicePort.CycleCountLocation:
                //url = "mobileapi/cycle/location";
                url = "mobileapi/cycle/{restfulid0}";
                break;
            case ServicePort.CycleCountSKU:
                url = "mobileapi/cycle/goods";
                break;
            case ServicePort.CycleCountNextRecord:
                url = "mobileapi/cycle/record";
                break;
            case ServicePort.CycleCountInsert:
                url = "mobileapi/cycle";
                break;

            case ServicePort.CountList:
                url = "mobileapi/inventoryCount/list";
                break;
            case ServicePort.CountDetail:
                url = "mobileapi/inventoryCount/detail";
                break;
            case ServicePort.CountNextRecord:
                url = "mobileapi/inventoryCount/record";
                break;
            case ServicePort.CountLocation:
                url = "mobileapi/inventoryCount/checkLocationAndLockInventory";
                break;
            case ServicePort.CountInsert:
                url = "mobileapi/inventoryCount/count";
                break;
            case ServicePort.CountExceptionLog:
                url = "mobileapi/inventoryCount/exception";
                break;


            case ServicePort.InventoryStatusLocation:
                url = "mobileapi/inventory/status/{restfulid0}";
                break;
            case ServicePort.InventoryStatusLocationInsert:
                url = "mobileapi/inventory/adjustment";
                break;

            case ServicePort.AutoUpdateGetVersion:
                url = "mobileapi/system/android/version";
                break;

            case ServicePort.AutoUpdateGetPatch:
                url = "mobileapi/system/android/patch";
                break;

            case ServicePort.CommonGetSKU:
                url = "mobileapi/sku";
                break;

            case ServicePort.GetCarrierInfo:
                url = "mobileapi/carrier/list";
                break;

            case ServicePort.GetVehicleNo:
                url = "mobileapi/shipment/associate";
                break;

            case ServicePort.SummaryCenter_Day:
                url = "wmsapi/api/chart/day";
                break;
            case ServicePort.SummaryCenter_Week:
                url = "wmsapi/api/chart/week";
                break;
            case ServicePort.SummaryCenter_Month:
                url = "wmsapi/api/chart/month";
                break;
            case ServicePort.SummaryCenter_Year:
                url = "wmsapi/api/chart/year";
                break;
            case ServicePort.GetWarehouse:
                url = "wmsapi/api/chart/warehouse";
                break;
            case ServicePort.GetDeliveryProgress:
                url = "wmsapi/api/chart/shipment/{restfulid0}";
                break;
            default:
                break;
        }

        return url;
    }
}

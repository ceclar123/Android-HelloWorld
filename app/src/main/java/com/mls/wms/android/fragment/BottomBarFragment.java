package com.mls.wms.android.fragment;

import android.app.Fragment;
import android.graphics.Point;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.mls.wms.android.R;
import com.mls.wms.android.util.WindowUtil;

/**
 * Created by 2014-400 on 2015-08-13.
 */
public class BottomBarFragment extends Fragment {
    public final int COLUMN_COUNT = 3;
    private View view = null;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fg_bottom_bar, container, false);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //当对应的Activity创建之后才调用
        //屏幕分辨率
        Point point = WindowUtil.getMetrics(getActivity());

        //指定Activity实现接口IBottomBar
        final IBottomBar bottomBar = (IBottomBar) getActivity();

        int width = point.x / COLUMN_COUNT;
        int height = width / 2;
        Button btn = (Button) view.findViewById(R.id.btn_left);
        btn.setWidth(width);
        btn.setHeight(height);
        if (bottomBar != null) {
            btn.setText(bottomBar.getBtnLeftName());
            btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    bottomBar.btn_left_click(v);
                }
            });
        }

        btn = (Button) view.findViewById(R.id.btn_middle);
        btn.setWidth(width);
        btn.setHeight(height);
        if (bottomBar != null) {
            btn.setText(bottomBar.getBtnMiddleName());
            btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    bottomBar.btn_middle_click(v);
                }
            });
        }

        btn = (Button) view.findViewById(R.id.btn_right);
        btn.setWidth(width);
        btn.setHeight(height);
        if (bottomBar != null) {
            btn.setText(bottomBar.getBtnRightName());
            btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    bottomBar.btn_right_click(v);
                }
            });
        }

    }
}

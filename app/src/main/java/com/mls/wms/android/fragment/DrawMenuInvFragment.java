package com.mls.wms.android.fragment;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mls.wms.android.R;
import com.mls.wms.android.extend.BaseApplication;
import com.mls.wms.android.widget.inwh.DirectMoveActivity;
import com.mls.wms.android.widget.inwh.InventoryCountListActivity;
import com.mls.wms.android.widget.inwh.InventoryQueryActivity;
import com.mls.wms.android.widget.inwh.InventoryStatusActivity;
import com.mls.wms.android.widget.inwh.PiecePutActivity;
import com.mls.wms.android.widget.inwh.RevokePutActivity;

/**
 * Created by 2014-400 on 2015-09-11.
 */
public class DrawMenuInvFragment extends Fragment {
    private View createView = null;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        createView = inflater.inflate(R.layout.fg_draw_menu_inventory, container, false);
        return createView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        final BaseApplication baseApplication = (BaseApplication) getActivity().getApplication();

        LinearLayout layoutCartonPut = (LinearLayout) view.findViewById(R.id.fg_draw_menu_inv_layout_carton_put);
        LinearLayout layoutPiecePut = (LinearLayout) view.findViewById(R.id.fg_draw_menu_inv_layout_piece_put);
        LinearLayout layoutRevokePut = (LinearLayout) view.findViewById(R.id.fg_draw_menu_inv_layout_revoke_put);
        LinearLayout layoutReplenishDown = (LinearLayout) view.findViewById(R.id.fg_draw_menu_inv_layout_replenish_down);
        LinearLayout layoutDirectMove = (LinearLayout) view.findViewById(R.id.fg_draw_menu_inv_layout_direct_move);
        LinearLayout layoutInventoryCount = (LinearLayout) view.findViewById(R.id.fg_draw_menu_inv_layout_inventory_count);
        LinearLayout layoutPiecePackage = (LinearLayout) view.findViewById(R.id.fg_draw_menu_inv_layout_piece_package);
        LinearLayout layoutInventoryQuery = (LinearLayout) view.findViewById(R.id.fg_draw_menu_inv_layout_inventory_query);
        LinearLayout layoutInventoryStatusAdjust = (LinearLayout) view.findViewById(R.id.fg_draw_menu_inv_layout_inventory_status_adjust);

        TextView tvLogo = (TextView) view.findViewById(R.id.fg_draw_menu_inventory_tv_logo);
        tvLogo.setTypeface(baseApplication.getIconFont());

        //整仓上架
        if (baseApplication.getGlobalSetting().getModuleService().indexOf(getResources().getString(R.string.MODULE_CARTON_PUT)) > -1) {
            TextView tvImg1 = (TextView) view.findViewById(R.id.fg_draw_menu_inv_tv_carton_put_img);
            tvImg1.setTypeface(baseApplication.getIconFont());

            TextView tvCartonPut = (TextView) view.findViewById(R.id.fg_draw_menu_inv_tv_carton_put);
            tvCartonPut.setOnClickListener(myClickListener);
        } else {
            layoutCartonPut.setVisibility(View.GONE);
        }
        //零仓上架
        if (baseApplication.getGlobalSetting().getModuleService().indexOf(getResources().getString(R.string.MODULE_PIECE_PUT)) > -1) {
            TextView tvImg2 = (TextView) view.findViewById(R.id.fg_draw_menu_inv_tv_piece_put_img);
            tvImg2.setTypeface(baseApplication.getIconFont());

            TextView tvPiecePut = (TextView) view.findViewById(R.id.fg_draw_menu_inv_tv_piece_put);
            tvPiecePut.setOnClickListener(myClickListener);
        } else {
            layoutPiecePut.setVisibility(View.GONE);
        }
        //撤单上架
        if (baseApplication.getGlobalSetting().getModuleService().indexOf(getResources().getString(R.string.MODULE_REVOKE_PUT)) > -1) {
            TextView tvImg3 = (TextView) view.findViewById(R.id.fg_draw_menu_inv_tv_revoke_put_img);
            tvImg3.setTypeface(baseApplication.getIconFont());

            TextView tvRevokePut = (TextView) view.findViewById(R.id.fg_draw_menu_inv_tv_revoke_put);
            tvRevokePut.setOnClickListener(myClickListener);
        } else {
            layoutRevokePut.setVisibility(View.GONE);
        }
        //补货下架
        if (baseApplication.getGlobalSetting().getModuleService().indexOf(getResources().getString(R.string.MODULE_DIRECT_REPLEN)) > -1) {
            TextView tvImg4 = (TextView) view.findViewById(R.id.fg_draw_menu_inv_tv_replenish_down_img);
            tvImg4.setTypeface(baseApplication.getIconFont());

            TextView tvReplenishDown = (TextView) view.findViewById(R.id.fg_draw_menu_inv_tv_replenish_down);
            tvReplenishDown.setOnClickListener(myClickListener);
        } else {
            layoutReplenishDown.setVisibility(View.GONE);
        }
        //直接移货
        if (baseApplication.getGlobalSetting().getModuleService().indexOf(getResources().getString(R.string.MODULE_DIRECT_MOVE)) > -1) {
            TextView tvImg5 = (TextView) view.findViewById(R.id.fg_draw_menu_inv_tv_direct_move_img);
            tvImg5.setTypeface(baseApplication.getIconFont());

            TextView tvDirectMove = (TextView) view.findViewById(R.id.fg_draw_menu_inv_tv_direct_move);
            tvDirectMove.setOnClickListener(myClickListener);
        } else {
            layoutDirectMove.setVisibility(View.GONE);
        }
        //库存盘点
        if (baseApplication.getGlobalSetting().getModuleService().indexOf(getResources().getString(R.string.MODULE_INVENTORY_COUNT)) > -1) {
            TextView tvImg6 = (TextView) view.findViewById(R.id.fg_draw_menu_inv_tv_inventory_count_img);
            tvImg6.setTypeface(baseApplication.getIconFont());

            TextView tvCountInventory = (TextView) view.findViewById(R.id.fg_draw_menu_inv_tv_inventory_count);
            tvCountInventory.setOnClickListener(myClickListener);
        } else {
            layoutInventoryCount.setVisibility(View.GONE);
        }
        //散货装箱
        if (baseApplication.getGlobalSetting().getModuleService().indexOf(getResources().getString(R.string.MODULE_PIECE_PACKAGE)) > -1) {
            TextView tvImg7 = (TextView) view.findViewById(R.id.fg_draw_menu_inv_tv_piece_packing_img);
            tvImg7.setTypeface(baseApplication.getIconFont());

            TextView tvPiecePacking = (TextView) view.findViewById(R.id.fg_draw_menu_inv_tv_piece_packing);
            tvPiecePacking.setOnClickListener(myClickListener);
        } else {
            layoutPiecePackage.setVisibility(View.GONE);
        }
        //库存查询
        if (baseApplication.getGlobalSetting().getModuleService().indexOf(getResources().getString(R.string.MODULE_INVENTORY_QUERY)) > -1) {
            TextView tvImg8 = (TextView) view.findViewById(R.id.fg_draw_menu_inv_tv_inventory_query_img);
            tvImg8.setTypeface(baseApplication.getIconFont());

            TextView tvInventoryQuery = (TextView) view.findViewById(R.id.fg_draw_menu_inv_tv_inventory_query);
            tvInventoryQuery.setOnClickListener(myClickListener);
        } else {
            layoutInventoryQuery.setVisibility(View.GONE);
        }
        //状态调整
        if (baseApplication.getGlobalSetting().getModuleService().indexOf(getResources().getString(R.string.MODULE_INVENTORY_STATUS_ADJUST)) > -1) {
            TextView tvImg9 = (TextView) view.findViewById(R.id.fg_draw_menu_inv_tv_inventory_status_edit_img);
            tvImg9.setTypeface(baseApplication.getIconFont());

            TextView tvStatusEdit = (TextView) view.findViewById(R.id.fg_draw_menu_inv_tv_inventory_status_edit);
            tvStatusEdit.setOnClickListener(myClickListener);
        } else {
            layoutInventoryStatusAdjust.setVisibility(View.GONE);
        }
    }


    private View.OnClickListener myClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v.getId() == R.id.fg_draw_menu_inv_tv_carton_put) {
                //
            } else if (v.getId() == R.id.fg_draw_menu_inv_tv_piece_put) {
                Intent intent = new Intent();
                intent.setClass(v.getContext(), PiecePutActivity.class);
                startActivity(intent);
            } else if (v.getId() == R.id.fg_draw_menu_inv_tv_revoke_put) {
                Intent intent = new Intent();
                intent.setClass(v.getContext(), RevokePutActivity.class);
                startActivity(intent);
            } else if (v.getId() == R.id.fg_draw_menu_inv_tv_replenish_down) {
                //
            } else if (v.getId() == R.id.fg_draw_menu_inv_tv_direct_move) {
                Intent intent = new Intent();
                intent.setClass(v.getContext(), DirectMoveActivity.class);
                startActivity(intent);
            } else if (v.getId() == R.id.fg_draw_menu_inv_tv_inventory_count) {
                Intent intent = new Intent();
                intent.setClass(v.getContext(), InventoryCountListActivity.class);
                startActivity(intent);
            } else if (v.getId() == R.id.fg_draw_menu_inv_tv_piece_packing) {
                //
            } else if (v.getId() == R.id.fg_draw_menu_inv_tv_inventory_query) {
                Intent intent = new Intent();
                intent.setClass(v.getContext(), InventoryQueryActivity.class);
                startActivity(intent);
            } else if (v.getId() == R.id.fg_draw_menu_inv_tv_inventory_status_edit) {
                Intent intent = new Intent();
                intent.setClass(v.getContext(), InventoryStatusActivity.class);
                startActivity(intent);
            }
        }
    };
}
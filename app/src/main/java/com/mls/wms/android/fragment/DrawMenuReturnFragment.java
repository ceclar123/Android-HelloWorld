package com.mls.wms.android.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mls.wms.android.R;
import com.mls.wms.android.extend.BaseApplication;

/**
 * Created by 2014-400 on 2015-09-11.
 */
public class DrawMenuReturnFragment extends Fragment {
    private View createView = null;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        createView = inflater.inflate(R.layout.fg_draw_menu_return, container, false);
        return createView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        final BaseApplication baseApplication = (BaseApplication) getActivity().getApplication();

        LinearLayout layoutReturnSale = (LinearLayout) view.findViewById(R.id.fg_draw_menu_return_layout_return_sale);
        LinearLayout layoutReturnPO = (LinearLayout) view.findViewById(R.id.fg_draw_menu_return_layout_return_asn);

        TextView tvLogo = (TextView) view.findViewById(R.id.fg_draw_menu_return_tv_logo);
        tvLogo.setTypeface(baseApplication.getIconFont());

        //销售退货
        if (baseApplication.getGlobalSetting().getModuleService().indexOf(getResources().getString(R.string.MODULE_RETURN_SALE)) > -1) {
            TextView tvImg1 = (TextView) view.findViewById(R.id.fg_draw_menu_return_tv_return_sale_img);
            tvImg1.setTypeface(baseApplication.getIconFont());

            TextView tvSale = (TextView) view.findViewById(R.id.fg_draw_menu_return_tv_return_sale);
            tvSale.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //
                }
            });
        } else {
            layoutReturnSale.setVisibility(View.GONE);
        }

        //采购退货
        if (baseApplication.getGlobalSetting().getModuleService().indexOf(getResources().getString(R.string.MODULE_RETURN_PO)) > -1) {
            TextView tvImg2 = (TextView) view.findViewById(R.id.fg_draw_menu_return_tv_return_asn_img);
            tvImg2.setTypeface(baseApplication.getIconFont());

            TextView tvAsn = (TextView) view.findViewById(R.id.fg_draw_menu_return_tv_return_asn);
            tvAsn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //
                }
            });
        } else {
            layoutReturnPO.setVisibility(View.GONE);
        }
    }

}

package com.mls.wms.android.model;

import com.mls.wms.android.util.JSONUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by 2014-400 on 2015-09-15.
 */
public class InventoryQueryModel extends BaseSKUModel implements IModel, Serializable {
    private String zoneTypeName;
    private String zoneNo;
    private String locationNo;
    private String cartonNo;
    private String lotKey;
    private String inventoryStatusName;
    private String isHold;
    private String isCycleCount;

    private int onhandQty;
    private int allocatedQty;
    private int availableQty;


    @Override
    public InventoryQueryModel parseJson(String json) throws Exception {
        InventoryQueryModel model = new InventoryQueryModel();

        JSONObject object = new JSONObject(json);

        model.setSkuId(JSONUtils.getLong(object, "skuId", 0L));
        model.setSku(JSONUtils.getString(object, "sku", ""));
        model.setBarcode(JSONUtils.getString(object, "barcode", ""));
        model.setItemName(JSONUtils.getString(object, "itemName", ""));
        model.setProperties(JSONUtils.getString(object, "properties", ""));
        model.setColorName(JSONUtils.getString(object, "colorName", ""));
        model.setSizeName(JSONUtils.getString(object, "sizeName", ""));
        model.setUnitName(JSONUtils.getString(object, "unitName", ""));

        model.setZoneTypeName(JSONUtils.getString(object, "zoneTypeName", ""));
        model.setZoneNo(JSONUtils.getString(object, "zoneNo", ""));
        model.setLocationNo(JSONUtils.getString(object, "locationNo", ""));
        model.setCartonNo(JSONUtils.getString(object, "cartonNo", ""));
        model.setLotKey(JSONUtils.getString(object, "lotKey", ""));
        model.setInventoryStatusName(JSONUtils.getString(object, "inventoryStatusName", ""));
        model.setIsHold(JSONUtils.getString(object, "isHold", ""));
        model.setIsCycleCount(JSONUtils.getString(object, "isCycleCount", ""));

        model.setOnhandQty(JSONUtils.getInt(object, "onhandQty", 0));
        model.setAllocatedQty(JSONUtils.getInt(object, "allocatedQty", 0));
        model.setAvailableQty(JSONUtils.getInt(object, "availableQty", 0));

        return model;
    }

    @Override
    public List<InventoryQueryModel> parseJsons(String json) throws Exception {
        List<InventoryQueryModel> list = new ArrayList<InventoryQueryModel>();
        JSONArray array = new JSONArray(json);
        for (int i = 0; i < array.length(); i++) {
            list.add(parseJson(array.getString(i)));
        }
        return list;
    }

    public String getZoneTypeName() {
        return zoneTypeName;
    }

    public void setZoneTypeName(String zoneTypeName) {
        this.zoneTypeName = zoneTypeName;
    }

    public String getZoneNo() {
        return zoneNo;
    }

    public void setZoneNo(String zoneNo) {
        this.zoneNo = zoneNo;
    }

    public String getLocationNo() {
        return locationNo;
    }

    public void setLocationNo(String locationNo) {
        this.locationNo = locationNo;
    }

    public String getCartonNo() {
        return cartonNo;
    }

    public void setCartonNo(String cartonNo) {
        this.cartonNo = cartonNo;
    }

    public String getLotKey() {
        return lotKey;
    }

    public void setLotKey(String lotKey) {
        this.lotKey = lotKey;
    }

    public String getInventoryStatusName() {
        return inventoryStatusName;
    }

    public void setInventoryStatusName(String inventoryStatusName) {
        this.inventoryStatusName = inventoryStatusName;
    }

    public int getOnhandQty() {
        return onhandQty;
    }

    public void setOnhandQty(int onhandQty) {
        this.onhandQty = onhandQty;
    }

    public int getAllocatedQty() {
        return allocatedQty;
    }

    public void setAllocatedQty(int allocatedQty) {
        this.allocatedQty = allocatedQty;
    }

    public int getAvailableQty() {
        return availableQty;
    }

    public void setAvailableQty(int availableQty) {
        this.availableQty = availableQty;
    }

    public String getIsHold() {
        return isHold;
    }

    public void setIsHold(String isHold) {
        this.isHold = isHold;
    }

    public String getIsCycleCount() {
        return isCycleCount;
    }

    public void setIsCycleCount(String isCycleCount) {
        this.isCycleCount = isCycleCount;
    }
}

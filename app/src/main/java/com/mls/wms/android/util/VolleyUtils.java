package com.mls.wms.android.util;

import com.android.http.LoadControler;
import com.android.http.LoadListener;
import com.android.http.RequestManager;
import com.android.volley.Request;
import com.mls.wms.android.extend.BaseApplication;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by 2014-400 on 2015-09-15.
 */
public class VolleyUtils {
    private VolleyUtils() {
    }

    /**
     * get
     *
     * @param baseApplication
     * @param url
     * @param requestListener
     * @param actionId
     * @return
     */
    public static LoadControler get(BaseApplication baseApplication, String url, RequestManager.RequestListener requestListener, int actionId) {
        return get(baseApplication, url, null, null, requestListener, true, actionId);
    }

    /**
     * get
     *
     * @param baseApplication
     * @param url
     * @param data
     * @param requestListener
     * @param actionId
     * @return
     */
    public static LoadControler get(BaseApplication baseApplication, String url, Map<String, String> data, RequestManager.RequestListener requestListener, int actionId) {
        return get(baseApplication, url, data, null, requestListener, true, actionId);
    }

    /**
     * get
     *
     * @param baseApplication
     * @param url
     * @param data
     * @param headers
     * @param requestListener
     * @param actionId
     * @return
     */
    public static LoadControler get(BaseApplication baseApplication, String url, Map<String, String> data, Map<String, String> headers, RequestManager.RequestListener requestListener, int actionId) {
        return get(baseApplication, url, data, headers, requestListener, true, actionId);
    }

    /**
     * get
     *
     * @param baseApplication
     * @param url
     * @param requestListener
     * @param shouldCache
     * @param actionId
     * @return
     */
    public static LoadControler get(BaseApplication baseApplication, String url, RequestManager.RequestListener requestListener, boolean shouldCache, int actionId) {
        return get(baseApplication, url, null, null, requestListener, shouldCache, actionId);
    }

    /**
     * get
     *
     * @param baseApplication
     * @param url
     * @param data
     * @param headers
     * @param requestListener
     * @param shouldCache
     * @param actionId
     * @return
     */
    public static LoadControler get(BaseApplication baseApplication, String url, Map<String, String> data, Map<String, String> headers, RequestManager.RequestListener requestListener, boolean shouldCache, int actionId) {
        if (headers == null) {
            headers = new HashMap<String, String>();
        }

        headers.put("Cookie", "JSESSIONID=" + baseApplication.getGlobalSetting().getToken());
        headers.put("Device", "Android");

        return RequestManager.getInstance().get(getUrl(baseApplication, url), data, headers, requestListener, shouldCache, actionId);
    }

    /**
     * default post method
     *
     * @param baseApplication
     * @param url
     * @param data            String, Map<String, String> or RequestMap(with file)
     * @param requestListener
     * @param actionId
     * @return
     */
    public static LoadControler post(BaseApplication baseApplication, String url, Object data, final RequestManager.RequestListener requestListener, int actionId) {
        return post(baseApplication, url, data, null, requestListener, actionId);
    }

    /**
     * post
     *
     * @param baseApplication
     * @param url
     * @param data
     * @param headers
     * @param requestListener
     * @param actionId
     * @return
     */
    public static LoadControler post(BaseApplication baseApplication, String url, Object data, Map<String, String> headers, final RequestManager.RequestListener requestListener, int actionId) {
        if (headers == null) {
            headers = new HashMap<String, String>();
        }

        headers.put("Cookie", "JSESSIONID=" + baseApplication.getGlobalSetting().getToken());
        headers.put("Device", "Android");

        return RequestManager.getInstance().post(getUrl(baseApplication, url), data, headers, requestListener, actionId);
    }

    /**
     * put
     *
     * @param baseApplication
     * @param url
     * @param data
     * @param requestListener
     * @param actionId
     * @return
     */
    public static LoadControler put(BaseApplication baseApplication, String url, Object data, final RequestManager.RequestListener requestListener, int actionId) {
        Map<String, String> headers = new HashMap<String, String>();

        headers.put("Cookie", "JSESSIONID=" + baseApplication.getGlobalSetting().getToken());
        headers.put("Device", "Android");

        return request(baseApplication, Request.Method.PUT, url, data, headers, requestListener, false, 10 * 1000, 1, actionId);
    }

    /**
     * request
     *
     * @param baseApplication
     * @param method          mainly Method.POST and Method.GET
     * @param url             target url
     * @param data            request params
     * @param headers         request headers
     * @param requestListener request callback
     * @param shouldCache     useCache
     * @param timeoutCount    reqeust timeout count
     * @param retryTimes      reqeust retry times
     * @param actionId        request id
     * @return
     */
    public static LoadControler request(BaseApplication baseApplication, int method, final String url, Object data, Map<String, String> headers,
                                        final RequestManager.RequestListener requestListener, boolean shouldCache, int timeoutCount, int retryTimes, int actionId) {
        if (headers == null) {
            headers = new HashMap<String, String>();
        }

        headers.put("Cookie", "JSESSIONID=" + baseApplication.getGlobalSetting().getToken());
        headers.put("Device", "Android");


        return RequestManager.getInstance().request(method, getUrl(baseApplication, url), data, headers, requestListener, shouldCache, timeoutCount, retryTimes, actionId);
    }

    /**
     * sendRequest
     *
     * @param baseApplication
     * @param method
     * @param url
     * @param data
     * @param headers
     * @param requestListener
     * @param shouldCache
     * @param timeoutCount
     * @param retryTimes
     * @param actionId
     * @return
     */
    public static LoadControler sendRequest(BaseApplication baseApplication, int method, final String url, Object data, Map<String, String> headers, final LoadListener requestListener, boolean shouldCache, int timeoutCount, int retryTimes, int actionId) {
        if (headers == null) {
            headers = new HashMap<String, String>();
        }

        headers.put("Cookie", "JSESSIONID=" + baseApplication.getGlobalSetting().getToken());
        headers.put("Device", "Android");

        return RequestManager.getInstance().sendRequest(method, getUrl(baseApplication, url), data, headers, requestListener, shouldCache, timeoutCount, retryTimes, actionId);
    }

    public static String getUrl(BaseApplication baseApplication, String oldUrl) {
        if (false == oldUrl.startsWith("http")) {
            String baseUrl = baseApplication.getGlobalSetting().getServerIP();
            if (true == baseUrl.endsWith("/")) {
                return baseUrl + oldUrl;
            } else {
                return baseUrl + "/" + oldUrl;
            }
        }

        return oldUrl;
    }
}

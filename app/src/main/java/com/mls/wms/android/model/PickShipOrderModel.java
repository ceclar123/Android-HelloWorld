package com.mls.wms.android.model;

import com.mls.wms.android.util.JSONUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by 2014-400 on 2015-09-02.
 */
public class PickShipOrderModel implements IModel, Serializable {
    private long skuId;
    private String sku;
    private String itemName;
    private String properties;
    private int isSkipped;
    private long locationId;
    private String locationNo;
    private String barcode;
    private int availableQty;
    private String nextLocationNo;
    private String shipmentId;
    private String orderIndex;

    public long getSkuId() {
        return skuId;
    }

    public void setSkuId(long skuId) {
        this.skuId = skuId;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getProperties() {
        return properties;
    }

    public void setProperties(String properties) {
        this.properties = properties;
    }

    public int getIsSkipped() {
        return isSkipped;
    }

    public void setIsSkipped(int isSkipped) {
        this.isSkipped = isSkipped;
    }

    public long getLocationId() {
        return locationId;
    }

    public void setLocationId(long locationId) {
        this.locationId = locationId;
    }

    public String getLocationNo() {
        return locationNo;
    }

    public void setLocationNo(String locationNo) {
        this.locationNo = locationNo;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public int getAvailableQty() {
        return availableQty;
    }

    public void setAvailableQty(int availableQty) {
        this.availableQty = availableQty;
    }

    public String getNextLocationNo() {
        return nextLocationNo;
    }

    public void setNextLocationNo(String nextLocationNo) {
        this.nextLocationNo = nextLocationNo;
    }

    public String getShipmentId() {
        return shipmentId;
    }

    public void setShipmentId(String shipmentId) {
        this.shipmentId = shipmentId;
    }

    public String getOrderIndex() {
        return orderIndex;
    }

    public void setOrderIndex(String orderIndex) {
        this.orderIndex = orderIndex;
    }


    @Override
    public PickShipOrderModel parseJson(String json) throws Exception {
        PickShipOrderModel model = new PickShipOrderModel();

        JSONObject object = new JSONObject(json);

        model.setSkuId(JSONUtils.getInt(object, "skuId", 0));
        model.setSku(JSONUtils.getString(object, "sku", ""));
        model.setItemName(JSONUtils.getString(object, "itemName", ""));
        model.setProperties(JSONUtils.getString(object, "properties", ""));

        model.setIsSkipped(JSONUtils.getInt(object, "isSkipped", 0));
        model.setLocationId(JSONUtils.getLong(object, "locationId", 0L));
        model.setLocationNo(JSONUtils.getString(object, "locationNo", ""));

        model.setBarcode(JSONUtils.getString(object, "barcode", ""));
        model.setAvailableQty(JSONUtils.getInt(object, "availableQty", 0));
        model.setNextLocationNo(JSONUtils.getString(object, "nextLocationNo", ""));
        model.setShipmentId(JSONUtils.getString(object, "shipmentId", "0"));
        model.setOrderIndex(JSONUtils.getString(object, "orderIndex", "0"));

        return model;
    }

    @Override
    public List<PickShipOrderModel> parseJsons(String json) throws Exception {
        List<PickShipOrderModel> list = new ArrayList<PickShipOrderModel>();
        JSONArray array = new JSONArray(json);
        for (int i = 0; i < array.length(); i++) {
            list.add(parseJson(array.getString(i)));
        }
        return list;
    }
}

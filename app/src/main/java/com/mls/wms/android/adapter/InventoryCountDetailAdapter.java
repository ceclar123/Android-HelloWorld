package com.mls.wms.android.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.HorizontalScrollView;
import android.widget.TextView;

import com.mls.wms.android.R;
import com.mls.wms.android.constant.InventoryCountModeCode;
import com.mls.wms.android.extend.SwipeLayoutAdapter;
import com.mls.wms.android.model.InventoryCountDetailModel;
import com.mls.wms.android.widget.inwh.InventoryCountAmountActivity;
import com.mls.wms.android.widget.inwh.InventoryCountCommodityActivity;

import java.util.List;

/**
 * Created by 2014-400 on 2015-09-24.
 */
public class InventoryCountDetailAdapter extends SwipeLayoutAdapter<InventoryCountDetailModel> {

    private LayoutInflater inflater;
    private Context context;
    private List<InventoryCountDetailModel> items;
    private long countId = 0L;
    private String countType = "";

    public InventoryCountDetailAdapter(Context context, List<InventoryCountDetailModel> items, long countId, String countType) {
        super(context, R.layout.act_inventory_count_detail_item, R.layout.act_inventory_count_detail_item_swipe, items);
        this.inflater = LayoutInflater.from(context);
        this.context = context;
        this.items = items;
        this.countId = countId;
        this.countType = countType;
    }

    @Override
    public long getItemId(int position) {
        return position % this.items.size();
    }

    @Override
    public int getCount() {
        return this.items.size();
    }

    @Override
    public InventoryCountDetailModel getItem(int position) {
        return position <= this.items.size() - 1 ? this.items.get(position) : null;
    }

    @Override
    public void setContentView(View contentView, int position, HorizontalScrollView parent) {
        InventoryCountDetailModel item = getItem(position);

        //货位编号
        TextView tvLocationNo = (TextView) contentView.findViewById(R.id.act_inventory_count_detail_item_location_no);
        tvLocationNo.setText(item.getLocationNo());
        //盘点状态
        TextView tvCycleCountStatus = (TextView) contentView.findViewById(R.id.act_inventory_count_detail_item_cycle_count_status);
        tvCycleCountStatus.setText(item.getStatusName());
        //盘点数量
        TextView tvCountQty = (TextView) contentView.findViewById(R.id.act_inventory_count_detail_item_cycle_count_qty);
        tvCountQty.setText(item.getCountQty() + "");
        //指定盘点人
        TextView tvAssignUser = (TextView) contentView.findViewById(R.id.act_inventory_count_detail_item_assign_user);
        tvAssignUser.setText(item.getAssignUser());
        //实际盘点人
        TextView tvRealUser = (TextView) contentView.findViewById(R.id.act_inventory_count_detail_item_real_user);
        tvRealUser.setText(item.getRealUser());
        //盘点日期
        TextView tvCountBeginDate = (TextView) contentView.findViewById(R.id.act_inventory_count_detail_item_cycle_begin_time);
        tvCountBeginDate.setText(item.getUpdateTime());
    }

    //实现setActionView方法
    @Override
    public void setActionView(View actionView, final int position, final HorizontalScrollView parent) {
        //盘点
        actionView.findViewById(R.id.act_inventory_count_detail_item_swipe_btn_count).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InventoryCountDetailModel item = items.get(position);
                if (InventoryCountModeCode.COUNTING.toString().equals(countType)) {
                    Intent intent = new Intent(v.getContext(), InventoryCountAmountActivity.class);
                    intent.putExtra("countId", countId);
                    intent.putExtra("countType", countType);
                    v.getContext().startActivity(intent);
                } else {
                    Intent intent = new Intent(v.getContext(), InventoryCountCommodityActivity.class);
                    intent.putExtra("countId", countId);
                    intent.putExtra("countType", countType);
                    v.getContext().startActivity(intent);
                }
            }
        });
    }
}
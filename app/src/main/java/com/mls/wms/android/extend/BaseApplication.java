package com.mls.wms.android.extend;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Application;
import android.app.Instrumentation;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.util.Log;

import com.android.http.RequestManager;
import com.mls.wms.android.model.GlobalSetting;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 2014-400 on 2015-08-14.
 */
public class BaseApplication extends Application {
    private List<Activity> activityList = new ArrayList<Activity>();
    private GlobalSetting globalSetting = new GlobalSetting();
    private Typeface iconFont;
    private Typeface openSansRegular;

    public GlobalSetting getGlobalSetting() {
        if (globalSetting == null) {
            this.initSetting();
        }
        return globalSetting;
    }

    public Typeface getIconFont() {
        return iconFont;
    }

    public Typeface getOpenSansRegular() {
        return openSansRegular;
    }

    public void setOpenSansRegular(Typeface openSansRegular) {
        this.openSansRegular = openSansRegular;
    }

    public void addActivity(Activity activity) {
        Log.i("Activity-create", activity.getClass().toString());
        activityList.add(activity);
    }

    public void removeActivity(Activity activity) {
        Log.i("Activity-destory", activity.getClass().toString());
        activityList.remove(activity);
    }

    public Activity getFirstActivity() {
        return activityList.get(0);
    }

    public Activity getLastActivity() {
        return activityList.get(activityList.size() - 1);
    }

    public void exit() {
        for (Activity activity : activityList) {
            Log.i("Activity-exit", activity.getClass().toString());
            activity.finish();
        }
        activityList.clear();

        //android.os.Process.killProcess(android.os.Process.myPid());
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        manager.killBackgroundProcesses(getPackageName());
    }


    @Override
    public void onCreate() {
        super.onCreate();
        RequestManager.getInstance().init(BaseApplication.this);
        initSetting();
    }

    private void initSetting() {
        iconFont = Typeface.createFromAsset(getAssets(), "iconfont.ttf");
        openSansRegular = Typeface.createFromAsset(getAssets(), "OpenSans-Regular.ttf");

        globalSetting = new GlobalSetting();
        SharedPreferences sp = getSharedPreferences("setting", Context.MODE_PRIVATE);
        globalSetting.setServerIP(sp.getString("ServerIP", ""));
        globalSetting.setWarehouseId(sp.getLong("WarehouseId", 0L));
        globalSetting.setWarehouseNo(sp.getString("WarehouseNo", ""));
        globalSetting.setWarehouseName(sp.getString("WarehouseName", ""));
        globalSetting.setTenantNo(sp.getString("TenantNo", ""));

        globalSetting.setUserId(sp.getLong("UserId", 0L));
        globalSetting.setLoginName(sp.getString("LoginName", ""));
        globalSetting.setUserName(sp.getString("UserName", ""));

        globalSetting.setIsMultiScan(sp.getInt("IsMultiScan", 0));
        globalSetting.setToken(sp.getString("Token", ""));
    }

    /**
     * 发送按键消息
     *
     * @param KeyCode
     */
    public void sendKeyEvent(final int KeyCode) {
        new Thread() {
            public void run() {
                try {
                    Instrumentation inst = new Instrumentation();
                    inst.sendKeyDownUpSync(KeyCode);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        }.start();
    }
}

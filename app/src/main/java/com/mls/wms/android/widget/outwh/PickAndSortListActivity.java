package com.mls.wms.android.widget.outwh;

import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.http.RequestManager;
import com.mls.wms.android.R;
import com.mls.wms.android.adapter.PickShipHeaderAdapter;
import com.mls.wms.android.constant.ServicePort;
import com.mls.wms.android.extend.AnimDialogFragment;
import com.mls.wms.android.extend.BaseActivity;
import com.mls.wms.android.extend.MultiListView;
import com.mls.wms.android.model.PickShipHeaderModel;
import com.mls.wms.android.model.ResponseResult;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PickAndSortListActivity extends BaseActivity {
    private final int LIST_ITEM_COUNT = 3;
    private EditText searchText = null;
    private TextView tvQuery = null;
    private MultiListView listHeader = null;
    private PickShipHeaderAdapter adapter = null;
    private PickShipHeaderModel selectModel = null;

    private List<PickShipHeaderModel> _listAll = null;
    private List<PickShipHeaderModel> _listCurrent = null;

    private AnimDialogFragment progressDialog = null;
    private Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_base_list);

        //弹出框
        progressDialog = new AnimDialogFragment();
        progressDialog.setResId(R.drawable.anim_progress_dialog);

        this.searchText = (EditText) this.findViewById(R.id.act_base_list_search_text);
        this.tvQuery = (TextView) this.findViewById(R.id.act_base_list_tv_query);
        this.listHeader = (MultiListView) this.findViewById(R.id.act_base_list_header);

        this.searchText.setHint(R.string.hint_wave_no);
        this.tvQuery.setTypeface(_BaseApplication.getIconFont());

        this.listHeader.setOnRefreshListener(new MultiListView.OnRefreshListener() {
            @Override
            public void onRefresh() {
                load();
            }
        });
        this.listHeader.setOnLoadListener(new MultiListView.OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        boolean flag = copyData();
                        adapter.notifyDataSetChanged();

                        listHeader.setCanLoadMore(flag);
                        listHeader.onLoadMoreComplete();
                    }
                }, 1 * 1000);
            }
        });

        //点击查询
        this.tvQuery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                load();
            }
        });

    }

    private void load() {
        Map<String, String> paras = new HashMap<String, String>();
        try {
            paras.put("id", Integer.parseInt(searchText.getText().toString()) + "");
        } catch (Exception e) {
            paras.put("id", "");
            e.printStackTrace();
        }
        paras.put("type", "0");//0=边拣边分 1=先拣后分 2=按单拣货

        progressDialog.show(getFragmentManager(), this.toString());
        get(ServicePort.getUrl(ServicePort.PickListWave), paras, new RequestManager.RequestListener() {
            @Override
            public void onRequest() {

            }

            @Override
            public void onSuccess(String s, Map<String, String> stringStringMap, String s2, int i) {
                try {
                    ResponseResult result = new ResponseResult();
                    result = result.parseJson(s);
                    if (result != null && result.getSuc() == true) {
                        if (result.getResult() == null || TextUtils.isEmpty(result.getResult().toString())) {
                            throw new Exception(getResources().getString(R.string.exception_no_data));
                        }

                        PickShipHeaderModel model = new PickShipHeaderModel();
                        _listCurrent = null;
                        _listAll = model.parseJsons(result.getResult().toString());

                        boolean flag = copyData();
                        listHeader.setCanLoadMore(flag);


                        adapter = new PickShipHeaderAdapter(PickAndSortListActivity.this, 0, _listCurrent);
                        listHeader.setAdapter(adapter);

                        progressDialog.dismiss();
                    } else {
                        String message = result == null ? getResources().getString(R.string.title_text_operate_fail) : result.getMessage();
                        progressDialog.dismiss();
                        Toast.makeText(PickAndSortListActivity.this, message, Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    progressDialog.dismiss();
                    Toast.makeText(PickAndSortListActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                } finally {
                    listHeader.onRefreshComplete();
                }

            }

            @Override
            public void onError(String s, String s2, int i) {
                progressDialog.dismiss();
                Toast.makeText(PickAndSortListActivity.this, s, Toast.LENGTH_SHORT).show();
            }
        }, 1);
    }

    /**
     * 复制数据
     *
     * @return false:数据加载完了
     */
    private boolean copyData() {
        if (_listAll != null) {
            if (_listCurrent == null) {
                _listCurrent = new ArrayList<PickShipHeaderModel>();
            }

            if (_listAll.size() > LIST_ITEM_COUNT) {
                for (int i = 0; i < LIST_ITEM_COUNT; i++) {
                    _listCurrent.add(_listAll.get(0));
                    _listAll.remove(0);
                }
            } else {
                _listCurrent.addAll(_listAll);
                _listAll.clear();
                return false;
            }
        }

        return true;
    }

    @Override
    public boolean getScannerVisible() {
        return false;
    }

    @Override
    public boolean getSetVisible() {
        return false;
    }

    @Override
    public int getTopNameResId() {
        return R.string.title_activity_pick_and_sort;
    }
}

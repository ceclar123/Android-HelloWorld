package com.mls.wms.android.model;

import com.mls.wms.android.util.JSONUtils;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Administrator on 2015-03-12.
 * WmsApi调用返回结果
 */
public class ApiResult implements IModel, Serializable {
    /**
     * 成功:true；失败:false
     */
    private Boolean success = false;
    /**
     * 成功|失败消息
     */
    private String message = "";
    /**
     * 返回结果
     */
    private String result = null;

    public Boolean getSuccess() {
        return success;

    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;

    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    @Override
    public ApiResult parseJson(String json) throws Exception {
        ApiResult responseResult = new ApiResult();

        JSONObject object = new JSONObject(json);

        responseResult.setMessage(JSONUtils.getString(object, "message", ""));
        responseResult.setResult(JSONUtils.getString(object, "result", ""));
        responseResult.setSuccess(JSONUtils.getBoolean(object, "success", false));

        return responseResult;
    }

    @Override
    public List<ApiResult> parseJsons(String json) throws Exception {
        return null;
    }
}

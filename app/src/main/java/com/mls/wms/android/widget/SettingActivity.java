package com.mls.wms.android.widget;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.android.http.RequestManager;
import com.mls.wms.android.R;
import com.mls.wms.android.constant.ServicePort;
import com.mls.wms.android.extend.BaseActivity;
import com.mls.wms.android.fragment.ChangePasswordFragment;

import java.util.Map;

/**
 * Created by 2014-400 on 2015-08-17.
 */
public class SettingActivity extends BaseActivity {
    private int errorCount = 0;
    private TextView tvUpdatePwd = null;
    private TextView tvAbout = null;
    private TextView tvLogout = null;
    private TextView tvQuit = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_setting);

        this.init();
    }

    private void init() {
        tvUpdatePwd = (TextView) this.findViewById(R.id.act_setting_tv_pwd);
        tvAbout = (TextView) this.findViewById(R.id.act_setting_tv_about);
        tvLogout = (TextView) this.findViewById(R.id.act_setting_tv_logout);
        tvQuit = (TextView) this.findViewById(R.id.act_setting_tv_quit);

        tvUpdatePwd.setTypeface(_BaseApplication.getIconFont());
        tvAbout.setTypeface(_BaseApplication.getIconFont());
        tvLogout.setTypeface(_BaseApplication.getIconFont());
        tvQuit.setTypeface(_BaseApplication.getIconFont());

        tvUpdatePwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tabClick(v);
            }
        });
        tvAbout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tabClick(v);
            }
        });
        tvLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tabClick(v);
            }
        });
        tvQuit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tabClick(v);
            }
        });

        //默认选中第一个
        this.loadChangePassword();
    }

    private void tabClick(View view) {
        if (view.getId() == R.id.act_setting_tv_pwd) {
            this.loadChangePassword();
        } else if (view.getId() == R.id.act_setting_tv_about) {
            setButton(tvAbout);
        } else if (view.getId() == R.id.act_setting_tv_logout) {
            setButton(tvLogout);
        } else if (view.getId() == R.id.act_setting_tv_quit) {
            setButton(tvQuit);
        }
    }

    private void setButton(View view) {
        tvUpdatePwd.setBackgroundResource(R.color.light_gray);
        tvUpdatePwd.setTextColor(getResources().getColor(R.color.black));

        tvAbout.setBackgroundResource(R.color.light_gray);
        tvAbout.setTextColor(getResources().getColor(R.color.black));

        tvLogout.setBackgroundResource(R.color.light_gray);
        tvLogout.setTextColor(getResources().getColor(R.color.black));

        tvQuit.setBackgroundResource(R.color.light_gray);
        tvQuit.setTextColor(getResources().getColor(R.color.black));

        if (view.getId() == R.id.act_setting_tv_pwd) {
            tvUpdatePwd.setBackgroundResource(R.color.blue);
            tvUpdatePwd.setTextColor(getResources().getColor(R.color.white));
        } else if (view.getId() == R.id.act_setting_tv_about) {
            tvAbout.setBackgroundResource(R.color.blue);
            tvAbout.setTextColor(getResources().getColor(R.color.white));
        } else if (view.getId() == R.id.act_setting_tv_logout) {
            tvLogout.setBackgroundResource(R.color.blue);
            tvLogout.setTextColor(getResources().getColor(R.color.white));
            logout();
        } else if (view.getId() == R.id.act_setting_tv_quit) {
            tvQuit.setBackgroundResource(R.color.blue);
            tvQuit.setTextColor(getResources().getColor(R.color.white));
            quit();
        }
    }

    private void loadChangePassword() {
        FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        Fragment fragment = new ChangePasswordFragment();
        ft.replace(R.id.act_setting_frame, fragment, this.getClass().toString());
        ft.commit();

        setButton(tvUpdatePwd);
    }

    private void logout() {
        AlertDialog dialog = new AlertDialog.Builder(this).create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.setIcon(android.R.drawable.ic_dialog_alert);
        dialog.setTitle(getResources().getString(R.string.title_text_logout));
        dialog.setMessage(getResources().getString(R.string.dialog_msg_logout_confirm));
        //确定按钮
        dialog.setButton(DialogInterface.BUTTON_POSITIVE, getResources().getText(R.string.enum_yes),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        get(ServicePort.getUrl(ServicePort.LoginOut), null,
                                new RequestManager.RequestListener() {
                                    @Override
                                    public void onRequest() {

                                    }

                                    @Override
                                    public void onSuccess(String s, Map<String, String> stringStringMap, String s2, int i) {
                                        Intent intent = new Intent(Intent.ACTION_VIEW);
                                        intent.setClass(SettingActivity.this, LoginActivity.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                        SettingActivity.this.startActivity(intent);
                                    }

                                    @Override
                                    public void onError(String s, String s2, int i) {
//                                        AlertDialog dialog = new AlertDialog.Builder(SettingActivity.this).create();
//                                        dialog.setCanceledOnTouchOutside(false);
//                                        dialog.setCancelable(false);
//                                        dialog.setIcon(android.R.drawable.ic_dialog_info);
//                                        dialog.setTitle(getResources().getString(R.string.title_text_tip));
//                                        dialog.setMessage(getResources().getString(R.string.dialog_msg_logout_fail));
//                                        //确定按钮
//                                        dialog.setButton(DialogInterface.BUTTON_POSITIVE, getResources().getText(R.string.enum_ok), new DialogInterface.OnClickListener() {
//                                            @Override
//                                            public void onClick(DialogInterface dialog, int which) {
//                                                //
//                                            }
//                                        });
//                                        dialog.show();

                                        Intent intent = new Intent(Intent.ACTION_VIEW);
                                        intent.setClass(SettingActivity.this, LoginActivity.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                        SettingActivity.this.startActivity(intent);
                                    }
                                }, 1);
                    }
                });
        dialog.setButton(DialogInterface.BUTTON_NEGATIVE, getResources().getText(R.string.enum_no),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //
                    }
                });
        dialog.show();
    }

    private void quit() {
        AlertDialog dialog = new AlertDialog.Builder(this).create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.setIcon(android.R.drawable.ic_dialog_alert);
        dialog.setTitle(getResources().getString(R.string.title_text_quit));
        dialog.setMessage(getResources().getString(R.string.dialog_msg_quit_confirm));
        //确定按钮
        dialog.setButton(DialogInterface.BUTTON_POSITIVE, getResources().getText(R.string.enum_yes),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        get(ServicePort.getUrl(ServicePort.LoginOut), null,
                                new RequestManager.RequestListener() {
                                    @Override
                                    public void onRequest() {

                                    }

                                    @Override
                                    public void onSuccess(String s, Map<String, String> stringStringMap, String s2, int i) {
                                        _BaseApplication.exit();
                                    }

                                    @Override
                                    public void onError(String s, String s2, int i) {
//                                        AlertDialog dialog = new AlertDialog.Builder(SettingActivity.this).create();
//                                        dialog.setCanceledOnTouchOutside(false);
//                                        dialog.setCancelable(false);
//                                        dialog.setIcon(android.R.drawable.ic_dialog_info);
//                                        dialog.setTitle(getResources().getString(R.string.title_text_about));
//                                        dialog.setMessage(getResources().getString(R.string.dialog_msg_quit_fail));
//                                        //确定按钮
//                                        dialog.setButton(DialogInterface.BUTTON_POSITIVE, getResources().getText(R.string.enum_ok),
//                                                new DialogInterface.OnClickListener() {
//                                                    @Override
//                                                    public void onClick(DialogInterface dialog, int which) {
//                                                        if (errorCount >= 3) {
//                                                            _BaseApplication.exit();
//                                                        }
//                                                        errorCount++;
//                                                    }
//                                                });
//                                        dialog.show();

                                        _BaseApplication.exit();
                                    }
                                }, 1);

                    }
                });
        dialog.setButton(DialogInterface.BUTTON_NEGATIVE, getResources().getText(R.string.enum_no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //
            }
        });

        dialog.show();
    }


    @Override
    public boolean getScannerVisible() {
        return false;
    }

    @Override
    public boolean getSetVisible() {
        return false;
    }

    @Override
    public int getTopNameResId() {
        return R.string.title_activity_setting;
    }
}


package com.mls.wms.android.extend;

import android.app.Fragment;

import com.android.http.LoadControler;
import com.android.http.RequestManager;
import com.mls.wms.android.util.VolleyUtils;

import java.util.Map;

/**
 * Created by 2014-400 on 2015-09-15.
 */
public class BaseFragment extends Fragment implements IHttpVolley {
    private BaseApplication getBaseApplication() {
        return (BaseApplication) getActivity().getApplication();
    }

    /**
     * get
     *
     * @param url
     * @param requestListener
     * @param actionId
     * @return
     */
    @Override
    public LoadControler get(String url, RequestManager.RequestListener requestListener, int actionId) {
        return VolleyUtils.get(getBaseApplication(), url, null, null, requestListener, true, actionId);
    }

    /**
     * get
     *
     * @param url
     * @param data
     * @param requestListener
     * @param actionId
     * @return
     */
    @Override
    public LoadControler get(String url, Map<String, String> data, RequestManager.RequestListener requestListener, int actionId) {
        return VolleyUtils.get(getBaseApplication(), url, data, null, requestListener, true, actionId);
    }

    /**
     * get
     *
     * @param url
     * @param data
     * @param headers
     * @param requestListener
     * @param actionId
     * @return
     */
    @Override
    public LoadControler get(String url, Map<String, String> data, Map<String, String> headers, RequestManager.RequestListener requestListener, int actionId) {
        return VolleyUtils.get(getBaseApplication(), url, data, headers, requestListener, true, actionId);
    }

    /**
     * get
     *
     * @param url
     * @param requestListener
     * @param shouldCache
     * @param actionId
     * @return
     */
    @Override
    public LoadControler get(String url, RequestManager.RequestListener requestListener, boolean shouldCache, int actionId) {
        return VolleyUtils.get(getBaseApplication(), url, null, null, requestListener, shouldCache, actionId);
    }

    /**
     * get
     *
     * @param url
     * @param data
     * @param headers
     * @param requestListener
     * @param shouldCache
     * @param actionId
     * @return
     */
    @Override
    public LoadControler get(String url, Map<String, String> data, Map<String, String> headers, RequestManager.RequestListener requestListener, boolean shouldCache, int actionId) {
        return VolleyUtils.get(getBaseApplication(), url, data, headers, requestListener, shouldCache, actionId);
    }

    /**
     * default post method
     *
     * @param url
     * @param data            String, Map<String, String> or RequestMap(with file)
     * @param requestListener
     * @param actionId
     * @return
     */
    @Override
    public LoadControler post(String url, Object data, final RequestManager.RequestListener requestListener, int actionId) {
        return VolleyUtils.post(getBaseApplication(), url, data, null, requestListener, actionId);
    }

    /**
     * @param url
     * @param data
     * @param headers
     * @param requestListener
     * @param actionId
     * @return
     */
    @Override
    public LoadControler post(String url, Object data, Map<String, String> headers, final RequestManager.RequestListener requestListener, int actionId) {
        return VolleyUtils.post(getBaseApplication(), url, data, headers, requestListener, actionId);
    }

    /**
     * put
     *
     * @param url
     * @param data
     * @param requestListener
     * @param actionId
     * @return
     */
    @Override
    public LoadControler put(String url, Object data, final RequestManager.RequestListener requestListener, int actionId) {
        return VolleyUtils.put(getBaseApplication(), url, data, requestListener, actionId);
    }

}

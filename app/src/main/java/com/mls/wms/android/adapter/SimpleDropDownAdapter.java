package com.mls.wms.android.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.mls.wms.android.R;
import com.mls.wms.android.model.DropDownItemModel;

import java.util.List;

/**
 * Created by 2014-400 on 2015-09-07.
 */
public class SimpleDropDownAdapter extends BaseAdapter {
    private LayoutInflater inflater;
    private Context context;
    private List<DropDownItemModel> items;

    public SimpleDropDownAdapter(Context context, List<DropDownItemModel> items) {
        this.inflater = LayoutInflater.from(context);
        this.context = context;
        this.items = items;
    }

    @Override
    public long getItemId(int position) {
        return position % this.items.size();
    }

    @Override
    public int getCount() {
        return this.items.size();
    }

    @Override
    public DropDownItemModel getItem(int position) {
        return position <= this.items.size() - 1 ? this.items.get(position) : null;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        DropDownItemModel item = getItem(position);

        ViewHolder viewHolder = null;
        if (convertView == null) {
            convertView = this.inflater.inflate(R.layout.act_base_spinner, null);

            viewHolder = new ViewHolder();
            //中文值
            viewHolder.tvKey = (TextView) convertView.findViewById(R.id.act_base_spinner_key);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.tvKey.setText(item.getKey());

        return convertView;
    }

    class ViewHolder {
        TextView tvKey;
    }
}

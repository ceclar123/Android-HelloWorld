package com.mls.wms.android.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.mls.wms.android.R;
import com.mls.wms.android.model.InventoryQueryModel;

import java.util.List;

/**
 * Created by 2014-400 on 2015-09-15.
 */
public class InventoryQueryAdapter extends BaseAdapter {
    private LayoutInflater inflater;
    private Context context;
    private List<InventoryQueryModel> items;

    public InventoryQueryAdapter(Context context, List<InventoryQueryModel> items) {
        this.inflater = LayoutInflater.from(context);
        this.context = context;
        this.items = items;
    }

    @Override
    public long getItemId(int position) {
        return position % this.items.size();
    }

    @Override
    public int getCount() {
        return this.items.size();
    }

    @Override
    public InventoryQueryModel getItem(int position) {
        return position <= this.items.size() - 1 ? this.items.get(position) : null;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        InventoryQueryModel item = getItem(position);
        ViewHolder viewHolder = null;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = this.inflater.inflate(R.layout.act_inventory_query_item, null);

            viewHolder.tvZoneType = (TextView) convertView.findViewById(R.id.act_inventory_query_item_zone_type);
            viewHolder.tvZoneNo = (TextView) convertView.findViewById(R.id.act_inventory_query_item_zone_no);
            viewHolder.tvLocationNo = (TextView) convertView.findViewById(R.id.act_inventory_query_item_location_no);

            viewHolder.tvInventoryStatus = (TextView) convertView.findViewById(R.id.act_inventory_query_item_inventory_status);
            viewHolder.tvCartonNo = (TextView) convertView.findViewById(R.id.act_inventory_query_item_carton_no);
            viewHolder.tvHoldStatus = (TextView) convertView.findViewById(R.id.act_inventory_query_item_frozen_status);
            viewHolder.tvCycleStatus = (TextView) convertView.findViewById(R.id.act_inventory_query_item_cycle_status);

            viewHolder.tvItemName = (TextView) convertView.findViewById(R.id.act_inventory_query_item_item_name);
            viewHolder.tvItemSku = (TextView) convertView.findViewById(R.id.act_inventory_query_item_item_sku);
            viewHolder.tvItemBarcode = (TextView) convertView.findViewById(R.id.act_inventory_query_item_item_barcode);
            viewHolder.tvItemColor = (TextView) convertView.findViewById(R.id.act_inventory_query_item_item_color);
            viewHolder.tvItemSize = (TextView) convertView.findViewById(R.id.act_inventory_query_item_item_size);
            viewHolder.tvItemUnit = (TextView) convertView.findViewById(R.id.act_inventory_query_item_item_unit);

            viewHolder.tvOnhandQty = (TextView) convertView.findViewById(R.id.act_inventory_query_item_onhand_qty);
            viewHolder.tvAllocateQty = (TextView) convertView.findViewById(R.id.act_inventory_query_item_allocate_qty);
            viewHolder.tvAvailableQty = (TextView) convertView.findViewById(R.id.act_inventory_query_item_available_qty);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.tvZoneType.setText(item.getZoneTypeName());
        viewHolder.tvZoneNo.setText(item.getZoneNo());
        viewHolder.tvLocationNo.setText(item.getLocationNo());

        viewHolder.tvInventoryStatus.setText(item.getInventoryStatusName());
        viewHolder.tvCartonNo.setText(item.getCartonNo());
        viewHolder.tvHoldStatus.setText(item.getIsHold());
        viewHolder.tvCycleStatus.setText(item.getIsCycleCount());

        viewHolder.tvItemName.setText(item.getItemName());
        viewHolder.tvItemSku.setText(item.getSku());
        viewHolder.tvItemBarcode.setText(item.getBarcode());
        viewHolder.tvItemColor.setText(item.getColorName());
        viewHolder.tvItemSize.setText(item.getSizeName());
        viewHolder.tvItemUnit.setText(item.getUnitName());

        viewHolder.tvOnhandQty.setText(item.getOnhandQty() + "");
        viewHolder.tvAllocateQty.setText(item.getAllocatedQty() + "");
        viewHolder.tvAvailableQty.setText(item.getAvailableQty() + "");

        return convertView;
    }

    class ViewHolder {
        TextView tvZoneType, tvZoneNo, tvLocationNo, tvItemName, tvItemSku, tvItemBarcode, tvItemColor, tvItemSize, tvItemUnit, tvOnhandQty, tvAllocateQty, tvAvailableQty, tvInventoryStatus, tvCartonNo, tvHoldStatus, tvCycleStatus;
    }
}


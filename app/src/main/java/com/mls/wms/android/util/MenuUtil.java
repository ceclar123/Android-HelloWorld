package com.mls.wms.android.util;

import android.content.Context;
import android.content.res.XmlResourceParser;

import com.mls.wms.android.R;
import com.mls.wms.android.model.Menu;

import org.xmlpull.v1.XmlPullParser;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 2014-400 on 2015-08-12.
 */
public class MenuUtil {
    private static List<Menu> menus = new ArrayList<Menu>();

    private MenuUtil() {
    }

    /**
     * 获取所有菜单
     *
     * @param context
     * @return
     * @throws Exception
     */
    public static List<Menu> getMenu(Context context) {
        if (menus != null && menus.size() > 0) {
            return menus;
        } else {
            Menu menu = null;
            XmlResourceParser parser = context.getResources().getXml(R.xml.menu);
            try {
                int event = parser.getEventType();
                while (event != XmlPullParser.END_DOCUMENT) {
                    switch (event) {
                        //判断当前事件是否是文档开始事件
                        case XmlPullParser.START_DOCUMENT:
                            menus = new ArrayList<Menu>();//初始化books集合
                            break;
                        //判断当前事件是否是标签元素开始事件
                        case XmlPullParser.START_TAG:
                            if ("menu".equals(parser.getName())) {//判断开始标签元素是否是book
                                menu = new Menu();
                                menu.setName(parser.getAttributeValue(0));
                                menu.setId(Integer.parseInt(parser.getAttributeValue(1)));
                                menu.setLevel(Integer.parseInt(parser.getAttributeValue(2)));
                                menu.setOrder(Integer.parseInt(parser.getAttributeValue(3)));
                                menu.setPid(Integer.parseInt(parser.getAttributeValue(4)));
                                menu.setTitle(parser.getAttributeValue(5));
                                menu.setViewClass(parser.getAttributeValue(6));
                            }
                            if (menu != null) {
                                //包裹的数据值
//                        if ("name".equals(parser.getName())) {
//                            menu.setTitle(parser.nextText());
//                        }
                            }
                            break;
                        case XmlPullParser.END_TAG:
                            //判断当前事件是否是标签元素结束事件
                            if ("menu".equals(parser.getName())) {//判断结束标签元素是否是book
                                if (menu != null) {
                                    menus.add(menu);
                                }
                                menu = null;
                            }
                            break;
                    }
                    event = parser.next();//进入下一个元素并触发相应事件
                }//end while
            } catch (Exception e) {
                e.printStackTrace();
            }

            return menus;
        }
    }

    /**
     * 获取指定级数菜单
     *
     * @param context
     * @param level
     * @return
     */
    public static List<Menu> getMenu(Context context, int level) {
        List<Menu> list = MenuUtil.getMenu(context);
        List<Menu> rtnList = new ArrayList<Menu>();
        for (Menu item : list) {
            if (item.getLevel() == level) {
                rtnList.add(item);
            }
        }
        return rtnList;
    }

    /**
     * 获取指定父ID菜单
     *
     * @param context
     * @param pid
     * @return
     * @throws Exception
     */
    public static List<Menu> getMenuByPid(Context context, int pid) {
        List<Menu> list = MenuUtil.getMenu(context);
        List<Menu> rtnList = new ArrayList<Menu>();
        for (Menu item : list) {
            if (item.getPid() == pid) {
                rtnList.add(item);
            }
        }
        return rtnList;
    }

    /**
     * 获取指定顶级ID菜单
     *
     * @param context
     * @param topId
     * @return
     * @throws Exception
     */
    public static List<Menu> getMenuByTopId(Context context, int topId) {
        List<Menu> list = MenuUtil.getMenu(context);
        List<Menu> rtnList = new ArrayList<Menu>();
        for (Menu item : list) {
            if (item.getPid() == topId) {
                rtnList.addAll(getMenuByPid(context, item.getId()));
            }
        }
        return rtnList;
    }
}

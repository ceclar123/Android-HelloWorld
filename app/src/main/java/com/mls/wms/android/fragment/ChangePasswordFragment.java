package com.mls.wms.android.fragment;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.android.http.RequestManager;
import com.mls.wms.android.R;
import com.mls.wms.android.constant.ServicePort;
import com.mls.wms.android.extend.BaseApplication;
import com.mls.wms.android.extend.BaseFragment;
import com.mls.wms.android.model.ResponseResult;
import com.mls.wms.android.util.DialogUtils;

import org.json.JSONObject;

import java.util.Map;

/**
 * Created by 2014-400 on 2015-09-10.
 */
public class ChangePasswordFragment extends BaseFragment {
    private View view = null;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fg_change_password, container, false);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        final BaseApplication baseApplication = (BaseApplication) getActivity().getApplication();
        final ITopBar topBar = (ITopBar) getActivity();
        if (topBar != null) {
            EditText etLoginName = (EditText) view.findViewById(R.id.fg_change_password_et_login_name);
            final EditText etOldPwd = (EditText) view.findViewById(R.id.fg_change_password_et_old_password);
            final EditText etNewPwd = (EditText) view.findViewById(R.id.fg_change_password_et_new_password);
            final EditText etConfirmPwd = (EditText) view.findViewById(R.id.fg_change_password_et_confirm_password);
            Button btnConfirm = (Button) view.findViewById(R.id.fg_change_password_btn_confirm);

            etLoginName.setText(baseApplication.getGlobalSetting().getLoginName() + "(" + baseApplication.getGlobalSetting().getUserName() + ")");


            btnConfirm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String oldPwd = etOldPwd.getText().toString().trim();
                    String newPwd = etNewPwd.getText().toString().trim();
                    String confirmPwd = etConfirmPwd.getText().toString().trim();

                    if (TextUtils.isEmpty(oldPwd)) {
                        etOldPwd.setError(getString(R.string.error_old_password_not_null));
                        etOldPwd.requestFocus();
                    } else if (TextUtils.isEmpty(newPwd)) {
                        etNewPwd.setError(getString(R.string.error_new_password_not_null));
                        etNewPwd.requestFocus();
                    } else if (TextUtils.isEmpty(confirmPwd) || false == newPwd.equals(confirmPwd)) {
                        etConfirmPwd.setError(getString(R.string.error_password_not_match));
                        etConfirmPwd.requestFocus();
                    } else {
                        JSONObject data = null;
                        try {
                            data = new JSONObject();
                            data.put("loginName", baseApplication.getGlobalSetting().getLoginName());
                            data.put("passwordOld", oldPwd);
                            data.put("password", newPwd);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        put(ServicePort.getUrl(ServicePort.Password), data, new RequestManager.RequestListener() {
                            @Override
                            public void onRequest() {

                            }

                            @Override
                            public void onSuccess(String s, Map<String, String> stringStringMap, String s2, int i) {
                                String errorMsg = null;
                                try {
                                    ResponseResult result = new ResponseResult();
                                    result = result.parseJson(s);
                                    if (result != null && result.getSuc() == true) {
                                        DialogUtils.showOK(getActivity(), getString(R.string.title_text_tip), getString(R.string.dialog_msg_change_password_success), new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                etOldPwd.setText("");
                                                etNewPwd.setText("");
                                                etConfirmPwd.setText("");
                                                etOldPwd.requestFocus();
                                            }
                                        });
                                    } else {
                                        errorMsg = result == null ? getResources().getString(R.string.title_text_operate_fail) : result.getMessage();
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    errorMsg = e.getMessage();
                                }

                                if (false == TextUtils.isEmpty(errorMsg)) {
                                    etOldPwd.setError(errorMsg);
                                    etOldPwd.requestFocus();
                                }
                            }

                            @Override
                            public void onError(String s, String s2, int i) {
                                etOldPwd.setError(s);
                                etOldPwd.requestFocus();
                            }
                        }, 1);
                    }
                }
            });
        }
    }
}
package com.mls.wms.android.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.HorizontalScrollView;
import android.widget.TextView;

import com.mls.wms.android.R;
import com.mls.wms.android.extend.SwipeLayoutAdapter;
import com.mls.wms.android.model.PickShipDetailModel;
import com.mls.wms.android.widget.outwh.PickAndSortActivity;
import com.mls.wms.android.widget.outwh.PickToSortActivity;

import java.util.List;

/**
 * Created by 2014-400 on 2015-09-02.
 */
public class PickShipDetailAdapter extends SwipeLayoutAdapter<PickShipDetailModel> {
    private LayoutInflater inflater;
    private Context context;
    private List<PickShipDetailModel> items;
    private int pickType = 0;
    private long pickId = 0L;

    public PickShipDetailAdapter(Context context, int pickType, long pickId, List<PickShipDetailModel> items) {
        super(context, R.layout.act_pick_ship_detail_item, R.layout.act_pick_ship_detail_item_swipe, items);
        this.inflater = LayoutInflater.from(context);
        this.context = context;
        this.items = items;
        this.pickType = pickType;
        this.pickId = pickId;
    }

    public PickShipDetailAdapter(Context context, int pickType, long pickId, int actionViewResourceId, List<PickShipDetailModel> items) {
        super(context, R.layout.act_pick_ship_detail_item, actionViewResourceId, items);
        this.inflater = LayoutInflater.from(context);
        this.context = context;
        this.items = items;
        this.pickType = pickType;
        this.pickId = pickId;
    }

    @Override
    public int getCount() {
        return this.items.size();
    }

    @Override
    public PickShipDetailModel getItem(int position) {
        return position <= this.items.size() - 1 ? this.items.get(position) : null;
    }


    @Override
    public void setContentView(View contentView, int position, HorizontalScrollView parent) {
        PickShipDetailModel item = getItem(position);

        //货位
        TextView lblLocationNo = (TextView) contentView.findViewById(R.id.act_pick_ship_detail_item_location_no);
        lblLocationNo.setText(item.getLocationNo());
        //商品名称
        TextView lblItemName = (TextView) contentView.findViewById(R.id.act_pick_ship_detail_item_item_name);
        lblItemName.setText(item.getItemName());
        //商品编码
        TextView lblItemSku = (TextView) contentView.findViewById(R.id.act_pick_ship_detail_item_item_sku);
        lblItemSku.setText(item.getSku());
        //商品条码
        TextView lblItemBarcode = (TextView) contentView.findViewById(R.id.act_pick_ship_detail_item_item_barcode);
        lblItemBarcode.setText(item.getBarcode());
        //颜色
        TextView lblItemColor = (TextView) contentView.findViewById(R.id.act_pick_ship_detail_item_item_color);
        lblItemColor.setText(item.getColorName());
        //尺码
        TextView lblItemSize = (TextView) contentView.findViewById(R.id.act_pick_ship_detail_item_item_size);
        lblItemSize.setText(item.getSizeName());
        //单位
        TextView lblItemUnit = (TextView) contentView.findViewById(R.id.act_pick_ship_detail_item_item_unit);
        lblItemUnit.setText(item.getUnitName());
        //应拣数
        TextView lblAllocatedQty = (TextView) contentView.findViewById(R.id.act_pick_ship_detail_item_allocated_qty);
        lblAllocatedQty.setText(item.getAllocatedQty() + "");
        //待拣数
        TextView lblAvailableQty = (TextView) contentView.findViewById(R.id.act_pick_ship_detail_item_available_qty);
        lblAvailableQty.setText(item.getAvailableQty() + "");
        //已拣数
        TextView lblPickedQty = (TextView) contentView.findViewById(R.id.act_pick_ship_detail_item_picked_qQty);
        lblPickedQty.setText(item.getPickedQty() + "");
    }

    @Override
    public void setActionView(View actionView, final int position, HorizontalScrollView parent) {
        //拣货
        actionView.findViewById(R.id.act_pick_ship_detail_item_swipe_btn_pick).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PickShipDetailModel item = items.get(position);
                if (pickType == 0) {
                    Intent intent = new Intent(v.getContext(), PickAndSortActivity.class);
                    intent.putExtra("id", pickId);
                    v.getContext().startActivity(intent);
                } else if (pickType == 1) {
                    Intent intent = new Intent(v.getContext(), PickToSortActivity.class);
                    intent.putExtra("id", pickId);
                    v.getContext().startActivity(intent);
                }
            }
        });
    }
}

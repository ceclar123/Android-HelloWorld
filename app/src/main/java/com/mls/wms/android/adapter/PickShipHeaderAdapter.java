package com.mls.wms.android.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.HorizontalScrollView;
import android.widget.TextView;

import com.mls.wms.android.R;
import com.mls.wms.android.extend.SwipeLayoutAdapter;
import com.mls.wms.android.model.PickShipHeaderModel;
import com.mls.wms.android.widget.outwh.PickAndSortActivity;
import com.mls.wms.android.widget.outwh.PickShipDetailActivity;
import com.mls.wms.android.widget.outwh.PickToSortActivity;

import java.util.List;

/**
 * Created by 2014-400 on 2015-09-01.
 */
public class PickShipHeaderAdapter extends SwipeLayoutAdapter<PickShipHeaderModel> {
    private LayoutInflater inflater;
    private Context context;
    private List<PickShipHeaderModel> items;
    private int pickType = 0;


    public PickShipHeaderAdapter(Context context, int pickType, List<PickShipHeaderModel> items) {
        super(context, R.layout.act_pick_ship_wave_item, R.layout.act_pick_ship_wave_item_swipe, items);
        this.inflater = LayoutInflater.from(context);
        this.context = context;
        this.items = items;
        this.pickType = pickType;
    }

    public PickShipHeaderAdapter(Context context, int pickType, int actionViewResourceId, List<PickShipHeaderModel> items) {
        super(context, R.layout.act_pick_ship_wave_item, actionViewResourceId, items);
        this.inflater = LayoutInflater.from(context);
        this.context = context;
        this.items = items;
        this.pickType = pickType;
    }

    @Override
    public long getItemId(int position) {
        return position % this.items.size();
    }

    @Override
    public int getCount() {
        return this.items.size();
    }

    @Override
    public PickShipHeaderModel getItem(int position) {
        return position <= this.items.size() - 1 ? this.items.get(position) : null;
    }


    @Override
    public void setContentView(View contentView, int position, HorizontalScrollView parent) {
        PickShipHeaderModel item = items.get(position);

        //波次单号
        TextView tvWaveNo = (TextView) contentView.findViewById(R.id.act_pick_ship_wave_item_wave_no);
        tvWaveNo.setText(item.getWaveId() + "");
        //类型
        TextView tvTypeName = (TextView) contentView.findViewById(R.id.act_pick_ship_wave_item_type_name);
        tvTypeName.setText(item.getTypeName());
        //订单数
        TextView tvOrderQty = (TextView) contentView.findViewById(R.id.act_pick_ship_wave_item_order_qty);
        tvOrderQty.setText(item.getTotalOrderQty() + "");
        //品种数
        TextView tvCategoryQty = (TextView) contentView.findViewById(R.id.act_pick_ship_wave_item_category_qty);
        tvCategoryQty.setText(item.getTotalCategoryQty() + "");
        //总数量
        TextView tvTotalQty = (TextView) contentView.findViewById(R.id.act_pick_ship_wave_item_total_qty);
        tvTotalQty.setText(item.getTotalQty() + "");
        //订单组成
        TextView tvStructName = (TextView) contentView.findViewById(R.id.act_pick_ship_wave_item_struct_name);
        tvStructName.setText(item.getOrderStructName());
        //拣货人
        TextView tvPicker = (TextView) contentView.findViewById(R.id.act_pick_ship_wave_item_picker);
        tvPicker.setText(item.getFpicker());
        //制单日期
        TextView tvCreateDate = (TextView) contentView.findViewById(R.id.act_pick_ship_wave_item_create_date);
        tvCreateDate.setText(item.getCreateDate());
    }

    //实现setActionView方法
    @Override
    public void setActionView(View actionView, final int position, final HorizontalScrollView parent) {
        //拣货
        actionView.findViewById(R.id.act_pick_ship_wave_item_swipe_btn_pick).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PickShipHeaderModel item = items.get(position);
                if (pickType == 0) {
                    Intent intent = new Intent(v.getContext(), PickAndSortActivity.class);
                    intent.putExtra("id", item.getWaveId());
                    v.getContext().startActivity(intent);
                } else if (pickType == 1) {
                    Intent intent = new Intent(v.getContext(), PickToSortActivity.class);
                    intent.putExtra("id", item.getWaveId());
                    v.getContext().startActivity(intent);
                }
            }
        });

        //明细
        actionView.findViewById(R.id.act_pick_ship_wave_item_swipe_btn_detail).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PickShipHeaderModel item = items.get(position);

                Intent intent = new Intent(v.getContext(), PickShipDetailActivity.class);
                intent.putExtra("type", 0);
                intent.putExtra("id", item.getWaveId());
                v.getContext().startActivity(intent);
            }
        });

    }

}

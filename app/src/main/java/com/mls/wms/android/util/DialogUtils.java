package com.mls.wms.android.util;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import com.mls.wms.android.R;

/**
 * Created by 2014-400 on 2015-09-06.
 */
public class DialogUtils {
    private DialogUtils() {
        throw new AssertionError();
    }

    public static void showOK(Context context, String title, String message, DialogInterface.OnClickListener listener) {
        AlertDialog dialog = new AlertDialog.Builder(context).create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.setIcon(android.R.drawable.ic_dialog_info);
        dialog.setTitle(title);
        dialog.setMessage(message);
        //确定按钮
        dialog.setButton(DialogInterface.BUTTON_POSITIVE, context.getResources().getText(R.string.enum_ok), listener);

        dialog.show();
    }

    public static void showYesNo(Context context, String title, String message, DialogInterface.OnClickListener yesListener, DialogInterface.OnClickListener noListener) {
        AlertDialog dialog = new AlertDialog.Builder(context).create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.setIcon(android.R.drawable.ic_dialog_info);
        dialog.setTitle(title);
        dialog.setMessage(message);

        dialog.setButton(DialogInterface.BUTTON_POSITIVE, context.getResources().getText(R.string.enum_yes), yesListener);
        dialog.setButton(DialogInterface.BUTTON_NEGATIVE, context.getResources().getText(R.string.enum_no), noListener);
        dialog.show();
    }

}

package com.mls.wms.android.constant;

/**
 * Created by 2014-400 on 2015-09-07.
 */
public class ScanTypeConstants {
    public static final int SCAN_BAR_CODE = 1;
    public static final int SCAN_QR_CODE = 2;
}

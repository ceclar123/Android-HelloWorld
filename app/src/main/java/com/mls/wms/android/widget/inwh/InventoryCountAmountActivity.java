package com.mls.wms.android.widget.inwh;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.http.RequestManager;
import com.google.zxing.client.android.Intents;
import com.mls.wms.android.R;
import com.mls.wms.android.constant.ScanTypeConstants;
import com.mls.wms.android.constant.ServicePort;
import com.mls.wms.android.extend.AnimDialogFragment;
import com.mls.wms.android.extend.BaseActivity;
import com.mls.wms.android.extend.ProblemFeedbackDialogFragment;
import com.mls.wms.android.model.CountNextRecordModel;
import com.mls.wms.android.model.ResponseResult;
import com.mls.wms.android.util.DialogUtils;
import com.mls.wms.android.util.ViewUtils;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by 2014-400 on 2015-09-24.
 */
public class InventoryCountAmountActivity extends BaseActivity {
    private long countId = 0L;
    private String countType = "";
    private long countTaskId = 0L;
    private long locationId = 0L;

    private EditText etCountId = null;
    private EditText etSpecLocationNo = null;
    private EditText etRealLocationNo = null;
    private EditText etCountQty = null;

    private Button btnContinue = null;
    private Button btnFeedback = null;
    private AnimDialogFragment progressDialog = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_inventory_count_amount);

        //弹出框
        progressDialog = new AnimDialogFragment();
        progressDialog.setResId(R.drawable.anim_progress_dialog);

        this.etCountId = (EditText) this.findViewById(R.id.act_inventory_count_amount_count_id);
        this.etSpecLocationNo = (EditText) this.findViewById(R.id.act_inventory_count_amount_spec_cycle_location_no);
        this.etRealLocationNo = (EditText) this.findViewById(R.id.act_inventory_count_amount_real_count_location_no);
        this.etCountQty = (EditText) this.findViewById(R.id.act_inventory_count_amount_count_qty);
        this.etCountQty.setText("1");

        this.btnContinue = (Button) this.findViewById(R.id.act_inventory_count_amount_btn_continue);
        this.btnFeedback = (Button) this.findViewById(R.id.act_inventory_count_amount_btn_feedback);

        Intent intent = getIntent();
        if (intent != null) {
            countId = intent.getLongExtra("countId", 0L);
            countType = intent.getStringExtra("countType") + "";

            this.etCountId.setText(this.countId + "");
        } else {
            Toast.makeText(this, R.string.dialog_msg_para_error, Toast.LENGTH_SHORT).show();
            this.finish();
            return;
        }

        this.getNextRecord();

        //文本框事件
        List<EditText> ets = ViewUtils.getDescendants(this, EditText.class, false);
        for (final EditText item : ets) {
            if (item.isEnabled() == true && item.getVisibility() == View.VISIBLE) {
                item.setSelectAllOnFocus(true);//获取焦点全选
                item.setOnKeyListener(new View.OnKeyListener() {
                    @Override
                    public boolean onKey(View v, int keyCode, KeyEvent event) {
                        if (keyCode == KeyEvent.KEYCODE_ENTER && event.getAction() == KeyEvent.ACTION_DOWN) {
                            return onEnterDownEditText(v);
                        }

                        return false;
                    }
                });
                item.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                    @Override
                    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                        if (actionId == EditorInfo.IME_ACTION_DONE) {
                            return onEnterDownEditText(v);
                        }
                        return false;
                    }
                });
            }
        }

        //按钮事件
        this.btnContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doSubmit();
            }
        });
        this.btnFeedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProblemFeedbackDialogFragment fragment = new ProblemFeedbackDialogFragment();
                fragment.setCountId(countId);
                fragment.setLocationNo(etSpecLocationNo.getText().toString());
                try {
                    fragment.setQty(Integer.parseInt(etCountQty.getText().toString()));
                } catch (Exception e) {
                    e.printStackTrace();
                    fragment.setQty(0);
                }

                fragment.show(getFragmentManager(), InventoryCountAmountActivity.this.toString());
            }
        });
    }

    /**
     * 按下回车
     *
     * @param view
     * @return
     */
    private boolean onEnterDownEditText(View view) {
        if (view.getId() == this.etRealLocationNo.getId()) {
            isValidCountLocation();
            return true;
        } else if (view.getId() == this.etCountQty.getId()) {
            isValidCountQty();
            return true;
        }

        return false;
    }

    private void getNextRecord() {
        Map<String, String> paras = new HashMap<String, String>();
        paras.put("countId", this.countId + "");
        paras.put("countType", this.countType);
        get(ServicePort.getUrl(ServicePort.CountNextRecord), paras, new RequestManager.RequestListener() {
            @Override
            public void onRequest() {

            }

            @Override
            public void onSuccess(String s, Map<String, String> stringStringMap, String s2, int i) {
                try {
                    ResponseResult result = new ResponseResult();
                    result = result.parseJson(s);
                    if (result != null && result.getSuc() == true) {

                        CountNextRecordModel model = new CountNextRecordModel();
                        model = model.parseJson(result.getResult().toString());

                        countTaskId = model.getCountTaskId();
                        if (countTaskId == 0L) {
                            throw new Exception(getString(R.string.dialog_msg_count_task_get_fail));
                        }
                        etSpecLocationNo.setText(model.getLocationNo());
                        etRealLocationNo.requestFocus();
                    } else {
                        String message = result == null ? getResources().getString(R.string.title_text_operate_fail) : result.getMessage();
                        DialogUtils.showOK(InventoryCountAmountActivity.this, getResources().getString(R.string.title_text_exception), message, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                InventoryCountAmountActivity.this.finish();
                            }
                        });
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    DialogUtils.showOK(InventoryCountAmountActivity.this, getResources().getString(R.string.title_text_exception), e.getMessage(), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            InventoryCountAmountActivity.this.finish();
                        }
                    });
                }
            }

            @Override
            public void onError(String s, String s2, int i) {
                DialogUtils.showOK(InventoryCountAmountActivity.this, getResources().getString(R.string.title_text_exception), s, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        InventoryCountAmountActivity.this.finish();
                    }
                });
            }
        }, 1);
    }

    private void isValidCountLocation() {
        if (true == TextUtils.isEmpty(this.etRealLocationNo.getText().toString())) {
            showDialog(getResources().getString(R.string.dialog_msg_real_count_location_no_not_null), etRealLocationNo);
        } else if (false == this.etSpecLocationNo.getText().toString().trim().equals(this.etRealLocationNo.getText().toString().trim())) {
            showDialog(getResources().getString(R.string.dialog_msg_real_count_location_no_error), etRealLocationNo);
        } else {
            Map<String, String> data = new HashMap<String, String>();
            data.put("inventoryCountId", this.countId + "");
            data.put("inventoryCountLocId", this.countTaskId + "");
            data.put("locationNo", this.etRealLocationNo.getText().toString().trim());

            get(ServicePort.getUrl(ServicePort.CountLocation), data, new RequestManager.RequestListener() {
                @Override
                public void onRequest() {

                }

                @Override
                public void onSuccess(String s, Map<String, String> stringStringMap, String s2, int i) {
                    String errorMsg = null;
                    try {
                        ResponseResult result = new ResponseResult();
                        result = result.parseJson(s);
                        if (result != null && result.getSuc() == true) {
                            locationId = Long.parseLong(result.getResult());

                            etCountQty.requestFocus();
                        } else {
                            errorMsg = result == null ? getResources().getString(R.string.title_text_operate_fail) : result.getMessage();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        errorMsg = e.getMessage();
                    }

                    if (false == TextUtils.isEmpty(errorMsg)) {
                        showDialog(errorMsg, etRealLocationNo);
                    }
                }

                @Override
                public void onError(String s, String s2, int i) {
                    showDialog(s, etRealLocationNo);
                }
            }, 1);
        }
    }


    private void isValidCountQty() {
        if (TextUtils.isEmpty(this.etCountQty.getText().toString())) {
            showDialog(getResources().getString(R.string.dialog_msg_count_qty_not_null), etCountQty);
        } else {
            int moveQty = Integer.parseInt(this.etCountQty.getText().toString().trim());
            if (moveQty <= 0) {
                showDialog(getResources().getString(R.string.dialog_msg_count_qty_smaller), etCountQty);
            } else if (moveQty > Integer.MAX_VALUE) {
                showDialog(getResources().getString(R.string.dialog_msg_count_qty_bigger), etCountQty);
            } else {
                doSubmit();
            }
        }
    }

    private boolean checkSubmit() {
        if (this.locationId == 0L) {
            showDialog(getResources().getString(R.string.dialog_msg_real_count_location_no_not_null), this.etRealLocationNo);
            return false;
        } else if (TextUtils.isEmpty(this.etCountQty.getText().toString())) {
            showDialog(getResources().getString(R.string.dialog_msg_count_qty_not_null), etCountQty);
        } else {
            int moveQty = Integer.parseInt(this.etCountQty.getText().toString().trim());
            if (moveQty <= 0) {
                showDialog(getResources().getString(R.string.dialog_msg_count_qty_smaller), this.etCountQty);
                return false;
            } else if (moveQty > Integer.MAX_VALUE) {
                showDialog(getResources().getString(R.string.dialog_msg_count_qty_bigger), this.etCountQty);
                return false;
            } else {
                //
            }
        }


        return true;
    }


    private void doSubmit() {
        if (true == this.checkSubmit()) {
            progressDialog.show(getFragmentManager(), this.toString());
            JSONObject data = null;
            try {
                data = new JSONObject();
                data.put("countId", this.countId);
                data.put("countTaskId", this.countTaskId);
                data.put("countType", this.countType);
                data.put("locationId", this.locationId);
                data.put("qty", this.etCountQty.getText().toString().trim());
            } catch (Exception e) {
                e.printStackTrace();
            }
            post(ServicePort.getUrl(ServicePort.CountInsert), data, new RequestManager.RequestListener() {
                @Override
                public void onRequest() {

                }

                @Override
                public void onSuccess(String s, Map<String, String> stringStringMap, String s2, int i) {
                    String errorMsg = null;
                    try {
                        ResponseResult result = new ResponseResult();
                        result = result.parseJson(s);
                        if (result != null && result.getSuc() == true) {
                            locationId = 0L;
                            countTaskId = 0L;

                            etRealLocationNo.setText("");
                            etCountQty.setText("1");
                            etRealLocationNo.requestFocus();

                            //获取下一个任务
                            getNextRecord();

                            progressDialog.dismiss();
                            return;
                        } else {
                            errorMsg = result == null ? getResources().getString(R.string.title_text_operate_fail) : result.getMessage();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        errorMsg = e.getMessage();
                    }

                    if (false == TextUtils.isEmpty(errorMsg)) {
                        progressDialog.dismiss();
                        showDialog(errorMsg, etCountQty);
                    }
                }

                @Override
                public void onError(String s, String s2, int i) {
                    progressDialog.dismiss();
                    showDialog(s, etCountQty);
                }
            }, 1);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case ScanTypeConstants.SCAN_BAR_CODE:
                if (resultCode == RESULT_OK) {
                    Bundle bundle = data.getExtras();
                    this.etRealLocationNo.setText(bundle.getString(Intents.Scan.RESULT, ""));
                    this.etRealLocationNo.requestFocus();
                    onEnterDownEditText(this.etRealLocationNo);
                }
            default:
                break;
        }

    }

    @Override
    public void tv_back_click() {
        DialogUtils.showYesNo(this, getString(R.string.title_text_tip), getString(R.string.dialog_msg_cycle_count_back_confirm),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                },
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (locationId == 0L) {
                            etRealLocationNo.requestFocus();
                        } else {
                            etCountQty.requestFocus();
                        }
                    }
                });
    }

    @Override
    public int getTopNameResId() {
        return R.string.title_count_qty;
    }
}

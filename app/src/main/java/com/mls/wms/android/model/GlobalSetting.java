package com.mls.wms.android.model;

import com.mls.wms.android.util.JSONUtils;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.List;

/**
 * Created by 2014-400 on 2015-08-13.
 */
public class GlobalSetting implements IModel, Serializable {
    /**
     * 服务IP
     */
    private String serverIP;
    /**
     * 租户ID
     */
    private long tenantId = 1L;
    /**
     * 租户编码(暂时写死)
     */
    private String tenantNo;
    /**
     * 仓库ID
     */
    private long warehouseId;
    /**
     * 仓库编码
     */
    private String warehouseNo;
    /**
     * 仓库名称
     */
    private String warehouseName;
    /**
     * 用户ID
     */
    private long userId;
    /**
     * 登录名
     */
    private String loginName;
    /**
     * 用户姓名
     */
    private String userName;
    /**
     * 登录Token
     */
    private String token;
    /**
     * 批量扫描权限(1:有;0:无)
     */
    private int isMutiScan;

    private String moduleService;

    public String getServerIP() {
        return serverIP;
    }

    public void setServerIP(String serverIP) {
        this.serverIP = serverIP;
    }

    public long getTenantId() {
        return tenantId;
    }

    public void setTenantId(long tenantId) {
        this.tenantId = tenantId;
    }

    public String getTenantNo() {
        return tenantNo;
    }

    public void setTenantNo(String tenantNo) {
        this.tenantNo = tenantNo;
    }

    public long getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(long warehouseId) {
        this.warehouseId = warehouseId;
    }

    public String getWarehouseNo() {
        return warehouseNo;
    }

    public String getWarehouseName() {
        return warehouseName;
    }

    public void setWarehouseName(String warehouseName) {
        this.warehouseName = warehouseName;
    }

    public void setWarehouseNo(String warehouseNo) {
        this.warehouseNo = warehouseNo;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getIsMultiScan() {
        return isMutiScan;
    }

    public void setIsMultiScan(int isMultiScan) {
        this.isMutiScan = isMultiScan;
    }

    public String getModuleService() {
        return moduleService;
    }

    public void setModuleService(String moduleService) {
        this.moduleService = moduleService;
    }

    /**
     * {"warehouseName":"上海仓","isMutiScan":0,"token":"37FDD21719392642A9CB0F3EDAA566F5-n2.jvm8082","userId":181,"userName":"朱振鹏","warehouseId":88,"warehouseNo":"SH1","moduleService":"","loginName":"0112"}
     *
     * @param json
     * @return
     */
    @Override
    public GlobalSetting parseJson(String json) throws Exception {
        GlobalSetting result = new GlobalSetting();

        JSONObject object = new JSONObject(json);

        result.setWarehouseId(JSONUtils.getLong(object, "warehouseId", 0L));
        result.setWarehouseNo(JSONUtils.getString(object, "warehouseNo", ""));
        result.setWarehouseName(JSONUtils.getString(object, "warehouseName", ""));

        result.setUserId(JSONUtils.getLong(object, "userId", 0L));
        result.setLoginName(JSONUtils.getString(object, "loginName", ""));
        result.setUserName(JSONUtils.getString(object, "userName", ""));

        result.setToken(JSONUtils.getString(object, "token", ""));
        result.setIsMultiScan(JSONUtils.getInt(object, "isMutiScan", 0));
        result.setModuleService(JSONUtils.getString(object, "moduleService", ""));

        return result;
    }

    @Override
    public List<GlobalSetting> parseJsons(String json) throws Exception {
        return null;
    }
}

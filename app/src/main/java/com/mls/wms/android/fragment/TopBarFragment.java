package com.mls.wms.android.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mls.wms.android.R;
import com.mls.wms.android.extend.BaseApplication;

/**
 * Created by 2014-400 on 2015-09-09.
 */
public class TopBarFragment extends Fragment {
    private View view = null;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fg_top_bar, container, false);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        final BaseApplication baseApplication = (BaseApplication) getActivity().getApplication();
        final ITopBar topBar = (ITopBar) getActivity();
        if (topBar != null) {
            TextView tvBack = (TextView) view.findViewById(R.id.fg_top_bar_img_back);
            TextView tvBackTitle = (TextView) view.findViewById(R.id.fg_top_bar_tv_back_title);
            TextView tvTitle = (TextView) view.findViewById(R.id.fg_top_bar_tv_title);
            TextView tvScan = (TextView) view.findViewById(R.id.fg_top_bar_img_scan);
            TextView tvSetting = (TextView) view.findViewById(R.id.fg_top_bar_img_setting);

            tvBack.setTypeface(baseApplication.getIconFont());
            tvScan.setTypeface(baseApplication.getIconFont());
            tvSetting.setTypeface(baseApplication.getIconFont());

            //返回
            tvBack.setVisibility(topBar.getBackVisible() == true ? View.VISIBLE : View.GONE);
            tvBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    topBar.tv_back_click();
                }
            });

            if (topBar.getBackVisible() == true) {
                //靠左标题
                tvBackTitle.setVisibility(View.VISIBLE);
                if (TextUtils.isEmpty(topBar.getTopNameString())) {
                    tvBackTitle.setText(topBar.getTopNameResId());
                } else {
                    tvBackTitle.setText(topBar.getTopNameString());
                }

                tvTitle.setVisibility(View.INVISIBLE);
                tvTitle.setText("");
            } else {
                //居中标题
                tvBackTitle.setVisibility(View.GONE);
                tvBackTitle.setText("");

                tvTitle.setVisibility(View.VISIBLE);
                if (TextUtils.isEmpty(topBar.getTopNameString())) {
                    tvTitle.setText(topBar.getTopNameResId());
                } else {
                    tvTitle.setText(topBar.getTopNameString());
                }
            }
            tvBackTitle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    topBar.tv_back_click();
                }
            });

            //扫描
            tvScan.setVisibility(topBar.getScannerVisible() == true ? View.VISIBLE : View.GONE);
            tvScan.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    topBar.tv_scan_click();
                }
            });

            //设置
            tvSetting.setVisibility(topBar.getSetVisible() == true ? View.VISIBLE : View.GONE);
            tvSetting.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    topBar.tv_setting_click();
                }
            });
        }
    }
}

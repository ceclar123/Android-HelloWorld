package com.mls.wms.android.model;

import com.mls.wms.android.util.JSONUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by 2014-400 on 2015-09-16.
 */
public class PutAwayAdviceModel extends BaseSKUModel implements IModel, Serializable {
    private String locationNo;
    private String inventoryStatusName;
    private int qty;


    @Override
    public PutAwayAdviceModel parseJson(String json) throws Exception {
        PutAwayAdviceModel model = new PutAwayAdviceModel();

        JSONObject object = new JSONObject(json);

        model.setSkuId(JSONUtils.getLong(object, "skuId", 0L));
        model.setSku(JSONUtils.getString(object, "sku", ""));
        model.setBarcode(JSONUtils.getString(object, "barcode", ""));
        model.setItemName(JSONUtils.getString(object, "itemName", ""));
        model.setProperties(JSONUtils.getString(object, "properties", ""));
        model.setColorName(JSONUtils.getString(object, "colorName", ""));
        model.setSizeName(JSONUtils.getString(object, "sizeName", ""));

        model.setLocationNo(JSONUtils.getString(object, "locationNo", ""));
        model.setInventoryStatusName(JSONUtils.getString(object, "inventoryStatusName", ""));
        model.setQty(JSONUtils.getInt(object, "qty", 0));

        return model;
    }

    @Override
    public List<PutAwayAdviceModel> parseJsons(String json) throws Exception {
        List<PutAwayAdviceModel> list = new ArrayList<PutAwayAdviceModel>();
        JSONArray array = new JSONArray(json);
        for (int i = 0; i < array.length(); i++) {
            list.add(parseJson(array.getString(i)));
        }
        return list;
    }


    public String getLocationNo() {
        return locationNo;
    }

    public void setLocationNo(String locationNo) {
        this.locationNo = locationNo;
    }

    public String getInventoryStatusName() {
        return inventoryStatusName;
    }

    public void setInventoryStatusName(String inventoryStatusName) {
        this.inventoryStatusName = inventoryStatusName;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }
}

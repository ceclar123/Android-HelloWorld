package com.mls.wms.android.model;

import com.mls.wms.android.util.JSONUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by 2014-400 on 2015-09-01.
 */
public class PickShipHeaderModel implements IModel, Serializable {
    private long waveId;
    private long shipmentId;
    private String typeName;
    private int totalOrderQty;
    private int totalCategoryQty;
    private int totalQty;
    private String orderStructName;
    private String platformName;
    private String shopName;
    private String carrierName;
    private String regionName;
    private String fpicker;
    private String createDate;

    public long getWaveId() {
        return waveId;
    }

    public void setWaveId(long waveId) {
        this.waveId = waveId;
    }

    public long getShipmentId() {
        return shipmentId;
    }

    public void setShipmentId(long shipmentId) {
        this.shipmentId = shipmentId;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public int getTotalOrderQty() {
        return totalOrderQty;
    }

    public void setTotalOrderQty(int totalOrderQty) {
        this.totalOrderQty = totalOrderQty;
    }

    public int getTotalCategoryQty() {
        return totalCategoryQty;
    }

    public void setTotalCategoryQty(int totalCategoryQty) {
        this.totalCategoryQty = totalCategoryQty;
    }

    public int getTotalQty() {
        return totalQty;
    }

    public void setTotalQty(int totalQty) {
        this.totalQty = totalQty;
    }

    public String getOrderStructName() {
        return orderStructName;
    }

    public void setOrderStructName(String orderStructName) {
        this.orderStructName = orderStructName;
    }

    public String getPlatformName() {
        return platformName;
    }

    public void setPlatformName(String platformName) {
        this.platformName = platformName;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getCarrierName() {
        return carrierName;
    }

    public void setCarrierName(String carrierName) {
        this.carrierName = carrierName;
    }

    public String getRegionName() {
        return regionName;
    }

    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }

    public String getFpicker() {
        return fpicker;
    }

    public void setFpicker(String fpicker) {
        this.fpicker = fpicker;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    @Override
    public PickShipHeaderModel parseJson(String json) throws Exception {
        PickShipHeaderModel model = new PickShipHeaderModel();

        JSONObject object = new JSONObject(json);

        model.setWaveId(JSONUtils.getLong(object, "waveId", 0L));
        model.setShipmentId(JSONUtils.getLong(object, "shipmentId", 0L));
        model.setTypeName(JSONUtils.getString(object, "typeName", ""));

        model.setTotalOrderQty(JSONUtils.getInt(object, "totalOrderQty", 0));
        model.setTotalCategoryQty(JSONUtils.getInt(object, "totalCategoryQty", 0));
        model.setTotalQty(JSONUtils.getInt(object, "totalQty", 0));

        model.setOrderStructName(JSONUtils.getString(object, "orderStructName", ""));
        model.setPlatformName(JSONUtils.getString(object, "platformName", ""));
        model.setShopName(JSONUtils.getString(object, "shopName", ""));

        model.setCarrierName(JSONUtils.getString(object, "carrierName", ""));
        model.setRegionName(JSONUtils.getString(object, "regionName", ""));
        model.setFpicker(JSONUtils.getString(object, "fpicker", ""));
        model.setCreateDate(JSONUtils.getString(object, "createDate", ""));

        return model;
    }

    @Override
    public List<PickShipHeaderModel> parseJsons(String json) throws Exception {
        List<PickShipHeaderModel> list = new ArrayList<PickShipHeaderModel>();
        JSONArray array = new JSONArray(json);
        for (int i = 0; i < array.length(); i++) {
            list.add(parseJson(array.getString(i)));
        }
        return list;
    }
}

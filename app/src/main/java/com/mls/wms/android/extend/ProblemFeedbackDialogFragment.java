package com.mls.wms.android.extend;

import android.app.DialogFragment;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.android.http.RequestManager;
import com.mls.wms.android.R;
import com.mls.wms.android.constant.ServicePort;
import com.mls.wms.android.model.ResponseResult;
import com.mls.wms.android.util.ToastUtils;
import com.mls.wms.android.util.VolleyUtils;

import org.json.JSONObject;

import java.util.Map;

/**
 * Created by 2014-400 on 2015-09-25.
 */
public class ProblemFeedbackDialogFragment extends DialogFragment {
    private long countId = 0L;
    private String locationNo = "";
    private String barcode = "";
    private int qty = 0;

    public long getCountId() {
        return countId;
    }

    public void setCountId(long countId) {
        this.countId = countId;
    }

    public String getLocationNo() {
        return locationNo;
    }

    public void setLocationNo(String locationNo) {
        this.locationNo = locationNo;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }


    private View createView;
    private RadioGroup rg1;
    private AnimDialogFragment progressDialog = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        setCancelable(false);

        createView = inflater.inflate(R.layout.dg_problem_feedback, container);
        return createView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        rg1 = (RadioGroup) view.findViewById(R.id.dg_problem_feedback_rg1);
        Button btnConfirm = (Button) view.findViewById(R.id.dg_problem_feedback_btn_confirm);
        Button btnBack = (Button) view.findViewById(R.id.dg_problem_feedback_btn_back);

        btnConfirm.setOnClickListener(myClickListener);
        btnBack.setOnClickListener(myClickListener);
    }

    private View.OnClickListener myClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v.getId() == R.id.dg_problem_feedback_btn_back) {
                dismiss();
            } else if (v.getId() == R.id.dg_problem_feedback_btn_confirm) {
                RadioButton checkedRb = null;
                for (int i = 0; i < rg1.getChildCount(); i++) {
                    RadioButton temp = (RadioButton) rg1.getChildAt(i);
                    if (true == temp.isChecked()) {
                        checkedRb = temp;
                        break;
                    }
                }

                if (checkedRb != null) {
                    //弹出框
                    progressDialog = new AnimDialogFragment();
                    progressDialog.setResId(R.drawable.anim_progress_dialog);
                    progressDialog.show(getFragmentManager(), this.toString());

                    JSONObject data = null;
                    try {
                        data = new JSONObject();
                        data.put("orderNo", countId);
                        data.put("locationNo", locationNo);
                        data.put("barcode", barcode);
                        data.put("qty", qty);
                        data.put("exceptionMessage", checkedRb.getText().toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    BaseApplication baseApplication = (BaseApplication) getActivity().getApplication();
                    VolleyUtils.post(baseApplication, ServicePort.getUrl(ServicePort.CountExceptionLog), data, new RequestManager.RequestListener() {
                        @Override
                        public void onRequest() {

                        }

                        @Override
                        public void onSuccess(String s, Map<String, String> stringStringMap, String s2, int i) {
                            String errorMsg = null;
                            try {
                                ResponseResult result = new ResponseResult();
                                result = result.parseJson(s);
                                if (result != null && result.getSuc() == true) {
                                    progressDialog.dismiss();
                                    dismiss();
                                    return;
                                } else {
                                    errorMsg = result == null ? getResources().getString(R.string.title_text_operate_fail) : result.getMessage();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                errorMsg = e.getMessage();
                            }

                            progressDialog.dismiss();
                            if (false == TextUtils.isEmpty(errorMsg)) {
                                ToastUtils.show(getActivity(), errorMsg);
                            }
                        }

                        @Override
                        public void onError(String s, String s2, int i) {
                            progressDialog.dismiss();
                            ToastUtils.show(getActivity(), s);
                        }
                    }, 1);

                }
            }
        }
    };
}
package com.mls.wms.android.fragment;

import android.view.View;

/**
 * Created by 2014-400 on 2015-08-14.
 */
public interface IBottomBar {
    public String getBtnLeftName();

    public String getBtnMiddleName();

    public String getBtnRightName();

    public void btn_left_click(View v);

    public void btn_middle_click(View v);

    public void btn_right_click(View v);
}

package com.mls.wms.android.model;

import android.text.TextUtils;

import com.mls.wms.android.util.JSONUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by 2014-400 on 2015-10-30.
 */
public class DeliveryProgressModel implements IModel, Serializable {
    private int[] data;
    private String series;

    @Override
    public DeliveryProgressModel parseJson(String json) throws Exception {
        DeliveryProgressModel model = new DeliveryProgressModel();

        JSONObject object = new JSONObject(json);
        model.setSeries(JSONUtils.getString(object, "series", ""));

        String data = JSONUtils.getString(object, "data", "");
        if (false == TextUtils.isEmpty(data)) {
            model.setData(copyInt(data));
        }

        return model;
    }

    @Override
    public List<DeliveryProgressModel> parseJsons(String json) throws Exception {
        List<DeliveryProgressModel> list = new ArrayList<DeliveryProgressModel>();
        JSONArray array = new JSONArray(json);
        for (int i = 0; i < array.length(); i++) {
            list.add(parseJson(array.getString(i)));
        }
        return list;
    }

    private int[] copyInt(String input) {
        int[] temp = null;
        try {
            JSONArray jsonArray = new JSONArray(input);
            temp = new int[jsonArray.length()];
            for (int i = 0; i < jsonArray.length(); i++) {
                temp[i] = jsonArray.getInt(i);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return temp;
    }

    public int[] getData() {
        return data;
    }

    public void setData(int[] data) {
        this.data = data;
    }

    public String getSeries() {
        return series;
    }

    public void setSeries(String series) {
        this.series = series;
    }
}

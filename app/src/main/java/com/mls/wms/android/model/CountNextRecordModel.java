package com.mls.wms.android.model;

import com.mls.wms.android.util.JSONUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by 2014-400 on 2015-09-24.
 */
public class CountNextRecordModel implements IModel, Serializable {
    private long countTaskId;
    private String locationNo;
    private String cartonNo;
    private String sku;

    @Override
    public CountNextRecordModel parseJson(String json) throws Exception {
        CountNextRecordModel model = new CountNextRecordModel();

        JSONObject object = new JSONObject(json);

        model.setCountTaskId(JSONUtils.getLong(object, "countTaskId", 0L));
        model.setSku(JSONUtils.getString(object, "sku", ""));
        model.setLocationNo(JSONUtils.getString(object, "locationNo", ""));
        model.setCartonNo(JSONUtils.getString(object, "cartonNo", ""));

        return model;
    }

    @Override
    public List<CountNextRecordModel> parseJsons(String json) throws Exception {
        List<CountNextRecordModel> list = new ArrayList<CountNextRecordModel>();
        JSONArray array = new JSONArray(json);
        for (int i = 0; i < array.length(); i++) {
            list.add(parseJson(array.getString(i)));
        }
        return list;
    }


    public long getCountTaskId() {
        return countTaskId;
    }

    public void setCountTaskId(long countTaskId) {
        this.countTaskId = countTaskId;
    }

    public String getLocationNo() {
        return locationNo;
    }

    public void setLocationNo(String locationNo) {
        this.locationNo = locationNo;
    }

    public String getCartonNo() {
        return cartonNo;
    }

    public void setCartonNo(String cartonNo) {
        this.cartonNo = cartonNo;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }
}

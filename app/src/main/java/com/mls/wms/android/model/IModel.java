package com.mls.wms.android.model;

import java.util.List;

/**
 * Created by 2014-400 on 2015-08-31.
 */
public interface IModel<T> {
    T parseJson(String json) throws Exception;

    List<T> parseJsons(String json) throws Exception;
}

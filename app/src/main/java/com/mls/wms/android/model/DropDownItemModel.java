package com.mls.wms.android.model;

import com.mls.wms.android.util.JSONUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by 2014-400 on 2015-09-07.
 */
public class DropDownItemModel implements IModel, Serializable {
    private String key;
    private String value;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public DropDownItemModel() {
        //
    }

    public DropDownItemModel(String key, String value) {
        this.key = key;
        this.value = value;
    }

    @Override
    public DropDownItemModel parseJson(String json) throws Exception {
        DropDownItemModel model = new DropDownItemModel();

        JSONObject object = new JSONObject(json);

        model.setKey(JSONUtils.getString(object, "key", "0"));
        model.setValue(JSONUtils.getString(object, "value", "0"));

        return model;
    }

    @Override
    public List<DropDownItemModel> parseJsons(String json) throws Exception {
        List<DropDownItemModel> list = new ArrayList<DropDownItemModel>();
        JSONArray array = new JSONArray(json);
        for (int i = 0; i < array.length(); i++) {
            list.add(parseJson(array.getString(i)));
        }
        return list;
    }
}
